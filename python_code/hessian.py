import numpy as np

def hessian(f, v, h):
    '''
    Computes the second derivative matrix (Hessian) of a function f(x) 
    at x=v using the first order finite difference method.
    '''
    v = np.asarray(v)
    N = len(v)
    f0 = f(v)
    dtype = type(f0) # if f(x) is complex then the differences will also be complex
    f1 = np.zeros(N, dtype=dtype)
    H = np.zeros([N, N], dtype=dtype)
    
    # compute first differences
    for i in range(N):
        vh = v.copy() # avoid modifying v
        vh[i] += h[i]
        f1[i] = f(vh)
        
    # compute the second derivative matrix
    for i in range(0, N):
        for j in range(i, N):
            vh = v.copy() # avoid modifying v
            vh[i] += h[i]
            vh[j] += h[j]
            f2 = f(vh) # second difference
            # first order finite difference for hessian element
            Hij = (f2 - f1[i] - f1[j] + f0)/(h[i] * h[j])
            H[j,i] = Hij
            H[i,j] = Hij
            
    return H