LATEX_CMD = lualatex -interaction=nonstopmode -shell-escape -synctex=1 document
BIB_CMD = biber document

default:
	$(LATEX_CMD)
	$(BIB_CMD)
	$(LATEX_CMD)

clean:
	rm -f ./chapters/*.aux
	rm -f *aux *bbl *bcf *blg *fdb_latexmk *fls *log *out *pdf *run.xml *synctex.gz *toc