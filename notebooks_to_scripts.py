import sys
import os
import pathlib
from pathlib import Path
import subprocess

notebook_path = Path('./notebooks')
if not notebook_path.exists():
    raise Exception(f'notebook path "{notebook_path.absolute()}" does not exist')

walk_generator = os.walk(notebook_path)
notebooks = []
for i in walk_generator:
    cwd, dirs, files = i
    pcwd = Path(cwd).absolute()
    if not pcwd.match('.ipynb_checkpoints'):
        for file in files:
            file_path = pcwd / file
            if file_path.suffix == '.ipynb':
                notebooks.append(file_path)

print(f'found {len(notebooks)} notebooks at "{notebook_path.absolute()}"')

for notebook_path in notebooks:
    print(f"converting {notebook_path}")
    cmd = f"jupyter nbconvert --to script {notebook_path}"
    res1 = subprocess.run(cmd, shell=True)
    try:
        res1.check_returncode()
    except Exception as e:
        print(res1.stderr.decode()) # print the traceback of the script
        sys.exit() # don't bother raising the error here
    
    cmd = f"python strip_ipy_magics.py {notebook_path.with_suffix('.py')}"
    res2 = subprocess.run(cmd, shell=True)
    try:
        res2.check_returncode()
    except Exception as e:
        print(res2.stderr.decode()) # print the traceback of the script
        sys.exit() # don't bother raising the error here
    