@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

REM loop over all .ipynb files in the root \notebooks subdirectories
REM strip each notebook of metadata for stroing in git

for /R .\notebooks\ %%f in (*.ipynb) do (
	set str1=%%f
	if "x!str1:\.ipynb_checkpoints\=!"=="x!str1!" (
	nb-clean clean !str1!
	) else (
	echo skipping ipynb checkpoint
	)
)