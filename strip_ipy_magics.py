import sys
import os
import re

ipy_magic_reg_expr = re.compile(r"^.*get_ipython\(\)\.run_line_magic\((.*)\).*$")
hook_reg_expr = re.compile(r"^\#\$\s*(.*)$")

temp_filename = "temp.out"

if __name__ == '__main__':
    script_name, *args = sys.argv
    for arg in args:
        print(arg)
        with open(arg, "r") as infile:
            with open(temp_filename, "w") as outfile: 
                for line in infile:
                    if match := hook_reg_expr.match(line):
                        outfile.write(match.groups()[0])
                    elif ipy_magic_reg_expr.match(line):
                        pass
                    else:
                        outfile.write(line)
    os.replace(temp_filename, arg)