#this script doesn't work because it turns out compiling chapters separately is a huge pain

import os
import sys
import pathlib
from pathlib import Path
import subprocess
import functools

import uuid

chapter_filenames = ["chapters/intro_chapter", "chapters/mode_matching_error_signal"]
chapter_names = ["introduction", "mode_matching_error_signal"]
latex_ = "-pdflatex=\"lualatex %O -interaction=nonstopmode -shell-escape -synctex=1 "

for ch_name, ch_filename in zip(chapter_names, chapter_filenames):
    job_lst = ["latexmk", "-pdf", latex_+f"-output-directory=ch_{ch_name} \"\includeonly{{{ch_filename}}}\input{{document}}\"\"", f"-jobname={ch_name}"]
    job_str = functools.reduce(lambda x,y: x+" "+y, job_lst)
    print(job_str)
    # res1 = subprocess.run(job_lst, shell=True, check=True, capture_output=True)