# Cell 0
%load_ext autoreload
%autoreload 2
# Cell 1
import numpy as np
import scipy
import matplotlib as mpl
import matplotlib.pyplot as plt

import scipy.optimize as optm

import os
import sys
import time
from pathlib import Path
fun_lib_path = Path.home() / 'git/alexei_fun_lib' 
sys.path.append(str(fun_lib_path))

import collections
import functools

import optics_funs as of
import numerical_funs as nf
import matplotlib_funs as mpf
import LCT_funs as lctf
import general_funs as gef

import new_types as nt
import mutants

import finesse
import finesse.ligo
from finesse.cymath.homs import HGModes

import func_funs as ff
import graph_funs as grf
import finesse3_funs as f3f
import wolfram_funs as wlf

import networkx as nx
import pygraphviz as pgv
import pydot

from IPython.display import Image, SVG
# Cell 2
code = """

laser L0 P=1

###############################################################################
###   PRC
###############################################################################
s sPRCin L0.p1 PRM.p1
m PRM
s lp1 PRM.p2 PR2.p1
bs PR2
s lp2 PR2.p2 PR3.p1
bs PR3
s lp3 PR3.p2 BS.p1
###############################################################################
###   BS
###############################################################################
bs BS
###############################################################################
###   Yarm
###############################################################################
# Distance from beam splitter to Y arm input mirror
s ly1 BS.p2 ITMY.p1
m ITMY
s LY ITMY.p2 ETMY.p1
m ETMY
###############################################################################
###   Xarm
###############################################################################
# Distance from beam splitter to X arm input mirror
s lx1 BS.p3 ITMX.p1
m ITMX
s LX ITMX.p2 ETMX.p1
m ETMX
###############################################################################
###   SRC
###############################################################################
s ls3 BS.p4 SR3.p1
bs SR3
s ls2 SR3.p2 SR2.p1
bs SR2
s ls1 SR2.p2 SRM.p1
m SRM
"""
# Cell 3
kat2 = finesse.Model()
kat2.parse(code)

G0 = kat2.optical_network
G1 = grf.remove_sinks(grf.remove_orphans(G0))
# Cell 4
remove_nodes = ['L0.p1.o', 'PR2.p3.i', 'PR2.p4.i', 'PR3.p3.i', 'PR3.p4.i', 'ETMY.p2.i', 'ETMX.p2.i', 'SR3.p3.i', 'SR3.p4.i', 'SR2.p3.i', 'SR2.p4.i', 'SRM.p2.i']
G2 = grf.remove_nodes_from(G1, remove_nodes)
# Cell 5
finesse.plotting.graphviz_draw(network=G2, ratio=0.39, scale=(1,-1), maxiter=1000)
# Cell 6
out0 = finesse.plotting.graphviz_draw(network=G2, ratio=0.39, scale=(1,-1), maxiter=1000, ipython_display=False, format='pdf')
with open('DRMI_graph.pdf', 'wb') as f:
    f.write(out0)
    
!pdfcrop DRMI_graph.pdf DRMI_graph.pdf
# Cell 7
GN = grf.copy_graph(G2)
GN = nx.relabel_nodes(GN, {n:str(i) for i,n in enumerate(GN)})
# Cell 8
finesse.plotting.graphviz_draw(network=GN, angle=0, ratio=0.5, scale=(0.95,-0.95))
# Cell 9
out1 = finesse.plotting.graphviz_draw(network=GN, ratio=0.5, scale=(0.95,-0.95), maxiter=1000, ipython_display=False, format='pdf')
with open('DRMI_graph_num.pdf', 'wb') as f:
    f.write(out1)
    
!pdfcrop DRMI_graph_num.pdf DRMI_graph_num.pdf
# Cell 10
def get_pygraphviz_node_layout(A):
    """Extracts the node positions from a pygraphviz AGraph object
    in a networkx layout format.
    
    Note that pygraphviz only allows nodes to be strings. If the orignal
    networkx graph used non-string node identifiers (e.g. int) then the corresponding 
    nodes will be cast to strings in pygraphviz.
    
    Example
    ----------
    A = nx.drawing.nx_agraph.to_agraph(G)
    A.layout('neato')
    layout = get_pygraphviz_node_layout(A)
    nx.draw(G, pos=layout)
    """
    layout = {}
    for node in A.nodes():
        pos_str = node.attr['pos']
        pos = [float(p) for p in pos_str.split(',')]
        layout[node.name] = pos
    return layout

def networkx_pygraphviz_layout(G, layout='neato'):
    A = nx.drawing.nx_agraph.to_agraph(G)
    A.layout(layout)
    layout = get_pygraphviz_node_layout(A)
    
# Cell 11
_ = grf.drawing.graphviz_draw(network=GN, angle=0, ratio=0.5, scale=(0.95,-0.95), out_keys={'A'}, graph_attrs={'mode':"", 'maxiter':10000})
A = _['A']
layout = get_pygraphviz_node_layout(A)
# Cell 12
nx.draw(GN, pos=layout, node_size=100)
# Cell 13
RG = grf.annotate_paths_to_graph(GN)

N = len(GN.nodes)

nodes1 = list(range(N))
nodes1.remove(2)
nodes1.remove(22)
nodes1.remove(28)
nodes1.remove(39)

for node in nodes1:
    grf.absorb_node(RG, node, inplace=True)
# Cell 14
RG.edges[2,22]['paths']
# Cell 15
RG.edges[2,28]['paths']
# Cell 16
RG.edges[2,39]['paths']
# Cell 17
finesse.plotting.graphviz_draw(network=RG, angle=0, ratio=0.7, scale=(0.95,-0.95))
# Cell 18
dd = finesse.plotting.graphviz_draw(network=RG, angle=0, ratio=0.7, scale=(0.95,-0.95), out_keys=['A'])
A = dd['A']
A.graph_attr.pop('ratio')
A.layout('neato')
# Cell 19
for e in A.edges():
    # remove existing edge position to force them to be automatic
    e.attr.pop('pos')
# Cell 20
e = A.get_edge(2,2)
e.attr['pos']
# Cell 21
class pygraphviz_edge_string():
    """The first coordinate is the head position.
    The second coordinate is the beginning of the line
    The final coordinate is the end of the line
    The remaining points are bezier control points. There needs to be 2,5,7... control points.
    """
    def __init__(self, s):
        self.points = self.string_to_points(s)
        
    @staticmethod
    def string_to_points(s):
        arr = np.array([p.split(',') for p in s[2:].split(' ')], dtype=float)
        return arr
    
    def head_position(self):
        """The first coordinate is the head position
        """
        self.points[0]
        
# Cell 22
e.attr['pos'] = 'e,1,1 1,1 1,1 1,1 1,100 1,1 1,1 10,1'
# Cell 23
e.attr['pos'] = 'e,50,200 1,10 1,20 100,1 60,60 45,22 20,50 1,1'
# Cell 24
e.attr['pos'] = 'e,99,70 1,10 1,20 100,1 100,60'
# Cell 25
es = pygraphviz_edge_string(e.attr['pos'])
# Cell 26
es.points
# Cell 27
A.to_string()
# Cell 28
Image(A.draw(format='png'))
# Cell 29
import pygraphviz as pgv
dot_str = 'digraph "" {\n\tgraph [bb="0,0,218.86,167.08",\n\t\tmaxiter=300,\n\t\tmode=sgd,\n\t\tnormalize=0,\n\t\toverlap=True,\n\t\tpad="0.5,0.5",\n\t\tratio="",\n\t\tsize="13,7"\n\t];\n\tnode [label="\\N",\n\t\tshape=oval,\n\t\tstyle=filled\n\t];\n\t0\t[height=0.5,\n\t\tpos="27,83.532",\n\t\twidth=0.75];\n\t2\t[height=0.5,\n\t\tpos="102.94,83.532",\n\t\twidth=0.75];\n\t0 -> 2;\n\t2 -> 2\t[pos="e,99,70 1,10 1,20 100,1 100,60"];\n\t22\t[height=0.5,\n\t\tpos="142.96,149.08",\n\t\twidth=0.75];\n\t2 -> 22;\n\t28\t[height=0.5,\n\t\tpos="142.99,18",\n\t\twidth=0.75];\n\t2 -> 28;\n\t39\t[height=0.5,\n\t\tpos="173.86,83.548",\n\t\twidth=0.75];\n\t2 -> 39;\n\t22 -> 2;\n\t22 -> 22;\n\t22 -> 39;\n\t28 -> 2;\n\t28 -> 28;\n\t28 -> 39;\n\t39 -> 2;\n\t39 -> 22;\n\t39 -> 28;\n\t39 -> 39;\n}\n'
dot_str = r"""digraph "" {
	graph [bb="0,0,218.86,167.08",
		maxiter=300,
		mode=sgd,
		normalize=0,
		overlap=True,
		pad="0.5,0.5",
        nodesep=0.5,
		size="13,7"
	];
	node [label="\N",
		shape=oval,
		style=filled
	];
	0	[height=0.5,
		pos="27,83.532",
		width=0.75];
	2	[height=0.5,
		pos="102.94,83.532",
		width=0.75];
	0 -> 2;
	2:n -> 2:_;
	22	[height=0.5,
		pos="142.96,149.08",
		width=0.75];
	2 -> 22;
	28	[height=0.5,
		pos="142.99,18",
		width=0.75];
	2 -> 28;
	39	[height=0.5,
		pos="173.86,83.548",
		width=0.75];
	2 -> 39;
	22 -> 2;
	22:n -> 22:n;
	22 -> 39;
	28 -> 2;
	28 -> 28;
	28 -> 39;
	39 -> 2;
	39 -> 22;
	39 -> 28;
	39 -> 39;
}"""
# Cell 30
A2 = pgv.AGraph(dot_str)
A2.has_layout = True
# Cell 31
A2.get_node(2).attr['pos']
# Cell 32
print(A2.draw().decode())
# Cell 33
Image(A2.draw(format='png'))
# Cell 34
import bezier
# Cell 35
nodes = np.random.randn(2,4)
# Cell 36
nodes
# Cell 37
bc = bezier.Curve.from_nodes(nodes)
# Cell 38
bc
# Cell 39

