# Cell 0
%load_ext autoreload
%autoreload 2
%gui qt
# Cell 1
import numpy as np
import scipy
import scipy.cluster
import scipy.io
import matplotlib.pyplot as plt

import lmfit
import numdifftools

import os
import sys
import time
from pathlib import Path
fun_lib_path = Path.home() / 'git/alexei_fun_lib' 
sys.path.append(str(fun_lib_path))

import collections

import optics_funs as of
import numerical_funs as nf
import matplotlib_funs as mpf
import LCT_funs as lctf
import general_funs as gef
import debug_funs as dgf
import stats_funs as sf
import wolfram_funs as wlf

import beam_profiling

import new_types as nt
import mutants
# Cell 2
Nx,Ny = 301,302

w = 0.1618
xs = np.linspace(-1,1,Nx) * 4*w
ys = np.linspace(-1,1,Ny) * 4*w

x0 = 0.76*w
y0 = -0.32*w
a = 1.578

E0 = a*of.i_nm_w(xs-x0,ys-y0,w,w*1.2)

c0,c1 = 1,0.1
noise_scale = c0 + c1*np.sqrt(E0)
noise = np.random.randn(Ny,Nx)*noise_scale
snr = E0/noise_scale
E1 = E0+noise
# Cell 3
mpf.imshow(E1)
# Cell 4
def fun(theta):
    a,x0,y0,wx,wy = theta
    model = a*of.i_nm_w(xs-x0,ys-y0,wx,wy)
    return np.ravel(model-E1)

def fun2(theta):
    return np.sum(np.abs(fun(theta))**2)

def lmfit_fun(params):
    a = params['a']
    x0 = params['x0']
    y0 = params['y0']
    wx = params['wx']
    wy = params['wy']

    model = a*of.i_nm_w(xs-x0,ys-y0,wx,wy)

    return (E1-model)

params = lmfit.Parameters()
params.add('a', value=1)
params.add('x0', value=0.007)
params.add('y0', value=-0.01)
params.add('wx', value=0.15)
params.add('wy', value=0.15)
# Cell 5
lmfit_soln = lmfit.minimize(lmfit_fun, params)
# Cell 6
fun2(soln.x)
# Cell 7
lmfit_soln
# Cell 8
np.sqrt(np.diag(lmfit_soln.covar))
# Cell 9
soln = scipy.optimize.least_squares(fun, [1,0,0,0.15,0.15], method='lm')
# Cell 10
soln.x
# Cell 11
def hessian(fun, x0, step_size=None):
    '''
    Computes the second derivative matrix (Hessian) of function fun(x) at x=x0
    using the first order finite difference method.
    '''
    
    if step_size is None:
        step_size = np.maximum(np.abs(x0) * 1e-7, 1e-8)
    N = len(x0)
    dx = step_size
    f0 = fun(x0)
    f1 = np.zeros(N)
    H = np.zeros([N,N])
    
    # compute first derivatives
    for i in range(N):
        xv = x0.copy()
        xv[i] += dx[i]
        f1[i] = fun(xv)
    
    # compute the second derivative matrix
    for i in range(0,N):
        for j in range(i,N):
            xv = x0.copy()
            xv[i] += dx[i]
            xv[j] += dx[j]
            f2 = fun(xv)
            # first order finite difference for second derivative
            h = (f2 - f1[i] - f1[j] + f0)/(dx[i]*dx[j])
            H[j,i] = h
            H[i,j] = h
            
    return H
# Cell 12
import numpy as np

def hessian(f, v, h):
    '''
    Computes the second derivative matrix (Hessian) of function f(x) at x=v
    using the first order finite difference method.
    '''
    N = len(v)
    f0 = f(v)
    f1 = np.zeros(N)
    H = np.zeros([N,N])
    
    # compute first derivatives
    for i in range(N):
        vh = v.copy()
        vh[i] += h[i]
        f1[i] = f(vh)
        
    # compute the second derivative matrix
    for i in range(0,N):
        for j in range(i,N):
            vh = v.copy()
            vh[i] += h[i]
            vh[j] += h[j]
            f2 = f(vh)
            # first order finite difference for second derivative
            Hij = (f2 - f1[i] - f1[j] + f0)/(h[i]*h[j])
            H[j,i] = Hij
            H[i,j] = Hij
            
    return H
# Cell 13
Jf = numdifftools.Jacobian(fun2,step=np.array([1e-7,1e-7,1e-7,1e-6,1e-6]))
Hf = numdifftools.Hessian(fun2)
# Cell 14
mH = myhess(fun2, soln.x)
mH2 = nf.hessian(fun2, soln.x, np.array([1e-7,1e-7,1e-7,1e-6,1e-6]))

mHi = np.linalg.inv(mH)
mH2i = np.linalg.inv(mH2)
# Cell 15
mH3 - mH2
# Cell 16
J = Jf(soln.x)
H = Hf(soln.x)

Hi = np.linalg.inv(H)
JJ = J.T@J
JJi = np.linalg.inv(JJ)
# Cell 17
2*Hi[0,4]**2/(Hi[0,0]*Hi[4,4])
# Cell 18
fig, ax = mpf.subplots_square(2,3)
ax = np.ravel(ax)

plt.sca(ax[0])
mpf.imshow(Hi)
plt.title('numdifftools')

plt.sca(ax[1])
plt.title('finite difference')
mpf.imshow(mHi)

plt.sca(ax[2])
plt.title('finite difference (jacobian)')
mpf.imshow(mH2i)

plt.sca(ax[3])
plt.title('lmfit')
mpf.imshow(lmfit_soln.covar)
# Cell 19

