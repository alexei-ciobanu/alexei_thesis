# Cell 0
%load_ext autoreload
%autoreload 1
%matplotlib inline
# Cell 1
import numpy as np
import scipy
import scipy.cluster
import scipy.io
import scipy.special
import matplotlib as mpl
import matplotlib.pyplot as plt

import sympy as sp

import lmfit
import numdifftools

import os
import sys
import time
from pathlib import Path
fun_lib_path = Path.home() / 'git/alexei_fun_lib' 
sys.path.append(str(fun_lib_path))

import collections

import optics_funs as of
import numerical_funs as nf
import matplotlib_funs as mpf
import LCT_funs as lctf
import general_funs as gef
import debug_funs as dgf
import stats_funs as sf
import wolfram_funs as wlf

import beam_profiling

import new_types as nt
import mutants

import numdifftools
# Cell 2
x,y,a,b = sp.symbols('x,y,a,b', real=True)
g = sp.symbols('g', real=True, positive=True)
wx,wy = sp.symbols('w_x,w_y', positive=True, real=True)
u0x = sp.sqrt(sp.sqrt(2/sp.pi)/wx)*sp.exp(-x**2/wx**2)
u0y = sp.sqrt(sp.sqrt(2/sp.pi)/wy)*sp.exp(-y**2/wy**2)
im = u0x*u0y * sp.conjugate(u0x) * sp.conjugate(u0y)
im
# Cell 3
# define knife edge beam profile

ix = sp.integrate(sp.integrate(im,(y, -np.inf, np.inf)),(x,-np.inf, a))
ix = sp.simplify(ix.rewrite(sp.erf))
ix
# Cell 4
# get beam size from knife edge

wxinv = sp.solve(sp.Eq(ix,g),wx)[0]
wxinv
# Cell 5
# noise transfer function

tf = sp.diff(wxinv, g)
tf
# Cell 6
expr = sp.simplify(sp.diff(tf,g))
expr
# Cell 7
sp.solve(sp.Eq(expr,0))
