# Cell 0
%load_ext autoreload
%autoreload 1
%matplotlib inline
# Cell 1
import numpy as np
import scipy
import scipy.cluster
import scipy.io
import scipy.special
import matplotlib as mpl
import matplotlib.pyplot as plt

import sympy as sp

import lmfit
import numdifftools

import os
import sys
import time
from pathlib import Path
fun_lib_path = Path.home() / 'git/alexei_fun_lib' 
sys.path.append(str(fun_lib_path))

import collections

import optics_funs as of
import numerical_funs as nf
import matplotlib_funs as mpf
import LCT_funs as lctf
import general_funs as gef
import debug_funs as dgf
import stats_funs as sf
import wolfram_funs as wlf

# import pykat

import beam_profiling

import new_types as nt
import mutants

%aimport optics_funs
# Cell 2
# Spatial frequency of HG modes
# Cell 3
def HG_matrix(xs, q, N=10, lam=1064e-9):
    w = of.q.get_w(q, lam=lam)
    dx = xs[1] - xs[0]
    e0 = of.u_n_q(xs, q, n=0, lam=lam, include_gouy=False)
    H1 = np.broadcast_to(e0[None, :], [N+1, len(xs)])
    H2 = np.polynomial.hermite.hermvander(np.sqrt(2)*xs/w, N).T
    
    n0 = of.gauss_norm(0, q, lam=lam, include_gouy=False)
    ns = np.arange(N+1).astype(int)
    H3 = of.gauss_norm(ns, q, lam=lam, include_gouy=False)/n0
    
    H = H1*H2*H3[:,None] * np.sqrt(dx)
    return H

def fft_freq(N, dx=1.0):
    df = 1/dx
    f = (np.arange(Nx)-(Nx-1)/2)/Nx * df
    return f

def cos_matrix(xs,q, N=10,lam=1064e-9):
    Nx = len(xs)
    w = of.q.get_w(q, lam=lam)
    ns = np.arange(N+1)
    H = np.cos(np.outer(ns, fft_freq(Nx, 1)*np.pi*2))
    return H
# Cell 4
Nx = 301
Nm = 101

q = 1j
# q = -1834.20310306031*1 + 1j*427.840832603161
w = of.q.get_w(q)
# q = of.q.from_z_w0(0,w)
xs = np.linspace(-1,1,Nx) * np.sqrt(Nm)/4*w
# xs = np.linspace(-1,1,Nx) * 4*w
dx = xs[1] - xs[0]
ns = np.arange(Nm+1)

H = HG_matrix(xs, q, N=Nm)

s = np.sqrt(ns)*w
s2 = np.sqrt(ns)*w*131/Nx

F = nf.centered_dft(Nx)

f = fft_freq(Nx, dx)
s3 = np.sqrt(ns)/w/np.pi

G = cos_matrix(xs, q, Nx-1)
# Cell 5
np.sqrt(101)
# Cell 6
(f[1] - f[0])*10.05/2
# Cell 7
1/w
# Cell 8
plt.pcolormesh(np.abs(H))
plt.colorbar()
# Cell 9
out = []
for i in range(50):
    res = np.conj(H.T)@(F[50+i,:]@H.T) - F[50+i,:]
    out.append(np.sum(np.abs(res)**2))
out = np.array(out)
# Cell 10
plt.plot(out)
# Cell 11
mpf.complex_imshow(H@np.conj(H.T))
# Cell 12
mpf.complex_imshow(np.conj(H.T)@H)
# Cell 13
mpf.complex_imshow(np.conj(H.T)@H@F)
# Cell 14
mpf.complex_imshow(np.abs(np.conj(H.T)@(F@H.T).T - F)**2)
# Cell 15
plt.semilogy(np.sum(np.abs(np.conj(H.T)@H@F - F)**2,axis=0))
# Cell 16
plt.pcolormesh(xs, ns, H.real)
plt.plot(s,ns)
# Cell 17
# Do unstable round trip DLCTs have eigenvectors?
# Cell 18
def iwasawa_random_unstable_cav():
    stab = True
    while stab:
        m = of.abcd.iwasawa_random()
        stab = of.abcd.cavity_stability(m, bool=True)
    return m
# Cell 19
m1 = iwasawa_random_unstable_cav()
m2 = of.abcd.iwasawa_random_stable_cav()
# Cell 20
N = 201

q = 2j
w = of.q.get_w(q)
xs = np.linspace(-1,1,N) * 4*w

D = lctf.LCT1D(xs, xs, M_abcd=m2)
# Cell 21
mpf.complex_imshow(D,interpolation='antialiased')
# Cell 22
eh, ev = lctf.compute_operator_eigenmodes(D)
# Cell 23
plt.plot(np.abs(eh))
# Cell 24
mpf.complex_plot(ev[:,0])
# mpf.complex_plot(D@ev[:,0]/eh[0])
# Cell 25

