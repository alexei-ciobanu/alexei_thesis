# Cell 0
%load_ext autoreload
%autoreload 1
%matplotlib inline
# Cell 1
import numpy as np
import scipy
import scipy.cluster
import scipy.io
import scipy.special
import matplotlib as mpl
import matplotlib.pyplot as plt

import sympy as sp

import lmfit
import numdifftools

import os
import sys
import time
from pathlib import Path
fun_lib_path = Path.home() / 'git/alexei_fun_lib' 
sys.path.append(str(fun_lib_path))

import collections

import optics_funs as of
import numerical_funs as nf
import matplotlib_funs as mpf
import LCT_funs as lctf
import general_funs as gef
import debug_funs as dgf
import stats_funs as sf
import wolfram_funs as wlf

# import pykat

import beam_profiling

import new_types as nt
import mutants

%aimport optics_funs
# Cell 2
gs = r"C:\Users\alexe\Downloads\885365488.pdf"

gsp = Path(gs)
# Cell 3
gsp.stem
# Cell 4
N = 301

q = 1j
w = of.q.get_w(q)

xs = np.linspace(-1,1,N) * 15.33*w
dx = xs[1] - xs[0]

s = nf.dft_xs_scaling(N, dx, lam=1064e-9)
print(s)

z = 1.13
m = of.abcd.space(z)

a,b,c,d = m.ravel()

xs2 = np.linspace(-1,1,N) * 10*w * s
dx2 = xs2[1] - xs2[0]

u0 = of.u_n_q(xs,q)
u1 = of.u_n_q(xs2, q+z/s)
# Cell 5
of.abcd.cm(s**2/z)@of.abcd.scaling(1/s)@of.abcd.frt(1)@of.abcd.cm(z)
# Cell 6
0.48118552*2.3483666
# Cell 7
of.abcd.scaling(1/s)@of.abcd.space(z)@of.abcd.scaling(z)
# Cell 8
Q1 = lctf.CM_kernel(xs, z)
F = lctf.centered_dft(N)
Q2 = lctf.CM_kernel(xs, s**2/z)

D1 = Q2@F@Q1 * 1/np.sqrt(s)
# Cell 9
mpf.complex_imshow(D1)
# Cell 10
mpf.complex_plot(D1@u0)
mpf.complex_plot(u1)
# Cell 11
mpf.complex_plot(u0)
# Cell 12

