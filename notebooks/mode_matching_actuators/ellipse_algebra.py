# Cell 0
%load_ext autoreload
%autoreload 1
# Cell 1
import numpy as np
import scipy
import scipy.cluster
import scipy.io
import scipy.special
import matplotlib as mpl
import matplotlib.pyplot as plt

import sympy as sp

import lmfit
import numdifftools

import os
import sys
import time
from pathlib import Path
fun_lib_path = Path.home() / 'git/alexei_fun_lib' 
sys.path.append(str(fun_lib_path))

import collections

import optics_funs as of
import numerical_funs as nf
import matplotlib_funs as mpf
import LCT_funs as lctf
import general_funs as gef
import debug_funs as dgf
import stats_funs as sf
import wolfram_funs as wlf
import math_funs as mf

import beam_profiling

import new_types as nt
import mutants
# Cell 2
def cross_product(r1, r2):
    a,b = r1
    c,d = r2
    return a*d-b*c

def dot_product(r1, r2):
    a,b = r1
    c,d = r2
    return a*c + b*d

def norm(r):
    a,b = r
    return np.sqrt(a**2 + b**2)
# Cell 3
def ellipse_minor_axis(r1,r2):
    a,b = r1.ravel()**2
    c,d = r2.ravel()**2
    minor_2 = (a - (b*c)/d)/(1-b/d)
    return np.sqrt(np.abs(minor_2))

def ellipse_major_axis(r1,r2):
    a,b = r1.ravel()**2
    c,d = r2.ravel()**2
    major_2 = (b-(a*d)/c)/(1-a/c)
    return np.sqrt(np.abs(major_2))

def ellipse_major_minor_axes(r1,r2):
    '''
    Takes two vectors and solves for the major and minor axes formed by their ellipse
    Solution can be checked that the two input vectors lie on this ellipse using 
    x**2/a**2 + y**2/b**2 == 1
    '''
    a = ellipse_minor_axis(r1, r2)
    b = ellipse_major_axis(r1, r2)
    return a,b
# Cell 4
def area_of_2_vectors(v1, v2):
    '''
    Compute area of two vectors by first finding their orthgonal components using SVD
    Then use area of ellipse formula.
    '''
    U,S,V = np.linalg.svd(np.hstack([r1,r2]))
    area = np.pi * np.product(np.abs(S))
    return area

def vector_volume(V_basis):
    '''
    Compute volume of vector basis by first finding their orthgonal components using SVD
    Then use volume of N-ellipsoid formula.
    
    If the number of basis vectors is less than the dimension then the equation computes the
    N-volume where N is the number of vectors.
    '''
    N_dim, N_vectors = V_basis.shape
    assert(N_vectors <= N_dim)
    U,S,V = np.linalg.svd(V_basis)
    area = np.pi*(N_vectors/2) / scipy.special.gamma(N_vectors/2 + 1) * np.product(np.abs(S))
    return area
# Cell 5
a,b = 3,1

t1,t2 = -0.4, np.pi/2 - 0.4
t1 = -0.5
t2 = 0.1

thetas = np.linspace(0,2*np.pi,601)
xs = a*np.sin(thetas)
ys = b*np.cos(thetas)

x1 = a*np.sin(t1)
y1 = b*np.cos(t1)

x2 = a*np.sin(t2)
y2 = b*np.cos(t2)

r1 = np.vstack([x1,y1])
r2 = np.vstack([x2,y2])

m = of.abcd.frt(0.6)

x1,y1 = m@r1
x2,y2 = m@r2

xs, ys = m@np.vstack([xs,ys])
# Cell 6
phi = np.arccos(dot_product(r1,r2)/(norm(r1)*norm(r2)))

r2_1 = dot_product(r1, r2)/(norm(r1)**2)*r1
r1_2 = dot_product(r1, r2)/(norm(r2)**2)*r2
a1_2 = (r2 - r2_1)

a_b = np.sqrt(np.abs((r1[0]**2 - r2[0]**2)/(r1[1]**2 - r2[1]**2)))
# Cell 7
ellipse_major_minor_axes(r1,r2)
# Cell 8
plt.plot(xs, ys)
plt.axis('equal')
plt.plot([0, x1],[0, y1])
plt.plot([0, x2],[0, y2])
# Cell 9
# Try SVD to orthogonalize the vectors
# Cell 10
r1 = np.random.randn(2,1)
r2 = np.random.randn(2,1)

R1 = 1*np.random.randn(10001)
R2 = 1*np.random.randn(10001)
# Cell 11
r3 = r1+r2
r3s = R1*r1 + R2*r2
# Cell 12
plt.plot(r3s[0,:], r3s[1,:],'.',ms = 1)
plt.plot([0,r1[0]],[0,r1[1]])
plt.plot([0,r2[0]],[0,r2[1]])
# plt.plot([0,r3[0]],[0,r3[1]])
plt.axis('equal')
# Cell 13
np.hstack([r1,r2])
# Cell 14
area_of_2_vectors(r1,r2)
# Cell 15
U,S,V = np.linalg.svd(np.hstack([r1,r2]))
s1 = U[0,:]*S[0]
s2 = U[1,:]*S[1]
# Cell 16
S[0]*S[1]
# Cell 17
plt.plot(r3s[0,:], r3s[1,:],'.',ms = 1)
plt.plot()
plt.plot([0,s1[0]],[0,s1[1]])
plt.plot([0,s2[0]],[0,s2[1]])
plt.axis('equal')
# Cell 18

