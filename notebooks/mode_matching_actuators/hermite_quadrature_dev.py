# Cell 0
%load_ext autoreload
%autoreload 1
# Cell 1
import numpy as np
import scipy
import scipy.cluster
import scipy.io
import scipy.special
import matplotlib as mpl
import matplotlib.pyplot as plt

import sympy as sp

import lmfit
import numdifftools

import os
import sys
import time
from pathlib import Path
fun_lib_path = Path.home() / 'git/alexei_fun_lib' 
sys.path.append(str(fun_lib_path))

import collections

import optics_funs as of
import numerical_funs as nf
import matplotlib_funs as mpf
import LCT_funs as lctf
import general_funs as gef
import debug_funs as dgf
import stats_funs as sf
import wolfram_funs as wlf

import pykat

import beam_profiling

import new_types as nt
import mutants
# Cell 2
N = 501
q = 1j
w = of.q.get_w(q)
xs = np.linspace(-1,1,301) * 14*w

plt.plot(of.u_n_q(xs,q,150))

d = np.diag(lctf.CM_kernel(xs, 1j))
# Cell 3
mpf.complex_plot(d)
# Cell 4
def hermite_quadrature(n, norm='amplitude'):
    x,w = scipy.special.roots_hermite(n)
    if norm == 'power':
        w = w/np.sqrt(np.pi)
    elif norm == 'amplitude':
        w = w * np.sqrt(np.sqrt(2/np.pi))
    return x,w
# Cell 5
N = 901
Nh = 31

w1 = 1
w2 = 0.9

q1 = of.q.from_z_w0(0,w1)
q2 = of.q.from_z_w0(0,w2)

xs = np.linspace(-1,1,N)*5*w1
dx = xs[1] - xs[0]

u1_0 = of.u_n_q(xs, q1, n=0)
u1_2 = of.u_n_q(xs, q1, n=2)

u2_0 = of.u_n_q(xs, q2, n=0)
u2_2 = of.u_n_q(xs, q2, n=2)

ue = np.exp(-xs**2) * np.sqrt(np.sqrt(2/np.pi))
# Cell 6
xi,wi = hermite_quadrature(Nh)

yi = of.u_n_q(xi, q2, n=0)
# Cell 7
np.sum(yi*wi)
# Cell 8
np.sum(u1_0*u2_0)*dx
# Cell 9

