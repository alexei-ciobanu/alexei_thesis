# Cell 0
%load_ext autoreload
%autoreload 1
# Cell 1
import numpy as np
import scipy
import scipy.cluster
import scipy.io
import scipy.special
import matplotlib as mpl
import matplotlib.pyplot as plt

import sympy as sp

import lmfit
import numdifftools

import os
import sys
import time
from pathlib import Path
fun_lib_path = Path.home() / 'git/alexei_fun_lib' 
sys.path.append(str(fun_lib_path))

import collections

import optics_funs as of
import numerical_funs as nf
import matplotlib_funs as mpf
import LCT_funs as lctf
import general_funs as gef
import debug_funs as dgf
import stats_funs as sf
import wolfram_funs as wlf

import beam_profiling

import new_types as nt
import mutants
# Cell 2
p1,d1,p2,z = sp.symbols('p1,d1,p2,z1', real=True)
zr = sp.symbols('z_r1', real=True, positive=True)

q = z + zr*sp.I
q = sp.symbols('q', complex=True)

f1 = of.abcd.symbolic.lens_p(p1)
s1 = of.abcd.symbolic.space(d1)
f2 = of.abcd.symbolic.lens_p(p2)

m = f2@s1@f1
# Cell 3
expr = of.abcd.symbolic.q_propag(q,m)
# Cell 4
dp1 = sp.simplify(sp.diff(expr, p1))
dp2 = sp.simplify(sp.diff(expr, p2))

a_cross = abs(sp.re(dp1)*sp.im(dp2) - sp.im(dp1)*sp.re(dp2))**2
a_mag = abs(dp1)**2 * abs(dp2)**2
# Cell 5
print(a_mag)
# Cell 6
sp.solve(sp.Eq(a_cross,a_mag),p1,p2)
# Cell 7
def complex_cross_product(z1,z2):
    a,b = z1.real, z1.imag
    c,d = z2.real, z2.imag
    return a*d-b*c
# Cell 8
r1 = gef.randn_complex()
r2 = np.random.rand()*nf.phase(r1)*np.exp(1j*np.pi/2)
# Cell 9
complex_cross_product(r1,r2)
# Cell 10
np.abs(r1) * np.abs(r2)
# Cell 11

