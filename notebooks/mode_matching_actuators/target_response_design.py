# Cell 0
%load_ext autoreload
%autoreload 1
# Cell 1
import numpy as np
import scipy
import scipy.cluster
import scipy.io
import scipy.special
import matplotlib as mpl
import matplotlib.pyplot as plt

import sympy as sp

import lmfit
import numdifftools

import os
import sys
import time
from pathlib import Path
fun_lib_path = Path.home() / 'git/alexei_fun_lib' 
sys.path.append(str(fun_lib_path))

import collections

import optics_funs as of
import numerical_funs as nf
import matplotlib_funs as mpf
import LCT_funs as lctf
import general_funs as gef
import debug_funs as dgf
import stats_funs as sf
import wolfram_funs as wlf
import math_funs as mf

import beam_profiling

import new_types as nt
import mutants
# Cell 2
cwd = Path(os.getcwd()).absolute()

#$ cwd = Path(__file__).absolute().parent
# Cell 3
q_target = 0.47 + 0.25j

liquid_lens_powers = np.linspace(-1.5, 3.5, 101)
liquid_lens_offsets = np.linspace(0, 30e-2, 100)
max_mismatch = 0.2

xs = np.linspace(-1, 1, liquid_lens_powers.size)
ys = np.linspace(-1, 1, liquid_lens_offsets.size)
rs = np.sqrt(np.add.outer(ys**2, xs**2))
target_overlap = np.exp(-rs**2*0.35)
target_mismatch = 1 - target_overlap
# Cell 4
plt.contourf(liquid_lens_powers, liquid_lens_offsets, target_mask, levels=11)
plt.colorbar()
# Cell 5
ltp = of.abcd.lens_p(liquid_lens_powers)
lto1 = of.abcd.space(liquid_lens_offsets)
lto2 = 30e-2 - lto1
m_lt = lto2@gef.outer_matmul(ltp, lto1)
m_lt = np.transpose(m_lt, [1,0,2,3])
# Cell 6
def objective_function(X, p=2, return_type='residual'):
    z_in, zr_in, z_out = X
    q_in = z_in + zr_in*1j
    m_z_out = of.abcd.space(z_out)
    m = m_z_out@m_lt
    q_out = of.q.propag(q_in, m)
    mismatch = of.mode_mismatch(q_out, q_target)
    residual = np.ravel(mismatch-target_mask)
    if return_type == 'residual':
        return np.sum(np.abs(residual)**p)
    elif return_type == 'mismatch':
        return mismatch
    elif return_type == 'q_out':
        return q_out
# Cell 7
X_init = np.abs(np.random.randn(3))*10
# X_init = np.array([-0.31295377,  0.55258535,  0.94222277])
print(f'{X_init = }')

soln = scipy.optimize.minimize(objective_function, X_init, args=(2,), method='Nelder-Mead')
print(f'{soln.x = }')
# Cell 8
q_outs = objective_function(soln.x, return_type='q_out')
mismatches = objective_function(soln.x, return_type='mismatch')
# Cell 9
plt.contourf(liquid_lens_powers, liquid_lens_offsets, mismatches, levels=11)
mpf.forceAspect()
plt.colorbar()
# Cell 10
with mpf.style_context(['mpl_default', 'default']):
    fig, ax = mpf.subplots(1, 2, figscale=[20,5])
    ax = np.ravel(ax)
    mpf.thesis_figsize(fig)
    
    plt.sca(ax[0])
    plt.contourf(liquid_lens_powers, liquid_lens_offsets, target_mask)
    cb0 = plt.colorbar()
    mpf.forceAspect()
    plt.xlabel('liquid lens focal power [Dpt]')
    plt.ylabel('liquid lens position [m]')
    cb0.set_label('mode mismatch')
    plt.title('Target response')
    
    plt.sca(ax[1])
    plt.contourf(liquid_lens_powers, liquid_lens_offsets, mismatches)
    cb1 = plt.colorbar()
    mpf.forceAspect()
    plt.xlabel('liquid lens focal power [Dpt]')
    plt.ylabel('liquid lens position [m]')
    cb1.set_label('mode mismatch')
    plt.title('Closest least-squares solution')
    
    plt.show()
    
#     mpf.thesis_savefig(fig, cwd / 'knife_edge_noise_transfer_function')
# Cell 11

