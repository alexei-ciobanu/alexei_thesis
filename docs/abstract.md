---
title: Wavefront modelling and sensing for gravitational wave interferometers
author: Alexei Ciobanu
geometry: margin=3.5cm
date: 24/02/2022
date: \today
header-includes:
    - \renewcommand{\baselinestretch}{1.3}
# numbersections: true
# toc: true
---

# Thesis Abstract

On the 14th of September 2015 by the Advanced Laser Interferometer Gravitational-wave Observatory (aLIGO) directly detected gravitational waves for the first time, marking the beginning of gravitational wave astronomy. In 2017 the detection of gravitational waves from a binary neutron star merger was subsequently followed up by optical and radio astronomers --- the first time an astrophysical event was observed by two completely separate astrophysical signals. This marked the beginning of multi-messenger astronomy.
Since then over 50 astrophysical events have been observed using gravitational waves.
To increase the rate of event detection the sensitivity of gravitational wave detectors must be improved.

Current state of the art gravitational wave detectors are optical interferometers in the dual recycled Fabry-Perot Michelson (DRFPMI) configuration with quantum squeezed light injected to reduce vacuum noise. Future plans to improve the sensitivity further rely on increasing the circulating laser power and improving the efficiency of quantum squeezing.
Squeezing efficiency is drastically reduced by optical losses in the interferometer of which mode mismatch is a large component. Higher laser power introduces larger thermal distortions in the interferometer, which increase mode mismatch.

![aLIGO DRFPMI](drmi_full.svg "aLIGO DRFPMI")

This thesis covers topics relevant to optical modelling of coupled cavity interferometers such as the DRFPMI with a focus on mode mismatch. Novel applications in aLIGO commissioning based on existing mode mismatch sensing techniques using the output mode cleaner (OMC) are presented. A new mode mismatch sensing technique based on transverse higher order mode sidebands is demonstrated on an optical tabletop and its applications to mode mismatch sensing in aLIGO is discussed. A new optical modelling framework based on linear canonical transformations and signal flow graph theory is presented.
