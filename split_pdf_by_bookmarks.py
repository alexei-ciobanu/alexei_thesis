import fitz # provided by pymupdf
import argparse
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument("document_path", help="path to pdf file (include the extension)")
parser.add_argument("-o", metavar='out_dir', default='.', help="output directory")

args = parser.parse_args()

document_path = Path(args.document_path)
document_name = document_path.stem
output_directory = Path(args.o)

doc = fitz.open(args.document_path)
toc = doc.get_toc()
# all top level bookmarks begin with 1 as the first element
top_level_toc = [x for x in toc if x[0] == 1]
first_page = 1
last_page = doc.page_count

page_numbers = [first_page] + [x[-1] for x in top_level_toc] + [last_page]
page_ranges = [page_numbers[i:i+2] for i in range(len(page_numbers))][:-1]

output_directory.mkdir(parents=True, exist_ok=True)

for i,page_range in enumerate(page_ranges):
    p1,p2 = page_range
    doc = fitz.open(args.document_path) # reopen document (surprisingly fast)
    # could alternatively use copy/deepcopy (might be slower)
    doc.select(range(p1-1, p2-1))
    # not sure why select() uses 0-based indexing for page numbers when pdf page 
    # numbers start with 1

    # need to call garbage collect when saving otherwise the deleted pages 
    # will also be saved to file
    filename = Path(f'{i:02d}_{document_name}.pdf')
    filepath = output_directory / filename
    doc.save(filepath, garbage=4, deflate=True)