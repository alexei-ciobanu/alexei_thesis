git repository for Alexei Cioabanu's PhD thesis

# Requirements

## \usepackage{minted}

* system installation of `pygments`
  * `pip install pygments`

* running latex in shell-escape mode
  * `pdflatex -shell-escape <input-file>`
  * potentially `minted` can cache the output of `pygments` so that the document can be recompiled without `-shell-escape` (useful for submitting to journals)

## \usepackage[backend=biber]{biblatex}

* need to run `biber` on the document
  * `biber %.bcf` or `biber %`, where `%` stands for the basename of the main `.tex` file
  * compile chain: `pdflatex`, `biber`, `pdflatex`

## TODO

* make a compile/build script