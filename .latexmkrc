@default_files = ('document.tex');

$pdf_mode = 4;
$lualatex = 'lualatex -interaction=nonstopmode -shell-escape -synctex=1';
$bibtex_use = 1