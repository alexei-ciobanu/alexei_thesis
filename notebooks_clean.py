import sys
import os
import pathlib
from pathlib import Path
import subprocess

notebook_path = Path('./notebooks')
if not notebook_path.exists():
    raise Exception(f'notebook path "{notebook_path.absolute()}" does not exist')

walk_generator = os.walk(notebook_path)
notebooks = []
for i in walk_generator:
    cwd, dirs, files = i
    pcwd = Path(cwd).absolute()
    if not pcwd.match('.ipynb_checkpoints'):
        for file in files:
            file_path = pcwd / file
            if file_path.suffix == '.ipynb':
                notebooks.append(file_path)

print(f'found {len(notebooks)} notebooks at "{notebook_path.absolute()}"')

for notebook_path in notebooks:
    print(f"cleaning {notebook_path}")
    res = subprocess.run(["nb-clean", "clean", notebook_path],shell=True, check=True, capture_output=True)