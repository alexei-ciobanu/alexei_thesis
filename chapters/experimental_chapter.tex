\chapter{Optical benchtop setup}

This chapter details the assembly of the benchtop used for the experiments performed for the mode matching error signal in \cref{chp:mode_matching_error_signal}.
The goal of this benchtop is to provide a platform for conducting experiments on mode matching and alignment error signals.
The assembly presents a number of technical challenges which are worth discussing.
\section{Beam Profiling}
The waist position and size of a laser beam has to be kept in mind when designing an optical benchtop to ensure the beam is the appropriate size when incident on any particular optical component.
Even more so when designing a mode matching experiment where the waist position and size have to be known precisely.
For this purpose a number of measurement techniques to measure a beam's spatial intensity profile are available. 
The process of characterising a beam's spatial intensity profile is called \textit{beam~profiling}.
The two methods that will be discussed here are camera measurements and knife-edge measurements.
\subsection{Camera beam profile}
A camera is a versatile beam profiling tool, able to capture the complete intensity distribution of a beam at a single plane.
Though a camera suitable for beam profiling is different to conventional imaging cameras.
For one the sensor material has to be chosen such that there is an appreciable response at the beam wavelength.
Silicon is a good material for imaging visible light, partially extending into the near infra-red. 
Indium gallium arsenide (InGaAs) is a more suitable sensor material for near infra-red imaging, but InGaAs cameras are typically more expensive for the same performance as silicon cameras.

The camera sensor is often protected by a transparent cover to prevent dust from getting on it.
This cover can cause problems in beam profiling as the laser partially reflects off the sensor and the cover, forming unwanted interference fringes in the image.
This is fixed by either removing the cover altogether, exposing the sensor to dust, or to apply an anti-reflective (AR) coating to the cover, which are typically dielectric coatings specifically tuned to the laser wavelength. 

To record measurements the analog voltages from the camera sensor need to be converted to digital via an analog-to-digital converter (ADC), which is typically part of the camera.
The relevant parameter in this digitization process are the ADC bit-depth, which determines the range discrete values a single pixel can take.
A common consumer bit-depth of 8~bits means that a single pixel can take any discrete value from 0 to 255, which provides sufficient detail for capturing and displaying color images but can result in noticeable discretisation error in scientific applications such as beam profiling.
Cameras with bit-depths as high as 16~bit are available for scientific applications.
At 16-bit with 65536 discrete values for each pixel, discretization error becomes less of a concern over other noise sources such as dark current and readout noise.

The choice of camera is then determined by considering the the application in mind, the contributing noise sources, and the overall cost of the camera.
For the beam profiles taken for this benchtop an silicon Andor Zyla camera was used.
The Zyla can operate in both 16-bit and 12-bit modes depending on the software setting.
For beam profiling we use the 16-bit mode exclusively.

After a beam intensity image is taken it needs to be processed.
Typically the goal is to extract the size of the beam in the principle axes.
For that a number of algorithms exist, based on different assumptions and formalisms.

\subsubsection{Least-squares fitting}
If the beam is close to a pure Gaussian (i.e. $>99\%$ single transverse mode) then fitting a Gaussian function to the intensity image can yield good results. 
The main advantages of the least-squares method is robustness to small beam imperfections and that uncertainties of fitted parameters are readily available.
However, the least-squares residual is not linear with respect Gaussian width $\{w_x, w_y\}$ or position $\{x_0, y_0\}$.
Therefore the position and size of the beam on the image needs to be obtained with a non-linear least-squares algorithm.

These algorithms perform an iterative optimization with respect to the least-squares residual. 
Specifically for non-linear least-squares the Levenberg-Marquardt algorithm is the most widely used, which interpolates between gradient descent and the Gauss-Newton algorithm. 
The Levenberg-Marquardt algorithm is available in SciPy~\cite{scipy_10_contributors_scipy_2020} under \verb|scipy.optimize.least_squares| via MINPACK~\cite{more_user_1980} and in the python package \verb|lmfit|~\cite{newville_lmfitlmfit-py_2021}.

The function to be fitted is the 2D Hermite-Gaussian intensity distribution given by
\begin{align}
    I_{nm}(x-x_0,y-y_0,w_x,w_y) = a_{nm} I_{n}(x-x_0, w_x) I_{m}(y-y_0, w_y)
\end{align}
for the set of parameters $\{a_{nm},x_0,y_0,w_x,w_y\}$ where
\begin{align}
    I_{n}(x,w) = a_n \left(H_n\left[\frac{x\sqrt{2}}{w} \right] \right)^2 \exp\left(-\frac{2x^2}{w^2}\right).
\end{align}

The least-squares residual $\chi^2$ for a measured $N\times M$ beam profile $\bm{I}$ is then given by
\begin{align}
    \chi^2(\bm{\theta}) = \sum_{i=0}^N \sum_{j=0}^M \left| a_{nm} I_{nm}(\bm{x}_j-x_0,\bm{y}_i-y_0,w_x,w_y) - \bm{I}_{ij} \right|^2
\end{align}
where the minimum of $\chi^2$ with respect to $\bm{\theta} = \{a_{nm},x_0,y_0,w_x,w_y\}$ gives the least-squares fit parameters $\bm{\theta}^*$ for the 2D Hermite-Gaussian intensity $I_{nm}$ to the samples of the measured beam profile $\bm{I}_{ij}$.
Non-linear least-squares algorithms require an initial guess of the fit parameters $\bm{\theta}$ to begin the optimization process.
The initial guess needs to be reasonably close to the optimal fit for the optimization to converge to a global minimum $\chi^2(\bm{\theta}^*)$.
A sufficiently poor guess will result in the algorithm fitting the Gaussian parameters to describe the background intensity in the beam profile image instead of the laser beam intensity.
A good guess can be supplied by hand using human intuition, or some kind of hueristic algorithm that approximates the fit parameters without needing an initial guess.
The $D4\sigma$ method or a digital knife-edge measurement can serve as reasonable hueristics.

The uncertainty in fit parameters is given by the inverse of the matrix of second-order partial derivatives of the least-squares residual $\chi^2$ with respect to the fit parameters $\bm{\theta}$.
This second derivative matrix $\bm{H}$ is called a \textit{Hessian} matrix, given by
\begingroup % keep the change local
\setlength\arraycolsep{7pt}
\begin{align}
    \bm{H} = \begin{bmatrix}
        \dfrac{\partial^2 \chi^2}{\partial \bm{\theta}_1^2} & \dfrac{\partial^2 \chi^2}{\partial \bm{\theta}_2 \partial \bm{\theta}_1} & \cdots &  \dfrac{\partial^2 \chi^2}{\partial \bm{\theta}_n \partial \bm{\theta}_1} \\[1.5em]
        \dfrac{\partial^2 \chi^2}{\partial \bm{\theta}_1 \partial \bm{\theta}_2} & \dfrac{\partial^2 \chi^2}{\partial \bm{\theta}_2^2} & \cdots &  \dfrac{\partial^2 \chi^2}{\partial \bm{\theta}_{n} \partial \bm{\theta}_2} \\[1.5em]
        \vdots & \vdots & \ddots & \vdots \\[1.5em]
        \dfrac{\partial^2 \chi^2}{\partial \bm{\theta}_n \partial \bm{\theta}_1} & \dfrac{\partial^2 \chi^2}{\partial \bm{\theta}_2 \partial \bm{\theta}_n} & \cdots &  \dfrac{\partial^2 \chi^2}{\partial \bm{\theta}_{n}^2} \\[1.5em]
    \end{bmatrix}
\end{align}
\endgroup
The covariance matrix $\bm{K}$ is then determined by inverting the Hessian where the second derivatives are evaluated at the global minimum $\bm{\theta} = \bm{\theta}^*$.
\begin{align}
    \bm{K} = \bm{H}(\bm{\theta}^*)^{-1}
\end{align}
The diagonal elements of $\bm{K}_{ii}$ contain the variance in the respective fit parameters $\bm{\theta}_i$ where the standard error uncertainty is given by the square root of the variance.

The off-diagonal elements $\bm{K}_{ij}$ give the covariance between fit parameters $\bm{\theta}_i$ and $\bm{\theta}_j$.
A high covariance signifies that the uncertainty between the two parameters is coupled.
In beam profiling it is often the case that beam width and overall beam power have a high covariance, which indicates that if the least-squares fit has overestimated the beam width then it would have also overestimated the beam power and vice-versa.
This can be intuitively understood by the fact that a larger beam with higher power is similar to a smaller beam with lower power, at least in the least-squares sense.

In some cases the second derivatives in $\bm{H}$ can be computed analytically, however in general numerical methods are used such as finite differences.
The first order finite difference approximation to a Hessian matrix element of a function $f(\bm{x})$ evaluated at $\bm{x} = \bm{v}$ is given by
\begin{align}
    \frac{\partial^2 f}{\partial \bm{x}_i \partial \bm{x}_j} \bigg\lvert_{\bm{x}=\bm{v}} = \frac{f(\bm{v} + h_i \bm{\hat{e}_i} + h_j \bm{\hat{e}_j}) - f(\bm{v} + h_i \bm{\hat{e}_i}) - f(\bm{v} + h_j \bm{\hat{e}_j}) + f(\bm{v})}{h_i h_j} + \bigO{\sqrt{h_i^2 + h_j^2}}
\end{align}
where $h_i$ is the step size for the $i$`th parameter $\bm{\hat{e}_i}$ is a basis vector with elements given by
\begin{align}
    (\bm{\hat{e}_i})_j = \begin{dcases}
        0 & i \neq j\\
        1 & i = j
    \end{dcases}
\end{align}
%A python implementation of the finite difference Hessian is given in \cref{code:hessian}.
%
%\begin{listing}[H]
%\inputminted{python}{python_code/hessian.py}
%\caption{Hessian matrix Python function}
%\label{code:hessian}
%\end{listing}

\subsubsection{$D4\sigma$ method}
The $D4\sigma$ method is the international ISO standard for profiling laser beams \cite{iso-11146-12005e_lasers_2005, iso-11146-22005e_lasers_2005, isotr-11146-32004e_lasers_2004}.
It works by computing the first and second moments of a measured intensity image~$\bm{I}$.
This is equivelant to treating the measured intensity as a probability distribution for finding a single photon from the beam at that position.
The mean and variance of this probability distribution are then equivelant to the first and second moments of the measured intensity~$\bm{I}$.

\begin{align}
	\braket{x^2} &= \frac{1}{I_0} \iint_{-\infty}^\infty I(x,y) (x-\braket{x})^2 \dif y \dif x \\
	\braket{y^2} &= \frac{1}{I_0} \iint_{-\infty}^\infty I(x,y) (y-\braket{y})^2 \dif y \dif x \\
	\braket{xy} &= \frac{1}{I_0} \iint_{-\infty}^\infty I(x,y) (x-\braket{x}) (y-\braket{y}) \dif y \dif x
\end{align}
where $\braket{x}$, $\braket{y}$ are the first moments in $x$ and $y$, and $I_0$ is the normalization factor to make the intensity $I(x,y)$ integrate to 1.
These quantities are given by
\begin{align}
	\braket{x} &= \frac{1}{I_0} \iint_{-\infty}^\infty I(x,y) x \dif y \dif x \\
	\braket{y} &= \frac{1}{I_0} \iint_{-\infty}^\infty I(x,y) y \dif y \dif x \\
	{I_0} &= \iint_{-\infty}^\infty I(x,y) \dif y \dif x
\end{align}
To convert the $D4\sigma$ quantities to the standard $1/e$ width the following equations are used
\begin{align}
	w_x^2 &= 2 \left( \braket{x^2} + \braket{y^2} + \frac{\braket{x^2} - \braket{y^2}}{|\braket{x^2} - \braket{y^2}|}\sqrt{\left(\braket{x^2} - \braket{y^2}\right)^2 + 4 \braket{xy}^2} \right)\\
	w_y^2 &= 2 \left( \braket{x^2} + \braket{y^2} - \frac{\braket{x^2} - \braket{y^2}}{|\braket{x^2} - \braket{y^2}|}\sqrt{\left(\braket{x^2} - \braket{y^2}\right)^2 + 4 \braket{xy}^2} \right)\\
\end{align}

There are a number of advantages to the $D4\sigma$ method. 
The first is that the beam width for $x$ and $y$ axes is computed in only 6 passes over the image $\bm{I}$. 
This is to be compared to the least-squares method which typically takes on the order of tens to hunders of passes over the image $\bm{I}$ to converge to a solution depending on how close is the initial guess and what are the convergence tolerances.
The second advantage is that combining these second moments of intensity with the rest of the second moments of the beam's Wigner distribution can be used to compute the beam quality factor~$M^2$, which is invariant under free-space propagation in the paraxial regime.
This beam quality factor~$M^2$ is valid even for highly non-Gaussian beams, meaning that the second moment width is a valid method for describing beam width for non-Gaussian beams.

The disadvantages of the $D4\sigma$ method are that the uncertainty in beam width is not as easy to estimate.
A \textit{bootstrapping} method from statistics can be used to estimate the uncertainty in beam width.
The \textit{bootstrapping} method involves picking a random subset of pixels in the camera image $\bm{I}$ to compute the $D4\sigma$ width using \textit{Monte-Carlo} integration.
Multiple random pixel subsets can be used to compute multiple $D4\sigma$ widths of a single camera image $\bm{I}$, where the uncertainty can be obtained by computing the mean and variance of those $D4\sigma$ widths.
The main issues with \textit{bootstrapping} is choosing the apropriate size of the pixel subsets and the number of trials to compute, both of which are done heuristically.

Additionally, it is well known that the $D4\sigma$ method is very sensitive to background ambient light in the camera image $\bm{I}$.
The image can be taken in a dark room to minimize the ambient light but the camera dark noise will still be present, hence a careful backround subtraction is still required to obtain reliable results.
This subtraction can be done by taking a reference image while the beam is blocked $\bm{I}_{\text{ref}}$ and performing the $D4\sigma$ calculation with respect to the subtracted image $\bm{I}_\text{sub} = \bm{I} - \bm{I}_{\text{ref}}$.
The subtracted image $\bm{I}_\text{sub}$ can potentially still suffer from light scattering off the sensor, most of which will exit the camera but some of which can make it back to the sensor and contribute to the measured image $\bm{I}$ which can't be simply subtracted.

\subsection{Knife-edge beam profile}

The knife-edge method is a robust beam profiling method that uses a single photodiode and a knife-edge on a micrometer.
A beam's power is fully captured on the photodiode when the sensor is larger than the beam size. As the knife edge moves across the photodiode, the measured power is reduced until the knife edge completely covers the photodiode where no optical power is measured. Recording the photodiode voltage as a function of knife edge position yields a \textit{knife edge beam profile} $I_x$ or $I_y$ if the knife edge was translated across the $x$ or $y$~axis respectively.
The knife edge beam profiles can be written as a cumulative integral of the beam intensity.
\begin{align}
	I_x(a) &= \int_{-\infty}^{\infty}\int_{-\infty}^a I(x,y) \dif x \dif y\\
	I_y(b) &= \int_{-\infty}^b  \int_{-\infty}^{\infty} I(x,y) \dif x \dif y,
\end{align}
which can also be easily implemented in software for a camera image as a \textit{digital knife edge}.
If the beam is a pure \tem{00} then the 2D intensity distribution is given by
\begin{align}
	I(x,y) = \frac{2}{\pi} \frac{1}{w_x w_y} \exp\left[ -2\left(\frac{x^2}{w_x^2} + \frac{y^2}{w_y^2} \right) \right]
\end{align}
and the knife edge beam profiles are then given by
\begin{align}
	I_x(a) &= \frac{1}{2}\left(1+\erf{\frac{a \sqrt{2}}{w_x}}\right) \label{eq:knife_edge_profile}\\
	I_y(b) &= \frac{1}{2}\left(1+\erf{\frac{b \sqrt{2}}{w_y}}\right)
	\end{align}
An example of a knife edge beam profile is shown in \cref{fig:knife_edge_beam_profile}.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.95\columnwidth]{knife_edge_profile.pdf}
	\caption{A plot of a cross-section of a non-astigmatic 2D Gaussian intensity profile at $y=0$ (blue) and its corresponding knife edge beam profile (red).
		The beam power is normalized to 1~W and the intensity is given in units of Watts per beam size squared.
	}
	\label{fig:knife_edge_beam_profile}
\end{figure}

$\erf{x}$ is the standard Gauss error function given by
\begin{align}
	\erf{x} = \frac{2}{\pi} \int_{0}^{x} e^{-t^2} \dif t.
\end{align}
Both the error function $\erf{x}$ and the knife edge beam profiles $I_x$ and $I_y$ are trancendental functions and hence they cannot be computed exactly using the standard algebraic operations (addition, multiplication, raising to a power).
However the error function $\erf{x}$ is well studied and rapidly converging approximations have been found. 
Nowadays most mathematical software libraries provide a procedure for computing the error function down to floating point precision.
For example the \verb|scipy| package~\cite{scipy_10_contributors_scipy_2020} has the error function available in \verb|scipy.special.erf|.

The beam size can be extracted by picking a point on the knife edge profile and working backwards to obtain what the beam size must have been.
The equations are
\begin{align}
	w_x &= \frac{a \sqrt{2}}{\erfinv{2 I_x(a) - 1}} \label{eq:knife_edge_inv_wx}\\
	w_y &= \frac{b \sqrt{2}}{\erfinv{2 I_y(b) - 1}} \label{eq:knife_edge_inv_wy}
\end{align}
where $\erfinv{x}$ is the inverse of $\erf{x}$ i.e. $\erfinv{\erf{x}} = x$.
\Cref{eq:knife_edge_inv_wx,eq:knife_edge_inv_wy} are defined everywhere except for at the center of the beam $a=b=0$ where $w_x=w_y=0/0$. 
This is because the center of the knife edge profile contains no information about the width of the beam.

From \cref{eq:knife_edge_inv_wx} we can see that the beam width $w_x$ can be computed from any single choice $a \neq 0$.
This degree of freedom has led to a number ``rule-of-thumb'' choices for knife edge beam profiling, namely the 20:80 rule, and the 10:90 rule.
The conversion factor between any one of these rules and the standard $1/e$ beam width is given by
\begin{align}
	\frac{w_{1/e}}{w_\text{KE}(r)} = \frac{\sqrt{2}}{\erfinv{2r - 1}}
	\label{eq:knife_edge_width_conversion}
\end{align}
where $0< r < 1$ is the knife edge rule.
For the 10:90 rule $r=0.9$, and similarly $r=0.8$ for the 20:80 rule. This is assuming that the $1/e$ and knife edge beam widths are both one-sided or two-sided widths. If the knife edge beam width is two-sided then an extra factor of $1/2$ is needed to get to a one-sided $1/e$ width.

\aac{double check monte carlo}
From \cref{eq:knife_edge_width_conversion} the rules may seem equivelant and up to personal preference, however on closer inspection one finds that these rules differ in their sensitivity to readout noise.
The readout noise can be simulated using the Monte-Carlo experiment by adding random variable $\hat{\eta}$ to the readout signal $I_x(a)$.
\begin{align}
	\hat{w}_x &= \frac{a \sqrt{2}}{\erfinv{2 (I_x(a) + \hat{\eta}) - 1}} \label{eq:knife_edge_wxinv}
\end{align}
Now what we are interested in from the Monte-Carlo experiment is how this noise effects the noise-to-signal ratio (NSR).
\begin{align}
	\text{NSR}_{\text{readout}} &= \frac{\std{\hat{w}_x}}{\mean{\hat{w}_x}} \label{eq:knife_edge_snr}\\
	&=  \std{\frac{a \sqrt{2}}{\erfinv{2 (I_x(a) + \hat{\eta})- 1}}} \times \frac{\erfinv{2 I_x(a)- 1}}{a \sqrt{2}}\\
	&= \frac{\std{\erfinv{2 (I_x(a) + \hat{\eta})- 1}}}{\erfinv{2 I_x(a)- 1}}
\end{align}
The NSR computed here depends on the variance of the input random variable $\hat{\eta}$.
We can obtain a dimensionless quantity indepndent of $\hat{\eta}$, which we will call the transfer function (TF).
\begin{align}
	\text{TF}_{\text{readout}} &= \frac{\text{NSR}_{\text{readout}}}{\std{\hat{\eta}}} \label{eq:knife_edge_noise_TF}
\end{align}
The NSR for any amount of readout noise can be computed by multiplying the noise amplitude with the TF.
Note that this TF approximation is only valid for small noise amplitudes $\std{\hat{\eta}} \ll 1$.
For large $\std{\hat{\eta}}$ the NSR has a non-linear response and so can only be accurately computed using \cref{eq:knife_edge_snr}. 
A plot of the noise TF for knife edge beam profiling rules is shown in \cref{fig:knife_edge_noise_transfer_function}.
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.95\columnwidth]{knife_edge_noise_transfer_function.pdf}
	\caption{A plot of the noise transfer function from \cref{eq:knife_edge_noise_TF} using a Monte-Carlo estimation with 3000 random samples $\hat{\eta}$ for each knife edge rule (blue), an analytical expression for the noise transfer function \cref{eq:knife_edge_analytical_noise_TF} (red), and the knife edge rule with the lowest noise transfer function (dashed black).
	}
	\label{fig:knife_edge_noise_transfer_function}
\end{figure}

We can identify that the noise transfer function is simply calculating the derivative of \cref{eq:knife_edge_wxinv} by using the Monte-Carlo approximation for the derivative
\begin{align}
	\frac{\dif f}{\dif x} \approx \frac{\std{f(x+\hat{\eta})}}{\std{\hat{\eta}}}
\end{align}
So the analytical expression for the noise TF is 
\begin{align}
	\text{TF}_\text{readout} (r) = \frac{w_\text{KE}(r)}{w_{1/e}} \frac{\dif}{\dif r} \left[ \frac{w_{1/e}}{w_\text{KE}(r)} \right]
\end{align}
the derivative can be computed using a computer algebra system (CAS), such as \verb|sympy| or Mathematica.
We find the derivative of \cref{eq:knife_edge_width_conversion} to be
\begin{align}
	\frac{\dif}{\dif r} \left[ \frac{w_{1/e}}{w_\text{KE}(r)} \right] = \frac{\exp\left[\erfinv{2r - 1}^2\right] \sqrt{2 \pi}}{\erfinv{2r-1}^2}
\end{align}
Substituting this into the expression for the TF yields
\begin{align}
	\text{TF}_\text{readout} (r) = \frac{\exp\left[\erfinv{2r - 1}^2\right] \sqrt{\pi}}{\erfinv{2r-1}} \label{eq:knife_edge_analytical_noise_TF}
\end{align}
which is what is plotted as the analytic solution in \cref{fig:knife_edge_noise_transfer_function}.

From \cref{fig:knife_edge_noise_transfer_function} we can conclude that the standard 10:90 and 20:80 rules are almost ideal with respect to minimizing the influence of readout noise.
The optimal rule is found to be somewhere between those two, close to $r=0.84$, which was determined by finding where the first derivative of the analytic solution \cref{eq:knife_edge_analytical_noise_TF} is zero.
In practice however, if the most precise estimate of beam width from a knife edge measurement is needed one would do better by performing a least-squares fit of equation \cref{eq:knife_edge_profile} to the measured data.

\section{Mode matching actuator design}
The job of a mode matching actuator is to change the beam parameter of an incoming beam to another beam parameter.
The actuator should be tunable so that even if the input beam parameter changes the output beam parameter remains constant.
To effectively actuate on the mode matching of a beam two distinct mode matching actuators are required, one for each of the mode matching degrees of freedom.

\subsection{Figure of merit design}

To quantify the performance of a mode matching actuator we first need to know if the actuator is capable of transforming the input beam parameter to a desired output beam parameter.
If we know the input beam parameter $q_\text{in}$ and the ABCD model of our mode matching actuator then the output beam parameter is given by
\begin{align}
	q_\text{out} = \frac{A q_\text{in} + B}{C q_\text{in} + D}
\end{align}

If we have two actuators that are tunable with parameters $a_1$ and $a_2$ respectively, then the general ABCD expression for a mode matching actuator around some operating point is given by
\aac{double check if this is valid}
\begin{align}
	q_\text{out} = \frac{(A_0 + A_1 a_1 + A_2 a_2 ) q_\text{in} + (B_0 + B_1 a_1 + B_2 a_2)}{(C_0 + C_1 a_1 + C_2 a_2) q_\text{in} + (D_0 + D_1 a_1 + D_2 a_2)} + \bigO{a^2}
\end{align}
where the operating point is given by $\{A_0, B_0, C_0, D_0\}$.
For most optical systems there are enough degrees of freedom such that there are an infinite number of operating points that match $q_\text{in}$ to some target beam parameter $q_\text{target}$ such that $q_\text{out} = q_\text{target}$.
This is usually done by changing lengths between various optics.
In general the operating point of a mode matching actuator influences the actuator gains and their orthogonality.
It is desirable to find an operating point that satisfies $q_\text{out} = q_\text{target}$ and where the actuators are orthogonal and cover a large range of beam parameters.

\subsubsection{Actuator orthogonality}
In practice it is difficult to design orthogonal actuators, as there are only two sources of mode matching actuation: the position of a focusing optic, and the focusing strength of an optic.
These two types of actuation themselves are not orthogonal, which can be easily seen by noting that the position of an optic with no focal power has no effect on mode matching.
A pair of mode matching actuators can however be made to be orthogonal at a specific point in actuator space, for a specific input beam parameter.
While this point is difficult to find in general it is easy to check.
For example for two mode matching actuators controlled by actuation parameters $a_1$, and $a_2$ respectively the cross product between the two actuators in beam parameter space is given by
\begin{align}
	\Delta a_1 \times \Delta a_2 = \re{\Delta a_1} \im{\Delta a_2} - \im{\Delta a_1} \re{\Delta a_2}
\end{align}
where
\begin{align}
	\Delta a_1 &= \frac{\partial q_\text{out}}{\partial a_1} \\ \Delta a_2 &= \frac{\partial q_\text{out}}{\partial a_2}
\end{align}
are the gradients of the mode matching actuators in beam parameter space and $q_\text{out}$ is the beam parameter leaving the second mode matching actuator.
The two actuators are orthogonal if and only if the magnitude of this cross product $\abs{\Delta a_1 \times \Delta a_2}$ is not zero and is equal to the product of the magnitude of the actuator gradients
\begin{align}
	\abs{\Delta a_1 \times \Delta a_2}^2 = \abs{\Delta a_1}^2 \abs{\Delta a_2}^2
	\label{eq:mode_matching_actuator_orthogonality}
\end{align}
which is derived from the cross product version of the Cacuhy-Schwarz inequality
\begin{align}
	\abs{\bm{u} \times \bm{v}}^2 \leq \abs{\bm{u}}^2 \abs{\bm{v}}^2
\end{align}
\Cref{eq:mode_matching_actuator_orthogonality} is difficult to solve analytically even for simple ABCD models of mode matching actuators, but it is easy to evaluate numerically.
Transforming \cref{eq:mode_matching_actuator_orthogonality} into a figure of merit we arrive at
\begin{align}
	\xi_\text{orth} = \frac{\abs{\Delta a_1 \times \Delta a_2}^2}{\abs{\Delta a_1}^2 \abs{\Delta a_2}^2}
\end{align}
where $0 \leq \xi_\text{orth} \leq 1$, $\xi_\text{orth} = 1$ if the two mode matching actuators are exactly orthogonal, and $\xi_\text{orth} = 0$ if the two mode matching actuators are exactly degenarate.

\subsubsection{Actuator balance}
$\xi_\text{orth}$ alone isn't enough to determine a good mode matching actuator configuration, since the actuators can also be unbalanced in their response, for example the actuator $a_1$ can be orthogonal and produce 20 times the actuation of $a_2$. 
The amount of unbalance between the two actuators can be calculated by
\begin{align}
	\xi_\text{bal} = \frac{2 \abs{\Delta a_1} \abs{\Delta a_2}}{\abs{\Delta a_1}^2 + \abs{\Delta a_2}^2}
\end{align}
where $0 \leq \xi_\text{bal} \leq 1$, $\xi_\text{bal} = 1$ if the two mode matching actuators are exactly balanced, and $\xi_\text{bal} = 0$ if one of the actuators has no response.

Often the actuators have different actuation ranges and we want them to be balanced with respect to their actuation ranges.
The actuation range of actuator $a_1$ is given by $b_1 = a_{1,\text{max}} - a_{1,\text{min}}$, and similarly for $a_2$.
The balance figure of merit is then given by
\begin{align}
	\xi_\text{bal} = \frac{2 \abs{b_1 \Delta a_1} \abs{b_2 \Delta a_2}}{\abs{b_1 \Delta a_1}^2 + \abs{b_2 \Delta a_2}^2}
\end{align}

\subsubsection{Actuation area}
Another figure of merit is the effective actuation range, which we will estimate by the area of the ellipse formed by using the actuation gradients $\Delta a_1$ and $\Delta a_2$.
% In general these axes will not be orthogonal and so we will need to compute the orthogonal projection of $\Delta a_1$ onto $\Delta a_2$ or vice versa.
In general these axes will not be orthogonal and so we will need to compute the semi-axes of this ellipse using the singular value decomposition (SVD).
% For this it will be useful to use the complex dot product defined by
% \begin{align}
% 	\Delta a_1 \cdot \Delta a_2 = \re{\Delta a_1} \re{\Delta a_2} + \im{\Delta a_1} \im{\Delta a_2}
% \end{align}
\begin{align}
	\begin{bmatrix}
		\re{\Delta a_1} & \re{\Delta a_2} \\
		\im{\Delta a_1} & \im{\Delta a_2}
	\end{bmatrix} = \bm{U} \bm{\Sigma} \bm{V}^*
\end{align}
The area of the ellipse is then given by the product of the lengths of the semi-axes, or equivelantly the product of the singular values of the SVD.
\begin{align}
	a_\text{area} &= \pi \abs{\bm{\Sigma}_{11}} \abs{\bm{\Sigma}_{22}}
\end{align}
Unfortunately $a_\text{area}$ alone is a poor figure of merit on it's own. 
The problem is that the $a_\text{area}$ is potentially unbounded which means there are cases when the optimizer will sacrifice every other figure of merit to chase after marginal improvements in the area.
This can be controlled by making the figure of merit scale off of $(a_\text{area})^p$ where $p<1$ to limit the desirability of larger areas but this doesn't adress the fundamental issue that the area can be arbitrarily large.
What we want is a figure of merit that incentivizes larger actuation areas but only up to a point in a smooth and continuous way.
This can be achieved by the following transformation,
\begin{align}
	\xi_\text{area} = \exp\left[-\frac{p}{a_\text{area}}\right]
\end{align}
where $0 \leq \xi_\text{area} \leq 1$ as needed and $p$ now determines the largest actuation area before the diminishing returns set in.

\subsubsection{Mode overlap}
The final figure of merit describes how close the operating point is to some target beam parameter $q_\text{target}$.
For this the mode overlap of the operating point $q_\text{out}$ and the target $q_\text{target}$ is a natural measure.
\begin{align}
	\mathcal{O}\!\left(q_\text{out}, q_\text{target}\right) = 1 - \abs{\frac{q_\text{out} - q_\text{target}}{q_\text{out} - q_\text{target}^*}}^2
\end{align}
The overlap has the opposite problem to the actuation area in that it doesn't incentivize the optimizer enough and is generally overshadowed by other figure of merits unless a very large exponent is used.
We can aleviate this by using the following transformation on the overlap as the figure of merit,
\begin{align}
	\xi_\text{ovl} = \exp\left[-p\left(1 - \frac{1}{\mathcal{O}\!\left(q_\text{out}, q_\text{target}\right)}\right)\right]
\end{align}
where $0 \leq \xi_\text{ovl} \leq 1$ as needed and $p$ determines the desirability of good mode matching relative to other figure of merits.

\subsubsection{Overall figure of merit}
Combining the figure of merits we arrive at
\begin{align}
	\xi_\text{FoM} = \left(\xi_\text{orth}\right)^{p_1} \times \left(\xi_\text{bal}\right)^{p_2} \times \left(\xi_\text{area}\right)^{p_3} \times \left(\xi_\text{ovl}\right)^{p_4}.
	\label{eq:actuator_figure_of_merit}
\end{align}
$\xi_\text{FoM}$ encodes that a good pair of mode matching actuators are orthogonal, balanced, span a large area in beam parameter space, and are close to a target beam parameter, where the exponents $\{p_1, p_2, p_3, p_4\}$ determine the desirability of each component.
From our experimentation we find that $\{p_1, p_2, p_3, p_4\} = \{2, 2, 1, 2\}$ give reasonable results.
Finding a pair of mode matching actuators $a_1$ and $a_2$ that maximize $\xi_\text{FoM}$ usually yields a desired mode matching actuator. 

Cases where maximizing $\xi_\text{FoM}$ resulted in undesirable actuators could often be traced back to $\xi_\text{area}$, which unlike $\xi_\text{orth}$ and $\xi_\text{bal}$ is unbounded.
This means that in some cases the optimizer could and would sacrifice orthogonality and balance to get a really large $\xi_\text{area}$ to maximize $\xi_\text{FoM}$.
This could be fixed by decreasing $p_3$ or increasing $p_1$ and $p_2$, which would disinsentivize the optimizer from chasing after those solutions.

\aac{plot the figure of merit solution for mode matching telescope}

The other major problem with this figure of merit method is that sometimes the optimal solution would be impractical.
In practice we don't want the actuators to span a large range of beam parameters since even though it will contain the beam parameter we want it will also waste most of the actuator's range on reaching beam parameters we have no interest in.
Additionally the static offset of the mode matching actuators could be unreachable for some of the optimal solutions.
It is likely that these issues could be addressed by including more figures of merit in $\xi_\text{FoM}$ to push the optimizer in the direction we want, however this would increase the complexity of the figure of merit approach.
It is for this reason that we decided to switch to optimize the response of the mode matching actuators to match a target response.

\subsection{Target response design}
Often in design the desired outcome or response is known beforehand. 
That is, a good design is often easy to spot.
If we can quantify this desired target response across the actuation parameters then we can perform a least-squares minimization on the design parameters $\bm{\theta}$ to minimize the residual between the modelled response and the target response.
Mathematically this can be expressed as finding $\bm{\theta}$ that minimizes the following residual
\begin{align}
	r(\bm{\theta}) = \int^{x_\text{max}}_{x_\text{min}} \left| f_\text{model}(x,\bm{\theta}) - f_\text{target}(x) \right|^2 \dif x \label{eq:target_response_least_squares}
\end{align}
It is worth noting that the residual $r(\bm{\theta})$ will not follow the $\chi^2$ distribution as there is no random Gaussian noise on either the $f_\text{model}$ or $f_\text{target}$ responses.
This minimization therefore is only trying to minimize the model differences.
The least-squares residual metric is only optimal in the case of purely additive Gaussian white noise and no model differences.
In our case the minimization is guaranteed to find the design parameters $\bm{\theta}$ that minimizes the mean Eucledian distance between the modelled and target responses, but this may not be what we want.
We can modify \cref{eq:target_response_least_squares} by changing the exponent to instead minimize the mean Minkowski distance, a.k.a the $L_p$ norm.
It is given by
\begin{align}
	r(\bm{\theta}) = \int^{x_\text{max}}_{x_\text{min}} \left| f_\text{model}(x,\bm{\theta}) - f_\text{target}(x) \right|^p \dif x \label{eq:target_response_minkowski}
\end{align}
where $p \geq 1$. 
$p=1$ is the $L_1$ norm corresponding the Manhattan (taxicab) distance, $p=2$ is the $L_2$ norm corresponding to the Eucledian distance, and $p=\infty$ is the $L_\infty$ norm corresponding the Chebyshev distance.
Intuitively $p$ can be thought of as a parameter that determines how much should larger distances should be penalized.
At $p=\infty$ the optimizer will only minimize with respect to a single point $x$ that produces the largest difference between $f_\text{model}$ and $f_\text{target}$ and ignore every other point.

Aside from the Minkowski distances, the class of Bregman distances is also useful.
For any convex function $\phi(x)$, the associated Bregman distance between two points $a$ and $b$ is given by
\begin{align}
	B(a, b; \phi) = \phi(a) - \phi(b) - \phi'(b) (a-b) \label{eq:directed_bregman_definition}
\end{align}
For $\phi(x) = \frac{1}{2}x^2$ and $\phi'(x) = x$ the associated Bregman distance $B(a, b; \phi) = (a-b)^2$ is identical to the Euclidean distance metric.
For $\phi(x) = x \log(x) $ and $\phi'(x) = 1 + \log(x)$ the associated Bregman distance is
\begin{align}
	B(a, b; \phi) = a \log\left(\frac{a}{b} \right) - a + b \label{eq:kullback_liebler_bregman}
\end{align}
which is sometimes called the relative entropy distance or the generalized Kullback-Liebler divergence.
It is worth noting that \cref{eq:kullback_liebler_bregman} is not a valid distance metric in the formal sense since it violates 2 out of 3 of the axioms for a distance metric.

By definition any distance metric $d(a,b)$ satisfies the following 3 axioms
\begin{align}
	d(a,b) &= 0 \implies a=b & \text{(identity)}\\
	d(a,b) &= d(b,a) & \text{(symmetry)}\\
	d(a,c) &\leq d(a,b) + d(b,c) & \text{(triangle inequality)}
\end{align}

In general Bregman distances fail to satisfy symmetry meaning that the distance from $a$ to $b$ is in general not the same as the distance from $b$ to $a$.
This can be trivially checked with \cref{eq:kullback_liebler_bregman} by noting that $B(1, 2; \phi) = 0.31$ and that $B(2, 1; \phi) = 0.39$
The triangle inequality encodes the intuitive notion of distance that there is no shorter path between two points than a straight line.
Bregman distances in general also don't satisfy the triangle inequality, which can be confirmed using \cref{eq:kullback_liebler_bregman} again with $B(0, 1; \phi) + B(1, 2; \phi) = 1.31$ which is less than the $B(0, 2; \phi) = 2$

The lack of symmetry can be trivially adressed by defining a symmetric quantity from any Bregman distance using the symmetrization procedure on \cref{eq:directed_bregman_definition} which yields the following definition of the symmetric Bregman distance
\begin{align}
	B_\text{symm}(a,b;\phi) &= B(a,b;\phi) + B(b,a;\phi)\\
	&= -\phi'(b)(a-b) - \phi'(a)(b-a) \label{eq:symmetrized_bregman_definition}
\end{align}

While Bregman distances are easy to make symmetric, it isn't possible to make them satisfy the triangle inequality in general.
It is for this reason that the relative entropy distance in \cref{eq:kullback_liebler_bregman} is technically a dissimilarity measure rather than a proper distance metric.

Despite this the relative entropy distance, \cref{eq:kullback_liebler_bregman}, still sees frequent use in convex optimization, machine learning, and Bayseian inference.
This is because in the context of information theory applying the relative entropy distance to probability distributions yields the Kullback-Liebler divergence $D_\text{KL}$
\begin{align}
	D_\text{KL}(P,Q) = \int^\infty_{-\infty} P(x) \log\left(\frac{P(x)}{Q(x)}\right) \dif x
\end{align}
which can be interpreted as the amount of information lost when $Q$ is used to approximate $P$.
The relative entropy distance is therefore a suitable measure of difference between two models and the result of using it for minimizing the difference between the modelled and target respones is shown \aac{show results}.