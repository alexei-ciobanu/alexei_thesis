\chapter{Mode matching error signal}

\section{Gaussian beams}
The 1D complex amplitude of a Gaussian beam centered at $x=\delta$ travelling at angle~$\gamma$ to the z-axis with a waist size of~$w_0$ located at~$z=0$ is given by \cite{siegman_lasers_1986}
\begin{multline}
    u_{n}(x,q,\delta,\gamma) = \left(\frac{2}{\pi}\right)^{1/4} \left(\frac{1}{2^n n! w_0}\right)^{1/2} \left(\frac{\iu z_R}{q}\right)^{1/2} \left(\frac{-q^*}{q}\right)^{n/2} \\ 
    H_n \! \left(\frac{(x-\delta) \sqrt{2}}{w(z)} \right) \exp \! \left[\frac{-\iu \pi}{\lambda} \left(\frac{(x-\delta)^2}{q} + x\gamma\right)\right]
    \label{eq:u_n_q}
\end{multline}
where $q$ is the complex beam parameter \cite{siegman_lasers_1986}, which encodes all of the necessary information of a beam's shape as
\begin{align}
    q &= z + \iu z_R
    \label{eq:def_q}
\end{align}
where $z$ is the location of the beam waist, and $z_R = \pi w_0^2 / \lambda$ is the Rayleigh range of a beam with waist size $w_0$ and wavelength~$\lambda$.
The beam size $w(z)$ is given by
\begin{equation}
    w(z) = w_0 \sqrt{1 + \left(\frac{z}{z_R}\right)^2} .
\end{equation}
The complex amplitude of a 2D Hermite-Gaussian \tem{nm} beam with beam shape~$(q_x,q_y)$ can be given as a product of two 1D HG beams due to the separability of HG modes in Cartesian coordinates
\begin{equation}
    U_{nm}(x,y,q_x,q_y,\delta_x,\delta_y,\gamma_x,\gamma_y) = u_{n}(x,q_x,\delta_x,\gamma_x) \, u_{m}(y,q_y,\delta_y,\gamma_y) .
    \label{eq:u_nm_q}
\end{equation}
For brevity we will assume cylindrical symmetry ($q \!=\! q_x \!=\! q_y$) and no misalignments ($\delta_x\!=\!\delta_y\!=\!\gamma_x\!=\!\gamma_y\!=\!0)$ unless otherwise stated.
To compute the magnitude of coupling of mode mismatch to higher order \tem{} modes in section~\ref{sec:first_order_theory} and table \ref{tab:coupling_coefficients} it is convenient to rescale the degrees of freedom to the following relative quantities
\begin{align}
    \varepsilon_{\delta} &= \frac{\delta}{w_0}  & \varepsilon_{\gamma} &= \frac{\pi w(z) \gamma }{\lambda}  \\
    \varepsilon_{w_0} &= \frac{\Delta w_0}{w_0}   &  \varepsilon_{z} &= \frac{\Delta z}{z_R}.
\end{align}
For the mode matching degrees of freedom it is also convenient to define a relative change in the $q$ parameter from equation \ref{eq:def_q} as
\begin{equation}
    \varepsilon_{q} = \varepsilon_z + \iu \varepsilon_{z_R}
\end{equation}
where $\varepsilon_{z_R}$ is the relative change in Rayleigh range, which is given by
\begin{equation}
    \varepsilon_{z_R} = \frac{\Delta z_R}{z_{R,1}} = \varepsilon_{w_0}^2.
\end{equation}

\section{Basis change and projections}

To describe an incident beam with electric field given by $E(x,y)$ in terms of cavity eigenmodes it is necessary to find the amplitude coefficients $a_{n,m}$ of those modes that describe the incident beam by computing the following integral
\begin{equation}
    a_{n,m;q} = \iint_{-\infty}^{\infty} E(x,y) U_{n,m}^*(x,y,q) dy dx
    \label{eq:electric_field_eigenmode_decomposition}
\end{equation}
for all possible combinations of $n$ and $m$. 
However for models with a predominantly \tem{00} field and mismatches less than 10\% it is typically sufficient to compute all $a_{n,m}$ with $n+m \le 6$.

This lets us write an arbitrary electric field given by $E(x,y)$ as a sum of HG modes
\begin{equation}
    E(x,y) = \sum_{n,m} a_{n,m;q} U_{n,m}(x,y,q) .
    \label{eq:electric_field_reconstruction}
\end{equation}
To translate the set of $a_{n,m}$ coefficients from one basis $q_1$ to another we can substitute \eqref{eq:electric_field_reconstruction} into \eqref{eq:electric_field_eigenmode_decomposition} to obtain
\begin{equation}
    a_{n_2,m_2;q_2} = \sum_{n_1,m_1} a_{n_1,m_1;q_1} \iint_{-\infty}^{\infty} U_{n_1,m_1}(x,y,q_1) U^*_{n_2,m_2}(x,y,q_2) dy .
    \label{eq:2d_basis_change}
\end{equation}
The integral in \eqref{eq:2d_basis_change} is an overlap between two normalized HG modes from different bases
\begin{equation}
    k_{n_1,m_1,n_2,m_2;q_1,q_2} = \iint_{-\infty}^{\infty} U_{n_1,m_1}(x,y,q_1) U^*_{n_2,m_2}(x,y,q_2) dy dx .
    \label{eq:2d_k_definition}
\end{equation}
Due to the fact that HG modes are separable in Cartesian coordinates we can rewrite the \eqref{eq:2d_k_definition} as a product of two 1D integrals in the following way
\begin{equation}
    k_{n_1,m_1,n_2,m_2;q_1,q_2} = k_{n_1,n_2;q_1,q_2} \times k_{m_1,m_2;q_1,q_2}
    \label{eq:2d_k_separable}
\end{equation}
where the two 1D integrals are given by
\begin{align}
    k_{n_1,n_2;q_1,q_2} &= \int_{-\infty}^{\infty} u_{n_1}(x,q_1)u_{n_2}^*(x,q_2) dx\\
    k_{m_1,m_2;q_1,q_2} &= \int_{-\infty}^{\infty} u_{m_1}(y,q_1)u_{m_2}^*(y,q_2) dy
\end{align}
where if $q_1 = q_2$ both integrals reduce to
\begin{equation}
    k_{n_1,n_2;q_1,q_1} = \delta_{n_1,n_2}
\end{equation}
where $\delta_{n_1,n_2}$ is the Kronecker delta.
For brevity we will omit the $q$ parameters from $k_{n_1,n_2}$ unless multiple projections are being considered.
For $q_1 \neq q_2$  this integral can be computed either numerically or from an analytic solution \cite{bayer-helms_coupling_1984, brown_finesse_2014}.
In principle this integral needs to be computed for every possible combination of $n_1$ and $n_2$, but in practice for small mismatches one only needs to consider the integrals where \hbox{$|n_1-n_2|=2$}.
A table of the coupling coefficients to first order in mismatch and misalignment is included in table \ref{tab:coupling_coefficients}.

\begin{table}[!htb]
    \centering
    \begin{tabular*}{\linewidth}{@{\extracolsep{\fill} }lcr}
        \hline
        Coupling \mbox{Coefficient} & Expression & Notes \\
        \hline
        $k_{n_1,m_1,n_2,m_2}$ & $k_{n_1,n_2} \times k_{m_1,m_2}$ & 2D $\to$ 1D\\
        $k_{n,n}$ & $1-\frac{\iu}{4}\left(2n+1\right) \varepsilon_z +\bigO{\varepsilon^2}$ & phase shift \\
        $k_{n,n+1}$ & $\left(\varepsilon_\delta - \frac{q}{|q|} \varepsilon_\gamma \right)\sqrt{(n+1)} + \bigO{\varepsilon^2}$ & misalignment \\
        $k_{n,n+2}$ & $\frac{\iu}{4}\varepsilon_{q}\sqrt{\left(n+1\right)\left(n+2\right)} +\bigO{\varepsilon^2} $ & mismatch \\
        $k_{n_1,n_2}$ & $-k_{n_2,n_1}^* +\bigO{\varepsilon^2} $ & $n \neq m$ \\
        \hline
    \end{tabular*}
    \caption{\bf First Order Coupling Coefficients}
    \label{tab:coupling_coefficients}
\end{table}

In general the integral in \eqref{eq:2d_k_definition} appears whenever one wishes to convert the amplitudes of one set of eigenmodes to another set of eigenmodes. 
We refer to this operation as a \textit{change of basis}, or a \textit{projection} onto a basis, where the basis is parameterised by the complex beam parameter~$q$.

\section{Mode matching error signals}\label{sec:full_errsig_derivation}

\subsection{Mode matching error signal between two beams}\label{sec:two_beam_errsig_derivation}
The proposed mode matching error signal between two beams can be obtained considering a \tem{00} carrier and a beam modulation sideband with powers $P_{00}$ and ${P_{02}}$ respectively.
\begin{align}
    E &= \left[\sqrt{P_{00}} U_{00}(q_1) + \sqrt{P_{02}}(U_{20}(q_2) + U_{02}(q_2))e^{\iu\Omega t}\right] e^{\iu\omega_0 t}.
\end{align}
To compute the interference between the two we choose to project the beam modulation sideband into the carrier basis, which results in an RF \tem{00} component in the carrier basis given by the $k_{0200}$ scattering coefficient from \eqref{eq:2d_k_definition} to first order in mismatch.
\begin{align}
    E &= \Big\{\sqrt{P_{00}} U_{00}(q_1) + \sqrt{P_{02}}\Big[ 2 k_{0200}(U_{00}(q_1)) \nonumber \\
    & \hspace{3em} + k_{0202}(U_{20}(q_1) + U_{02}(q_1)) \Big] e^{\iu\Omega t}\Big\} e^{\iu\omega_0 t} + \bigO{\varepsilon^2}.
\end{align}
The mode matching error signal $\mathcal{Z}$ is then obtained by demodulating the intensity of the combined beams at the offset frequency $\Omega$, which gives
\begin{equation}
    \mathcal{Z}_\text{beam} = 2\sqrt{P_{00}P_{02}} \left( k_{0200}^* \right) + \bigO{\varepsilon^2}
\end{equation}
where we dropped the $k_{0202}$ terms because they do not beat with the \tem{00} carrier as they are now in the same $q_1$ basis, and hence orthogonal.
We can break up the $k_{0200}$ 2D coupling coefficient into a product of two 1D scattering coefficients due to the fact that HG modes are separable in Cartesian coordinates
\begin{equation}
    \mathcal{Z}_\text{beam} = 2\sqrt{P_{00}P_{02}} \left( k_{20}^* k_{00}^* \right) + \bigO{\varepsilon^2} .
\end{equation}
Here we can substitute the 1D coupling coefficients with their first order approximation from table~\ref{tab:coupling_coefficients}
\begin{equation}
    \mathcal{Z}_\text{beam} = -\iu \sqrt{P_{00}P_{02}} \frac{\sqrt{2}}{2} \left( \varepsilon_{q} + \frac{\iu}{4} \varepsilon_{q} \varepsilon_{z} \right) + \bigO{\varepsilon^2}.
\end{equation}
We can drop the $\varepsilon_{q} \varepsilon_{z}$ term as it is quadratic in $\varepsilon$.
The overall factor of $\iu$ can be ignored if the absolute demodulation phase is not relevant.
This leaves us with the final form of the mode matching error signal between two beams that was stated in \eqref{eq:beam_to_beam_errsig}
\begin{equation}
    \mathcal{Z}_\text{beam} = -\iu \sqrt{P_{00}P_{02}} \frac{\sqrt{2}}{2} \varepsilon_{q} + \bigO{\varepsilon^2}.
\end{equation}

\subsection{Mode matching error signal for resonant cavity}
Consider a beam consisting of a carrier \tem{00} and sideband \tem{02} both in basis $q_1$ incident on a cavity with eigenmode basis $q_{\text{cav}}$.
In order to compute the cavity reflected field it is necessary to project the incident beam into the $q_{\text{cav}}$ basis.
The incident electric field of the incident beam in the $q_{\text{cav}}$ basis is
\begin{align}
    E_{\text{inc}} &= \Big\{ U_{00}(q_{\text{cav}}) \left[\sqrt{P_{00}} k_{0000} + \sqrt{P_{02}} e^{\iu\Omega t} \big( k_{0200} + k_{2000} \big)\right] \nonumber \\
    & \qquad + U_{02}(q_{\text{cav}}) \left[\sqrt{P_{00}} k_{0002} + \sqrt{P_{02}} k_{0202} e^{\iu\Omega t} \right] \nonumber \\
    & \qquad + U_{20}(q_{\text{cav}}) \left[\sqrt{P_{00}} k_{0020} + \sqrt{P_{02}} k_{2020} e^{\iu\Omega t} \right] \Big\}e^{\iu\omega_0 t} \nonumber \\ 
    & \qquad + \bigO{\varepsilon^2}
\end{align}
On reflection all components are promptly reflected with the exception of the resonant mode and frequency, that being the \tem{00} mode at the carrier frequency $\omega_0$, which picks up a cavity reflection $R_{\text{cav}}$ defined in equation~\refeq{eq:cav_refl_coeff}.
The reflected electric field is then
\begin{align}
    E_{\text{refl}} &= \Big\{ U_{00}(q_{\text{cav}}) \left[ R_{\text{cav}} \sqrt{P_{00}} k_{0000} + \sqrt{P_{02}} e^{\iu\Omega t} \big( k_{0200} + k_{2000} \big)  \right] \nonumber \\
    & \qquad + U_{02}(q_{\text{cav}}) \left[\sqrt{P_{00}} k_{0002} + \sqrt{P_{02}} k_{0202} e^{\iu\Omega t} \right] \nonumber \\
    & \qquad + U_{20}(q_{\text{cav}}) \left[\sqrt{P_{00}} k_{0020} + \sqrt{P_{02}} k_{2020} e^{\iu\Omega t} \right] \Big\}e^{\iu\omega_0 t} \nonumber \\ 
    & \qquad + \bigO{\varepsilon^2}
\end{align}
To compute the error signal amplitude we now demodulate at the sideband frequency.
The complex demodulated amplitude is then
\begin{align}
    \mathcal{Z_{\text{cav}}} &= \sqrt{P_{00}P_{02}} \Big[ R_{\text{cav}} \big( k_{0000}k_{0200}^* + k_{0000}k_{2000}^* \big)  \nonumber\\
    & \qquad \qquad  + k_{0002}k_{0202}^* + k_{0020}k_{2020}^*\Big]+ \bigO{\varepsilon^2}. 
\end{align}
Here we can use the $k_{n,n}$ entry in table \ref{tab:coupling_coefficients} to note that to first order in $\varepsilon$ the following equalities hold: $k_{0000} = k_{2020} = k_{0202} = 1$, $k_{0002} = k_{0020} = k_{02}$, and $k_{0200} = k_{2000} = k_{20}$.
Applying those simplifications leaves us with
\begin{equation}
    \mathcal{Z}_{\text{cav}} = \sqrt{P_{00}P_{02}} \left[2 R_{\text{cav}} k_{20}^* + 2 k_{02} \right] + \bigO{\varepsilon^2}.
\end{equation}
We can simplify further by using the last entry of table \ref{tab:coupling_coefficients} to note that $k_{02} = -k_{20}^*$, leaving us with
\begin{align}
    \mathcal{Z}_{\text{cav}} &= -2\sqrt{P_{00}P_{02}} \left[R_{\text{cav}} k_{02} - k_{02} \right] + \bigO{\varepsilon^2}\\
    &= -2\sqrt{P_{00}P_{02}} \left(R_{\text{cav}} - 1\right) k_{02} + \bigO{\varepsilon^2}.
\end{align}
We can substitute the $k_{02}$ with its first order approximation from table \ref{tab:coupling_coefficients} to obtain the result quoted in \eqref{eq:cav_mm_errsig}
\begin{equation}
    \mathcal{Z}_{\text{cav}} = -\iu \sqrt{P_{00}P_{02}} \left(R_{\text{cav}} - 1 \right) \frac{\sqrt{2}}{2}\varepsilon_{q} +\bigO{\varepsilon^2}.
\end{equation}

\section{Shot Noise} \label{sec:shot_noise}
For a single demodulation of a single optical sideband, the single sided shot noise PSD is given by \cite{harms_quantum-noise_2007}
\begin{align}
    S(f) = 2 \p \hbar \p c \p k \p P
\end{align}
where $P$ is the average incident DC optical power. 
The root mean square (RMS) noise is then given by
\begin{align}
    \text{RMS}^2 &= \int_{0}^{\Delta f} S(f) df\\
    \text{RMS} &= \sqrt{2 \p \hbar \p c \p k \p P \Delta f}\\
    &= \sqrt{\frac{2 \p \hbar \p c \p k \p P}{T}}
\end{align}
where $\Delta f = 1/T$ is the integration bandwidth and $T$ is the integration time in seconds.
The signal to noise ratio is then given by
\begin{align}
    \text{SNR} &= \frac{|\mathcal{Z}|}{\text{RMS}} \\
    &= \frac{|\mathcal{Z}|}{\sqrt{2 \p \hbar \p c \p k \p \left(P_{00} + P_{02}\right)}} \sqrt{T}\\
    &= \frac{\sqrt{P_{00}P_{02}}}{\sqrt{{2 \p \hbar \p c \p k \p \left(P_{00} + P_{02}\right)}}} \left|(R_{\text{cav}} - 1) \frac{\sqrt{2}}{4} \varepsilon_q \right| \sqrt{T}
\end{align}
Evaluating the constants and assuming an impedance matched cavity $(R_{\text{cav}} = 0)$ results in
\begin{align}
    \text{SNR} &= 5.8\times10^{8} \; |\varepsilon_q| \left( \frac{\sqrt{P_{00}P_{02}}}{\sqrt{{P_{00} + P_{02}}}} \right) \sqrt{T} .
\end{align}
Substituting in the measured powers in the tabletop experiment $P_{00} = 2 mW$, $P_{02} = 20 \mu W$ gives us
\begin{align}
    \text{SNR} &= 2.6\times10^6 \; |\varepsilon_q| \sqrt{T} .
\end{align}
We can convert the SNR into units of mode mismatch by substituting \eqref{eq:eps_q_to_M}
\begin{align}
    \text{SNR} &= 1.7\times10^{12} \; \mmp \times T .
\end{align}
This translates to sub-ppm of mode mismatch even with a 1~$\mu$s integration time.
We conclude that in practice technical noise sources and not shot noise will be relevant to the SNR of the mode matching error signal.

% \section{Experimental construction}

% Here we concern ourselves with the justification of the experimental setup depicted in \cref{fig:experiment_setup}.
% The design of a physics experiment is surprisingly complicated and often imprecise.
% We begin by considering what we want to show or demonstrate.
% This would be the equivalence between the presence second order HG modes and a change of HG basis for an \tem{00} beam.
% It is commonly understood by optical experimentalists that mode matching losses in a cavity occur because the input \tem{00} beam is scattered to higher order modes by the cavity.
% What I was interested in was a related problem: can I induce mode mismatch by adding higher order modes to a beam? 

% At the time at LIGO people around me were concerned with mode matching in the interferometer.
% They were measuring OMC cavity scans and inferring the mode matching by the relative difference between the resonance of the \tem{00} and second order modes inside the OMC.
% A standard and reliable technique.
% As part of my contribution to the commissioning effort I was tasked with running the analysis on these OMC cavity scans and extracting the magnitude of the cavity resonances for estimating mode mismatch.
% This task was rather labor intensive and surprisingly difficult to automate.
% During that entire time I was often wishing for a better method of measuring mode matching.
% The cavity scan in addition to being laborious was also offline, meaning that it couldn't be performed while the interferometer was running.

% This was all around the start of my PhD and I had just come to grips with the PDH technique for locking a cavity's length to a laser's frequency or vice versa. 
% This PDH method is used all over LIGO extensively and works to bring and hold the interferometer on resonance.
% I am oversimplifying of course, the details behind LIGO's specific implementation of PDH spans over several PhDs worth of work.
% But this simple picture helps motivate this discussion forward.
% The idea is not my own, the credit here goes to Eric Black who wrote an excellent paper on the basics of PDH.
% Fundamentally, the point of PDH is to induce a known phase modulation via an EOM into a beam to serve as a guide for locking a cavity length to a laser's frequency. 

% I had also recently read Paul Fulda's work on the EOBD, which proposed a special type of EOM to induce not a phase modulation but an alignment modulation into a beam. 
% Then this alignment modulation generates an alignment error signal to lock a cavity axis to a beam's propagation axis or vice versa just like the PDH phase modulation generates a length error signal.
% Having been familiar with the basics HG modes and that misalignment is related to first order modes I began wondering if this relationship extended to mode mismatch and second order modes.
% To test this I quickly whipped up a Finesse2 model of beam with second order mode sidebands incident on a mode mismatched cavity and to my delight found what I had predicted: the reflection off the cavity had an intensity modulation that changed with mode mismatch in a way that looked like an error signal.

% Upon my discovery of the mode matching error signal I had two questions to answer: how to generate this second order mode sideband and is this a useful error signal.
% The usefulness of this error signal could be rephrased into does following this error signal always take you to the optimal mode matching point.
% When I first tackled this problem I spent an embarassingly long amount of time running Finesse simulations with different parameters to spot some kind of pattern that would tell me if this error signal works.
% The solution I found was to take a step back and to look at the equations that are being solved by Finesse.
% This led to a relatively simple solution. The error signal was a complex function and so if it has exactly one zero then a simple gradient descent starting from any point is guaranteed to reach that zero.
% The usefulness of the error signal could then be rephrased more concretely into: does the mode matching error signal function have only one zero, and does this zero line up with the optimal mode matching point.
% The answer to both is yes under ideal conditions, but when imperfections to the modulation are introduced the zero of the error signal becomes offset from the optimal point something that was also demonstrated in the experiment.

% The generation of the second order mode sidebands was not a straightforward path.
% I began with taking the same approach for Fulda~\cite{fulda_alignment_2017} and to design a custom built EOM that would produce second order sidebands.
% This proceeded all the way to the creation of a model of a lithium niobate crystal sandwiched between two parabolic electrodes that would create a specific electric field inside the crystal that would scatter an incoming \tem{00} beam into second order mode sidebands.
% The electrodes and the subsequent electric field were all modelled in COMSOL, a finite element modelling package.
% The electro-optical interaction with the lithium niobate crystal was modelled with equations I got from a nonlinear optics textbook.
% It was at the stage where we began writting the budget for the required materials when I realised that everything I had done was unnecessary and that the required second order mode sidebands can simply be generated with a regular commercial EOM and a cavity.

% This is when I began to be familiar with HG mode scattering coefficients and using the full solution to said coefficients provided by Bayer-Helms~\cite{bayer-helms_coupling_1984}. 
% It had dawned on me that mode matching losses in a cavity are only losses if you look at them that way.
% A cavity can be used to lock onto a higher order mode, in which case everything else becomes a loss.
% This of course extends to locking onto sidebands and even higher order mode sidebands.

% \section{Liquid lens}
