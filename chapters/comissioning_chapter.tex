\chapter{Commissioning for Advanced LIGO}
\label{chp:commissioning}
This chapter describes the various contributions to commissioning efforts for Advanced LIGO that I undertook during my PhD.
For LIGO commissioning periods are between observation runs where the detector is adjusted or modified to improve performance.
I was involved in LIGO commissioning in 2018 and 2019 with a focus on modelling and understanding the issues related to mode matching between the various LIGO subsystems. 
The two main activities presented here were the analysis of OMC astigmatism from measurements of the OMC transverse mode splitting, and a measurement of the SR3 rear heater's ability to correct mode mismatch.

\section{Squeezer beam and LHO OMC astigmatism}
\label{sec:sqz_LHO_astigmatism}
During commissioning leading up to LIGO's third observation run (O3) it was found that the squeezer beam was noticeably astigmatic going into the OMC~\cite{daniel_brown_opo_2018}.
This limits the overall mode matching that can be achieved and generally degrades squeezing performance.
While the squeezer beam was profiled I analyzed the OMC cavity scans to quantify the mode mismatch and investigate how it was impacted by the beam astigmatism.
The mode mismatch is obtained by measuring the ratio of the second order mode peak height to the fundamental mode peak height during a cavity length scan.
\begin{figure}[!htb]
	\centering
	\includegraphics[width=\textwidth]{LHO_full_OMC_scan.pdf}
    \caption{A full period of an LHO OMC cavity length scan of the squeezer beam. All known peaks are labelled based on the relative location of higher order mode resonances based on OMC cavity geometry. The label \tem{0n} is intended to denote all the n'th order higher modes that should resonate at, or close to that frequency.}
    \label{fig:LHO_full_OMC_scan}
\end{figure}

One of the OMC scans I took is shown in \cref{fig:LHO_full_OMC_scan}.
The ratio between \tem{02} and \tem{00} peaks is roughly 0.1, which yields a mode mismatch of~10\%.
The mode order of a resonant peak is determined by their relative distance to the fundamental mode peak and the design parameters of the OMC cavity geometry.
The incident beam is single frequency and so every peak in the cavity scan corresponds to a higher order transverse mode of the incident beam in the OMC eigenmode basis.

If the OMC was operating to design the three second order peaks (\tem{02}, \tem{11}, \tem{20}) should be nearly degenerate and hence should not be resolvable. 
In this case when the incident beam is astigmatic the ratio between the fundamental and second order peaks should give an estimate for the average mode mismatch between the vertical and horizontal axes, but this was not what I found.
Instead the resonant power around the resonance for the second order modes took an odd shape, which remained consistent from scan to scan. 

\begin{figure}[!htb]
	\centering
	\includegraphics[width=1.0\textwidth]{single_fit.pdf}
	\caption{The second order OMC resonance with a strongly astigmatic beam.}
	\label{fig:single_lotrentz_fit}
\end{figure}

A close-up on the second order resonance peak is shown in \cref{fig:single_lotrentz_fit} along with a least-squares fit of a Lorentzian function, which closely approximates the line shape of a single mode or degenerate multi-mode cavity resonance. 
The Lorentzian function with a full-width-half-maximum (FWHM) $\Gamma$ is given by
\begin{align}
	L(x,\Gamma) = \frac{\left( \frac{\Gamma}{2} \right)^2}{x^2 + \left( \frac{\Gamma}{2} \right)^2 }
\end{align}
This definition is chosen such that the maximum value of the Lorentzian is $L(0,\Gamma) = 1$.

The single Lorentzian fit in \cref{fig:single_lotrentz_fit} is quite bad making it difficult to accurately predict the resonant amplitude.
Introducing a second Lorentzian function improves the fit substantially, as shown in \cref{fig:double_lotrentz_fit}.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=1.0\textwidth]{double_fit.pdf}
	\caption{Two Lorentzians fitted to a second order OMC resonance with a strongly astigmatic beam.}
	\label{fig:double_lotrentz_fit}
\end{figure}

The two peaks in \cref{fig:double_lotrentz_fit} correspond to the \tem{20} and \tem{02} modes respectively.
They are resolvable because the OMC cavity is more astigmatic than the design, which breaks the frequency degeneracy for higher order modes.
The resonance frequency for vertical higher order modes is slightly shifted with respect to the horizontal higher order modes.
The difference in the heights of these two peaks gives a measure of beam astigmatism, and the separation frequency of these two peaks gives a measure of the cavity astigmatism.

This cavity astigmatism can be rephrased in terms of splitting of the transverse mode spacing (TMS), which can be directly computed from the fits in \cref{fig:double_lotrentz_fit}.
The TMS splitting is a unitless quantity given by
\begin{align}
	\text{TMS splitting} = \frac{|x_{02} - x_{20}|}{2 \Gamma}
    \label{eq:TMS_splitting}
\end{align}
where $x_{02}$ and $x_{20}$ are the locations of the \tem{02} and \tem{20} resonance peaks respectively.
The factor of 2 on the denominator comes from the fact that we are measuring the TMS splitting using second order modes.
The TMS splitting from \cref{fig:double_lotrentz_fit} is about 0.43.
To convert it to a frequency splitting the TMS splitting should be multiplied by the FWHM of the OMC.

In principle in \cref{fig:double_lotrentz_fit} there should be a another higher order mode: the \tem{11} mode, whose resonance frequency sits exactly between the \tem{20} and \tem{02} modes.
However the amplitude of the \tem{11} mode was small enough here as to be negligible.
This can be justified by noting that the scattering into the \tem{11} mode primarily happens through misalignment; mode mismatch only changes the strength of the misalignment coupling~\cite{bayer-helms_coupling_1984}.
Similar to mode mismatch, misalignment is obtained by taking the ratio of the \tem{01} peak to the \tem{00} peak in a cavity length scan. 

\Cref{fig:LHO_full_OMC_scan} shows that the \tem{01} peak is about an order of magnitude smaller than the \tem{02} peak.
Since the mode mismatch was 10\% this indicates that the beam was misaligned to the cavity to less than 1\%.
The peak height for the \tem{11} mode is expected to be around $10^{-4}$~W, less than the noise floor and so its contribution to the profile of the second order peak in \cref{fig:double_lotrentz_fit} is negligible.

The least-squares fits to cavity scan peaks are accurate but difficult to automate for commissioning due to requiring human expert knowledge to provide a reasonable guess for initial fit parameters.
Cases like \cref{fig:LHO_full_OMC_scan} are relatively simple for an expert where the beam has a single frequency and is well mode matched and aligned. 
Despite that automatically identifying peaks is challenging even in simple cases since the relationship between PZT voltage and cavity length is not exactly linear. 
This shifts the position of the peaks relative to each other and can result in misidentified peaks. 

These challenges make computer aided tools unappealing in comparison to estimating the resonant amplitude by the maximum height of the resonant peak, especially when quick answers important to keep up the pace of commissioning.
Estimating mode mismatch with peak heights in this case produces systematic underestimation as illustrated by \cref{fig:split_lorentzian_sum_comparison}.
\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.85\textwidth]{lorentzian_comparison.pdf}
	\caption{A demonstration that the peak of the sum of two Lorentzians is reduced if the Lorentzians are not on top of each other.
	The Lorentzian separation used here is the same as was measured in the LHO OMC cavity scan in \cref{fig:double_lotrentz_fit}.}
	\label{fig:split_lorentzian_sum_comparison}
\end{figure}

The maximum of the split peak in \cref{fig:split_lorentzian_sum_comparison} is about 1.6 times less than if the OMC had no astigmatism, which means that mode mismatch estimates need to be multiplied by that amount to compensate.
This correction factor is not constant and depends on the relative height of the two peaks, which in turn depends on the astigmatism of the beam relative to the cavity.
The correction factor as a function of the relative height difference between the second order peaks is shown in \cref{fig:OMC_mismatch_correction_factor}.
\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.85\textwidth]{OMC_mismatch_correction_factor.pdf}
	\caption{A plot of the peak height correction factor as a function of relative height difference between the two peaks. The Lorentzian separation used here is the same as was measured in the LHO OMC cavity scan in \cref{fig:double_lotrentz_fit}.}
	\label{fig:OMC_mismatch_correction_factor}
\end{figure}
Zero mode mismatch symmetry corresponds to the height of one of the \tem{02} or \tem{20} to be exactly zero, in which case the height of the remaining peak gives an accurate estimate of mode mismatch and hence the correction factor is exactly 1.
A mode mismatch symmetry of 1 corresponds to the \tem{02} and \tem{20} peaks having the same height, which is the case depicted in \cref{fig:split_lorentzian_sum_comparison} with the correction factor of 1.6.
In general the mode mismatch symmetry is given by
\begin{align}
	\text{mode mismatch symmetry} = \frac{\min{P_{02}, P_{20}}}{\max{P_{02}, P_{20}}}
\end{align}
where $P_{02}$ and $P_{20}$ are the powers in the \tem{02} and \tem{20} modes respectively.

\subsection{Comparison of LHO and LLO astigmatism}
Here we compare the astigmatism of the LHO OMC to the LLO OMC using the same method as \cref{sec:sqz_LHO_astigmatism}.
A sample of second order resonance peaks from both detectors was extracted from OMC cavity scans performed by on-site commissioners for a different purpose in early 2019.
These are shown in \cref{fig:LHO_LLO_second_order_peaks}.
The LHO and LLO cavity scans were ran for six and two cycles respectively.
Two Lorentzians were fit to each resonance peak and the residuals are shown in \cref{fig:LHO_LLO_second_order_peak_residuals}.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{second_order_peak_LHO_LLO.pdf}
    \caption{A plot of OMC second order resonance peaks in the LHO (left) and LLO (right) GW detectors.}
    \label{fig:LHO_LLO_second_order_peaks}
\end{figure}
\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{second_order_peak_LHO_LLO_residuals.pdf}
    \caption{Residuals for the Lorentzian fits to the second order OMC resonance peaks shown in \cref{fig:LHO_LLO_second_order_peaks}. 
    The colors of the traces correspond to the same traces from \cref{fig:LHO_LLO_second_order_peaks}.}
    \label{fig:LHO_LLO_second_order_peak_residuals}
\end{figure}

We find that the LLO OMC is significantly less astigmatic than the LHO OMC, and slightly less astigmatic than the aLIGO OMC design.
It is known that the OMCs in LHO and LLO differ slightly in their as built parameters, though direct measurements of these parameters in-situ is difficult.
This measurement of OMC TMS splitting gives a way to directly measure the impact of some of these parameters. 
We find that the TMS splitting in LHO and LLO are 0.44 and 0.17 respectively.
A summary plot of the measured OMC TMS splitting is shown in \cref{fig:LHO_LLO_TMS_splitting}.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.75\textwidth]{LHO_LLO_OMC_TMS_splitting.pdf}
    \caption{Calculated OMC TMS splitting using \cref{eq:TMS_splitting} for both LHO and LLO based on the separation of the two fitted Lorentzians to the transverse second order OMC resonances in \cref{fig:LHO_LLO_second_order_peaks}.
    The dashed line is the predicted TMS splitting calculated by Finesse based on the aLIGO design parameters for the OMC.}
    \label{fig:LHO_LLO_TMS_splitting}
\end{figure}

The time of the measured peaks was used to obtain the PZT voltage on PZT2, which was used to scan the OMC cavity length.
The other PZT in the OMC --- PZT1 was set to 10~V is both detectors during that time.
The nearby points in the LHO data show the reproducibility of our method, which we estimate is about $\pm 0.02$.
The uncertainty estimates for each measurement point based on the least squares fits are orders of magnitude less, indicating some degree of model misspecification.

Based on the results in \cref{fig:LHO_LLO_second_order_peak_residuals} we conclude that that the TMS splitting and hence OMC astigmatism does not strongly depend on the applied PZT voltage in both detectors.
This dependence broadly agrees with results published by Arai~\cite{koji_arai_aligo_2015}, who measured the TMS splitting in both OMCs prior to them being installed in each detector.
His measured value for the TMS splitting for all OMCs was about 0.218, which is closer to the aLIGO design than either the measured LLO or LHO TMS splitting shown in \cref{fig:LHO_LLO_TMS_splitting}.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.75\textwidth]{OMC_mirror_angle_vs_TMS_splitting.pdf}
    \caption{OMC TMS splitting as a function of angle of incidence on the OMC curved mirrors (CMs). The red dashed line is the measured LLO TMS splitting and the red solid line is the measured LHO TMS splitting.}
    \label{fig:OMC_mirror_angle_vs_TMS_splitting}
\end{figure}

To discuss this measured difference it is important to note that the TMS splitting is only dependent on the cavity geometry, which is given by the lengths between each mirror, the mirror radius of curvature, and angle of incidence (AOI) on each mirror.
Out of any of these the AOI seems the most likely to differ from design.
The OMC input coupler (IC) and output coupler (OC) are flat and so their AOI does not directly contribute to the TMS splitting. 
Changing the angle of the IC and OC can change the cavity round trip beam path so that the beam hits the CMs off-center, which means the beam will see a different angle and hence lead to a change in the TMS splitting.
We neglect this effect and only look at the TMS splitting purely as a function of AOI on the CMs to get a sense of scale of the TMS splitting in more concrete terms.
In \cref{fig:OMC_mirror_angle_vs_TMS_splitting} we show how the TMS splitting changes when the AOI on the curved mirrors (CM) changes in an aLIGO design OMC.
We see that to get the TMS splitting that was measured for LHO the AOI on both CMs has to increase from their design by about 1.5 degrees.
Similarly the LLO TMS splitting can be obtained by decreasing both the AOI on both CMs by about 0.5 degrees.

At the time of writing we do not know how this change in TMS splitting came about in the time between being measured by Arai before installation and the in-situ measurements we presented here.
The difference that we report is small and likely has a negligible impact on the mode cleaning capabilities of either OMC.

% A likely explanation behind the difference in measured TMS splittings was that the astigmatism of the OMCs was changed during or after installation into the detectors.
% \aac{look into a monte carlo of mirror misalignments in finesse3}

\section{Beam shape inference from SR3 rear heater scans}
\label{sec:SR3_ring_heater_scan}
As part of the commissioning effort for the third observation run (O3) I investigated how the SR3 rear heater impacted the mode matching from the SRC to the OMC.
High mode matching is important for minimizing squeezing losses and at the time the SR3 rear heater provided the only method of mode matching actuation from inside the signal recycling cavity (SRC).
During this measurement the interferometer was in a ``single bounce'' state, where the PRM, SRM, ITMX, ETMX, and ETMY are all misaligned in \cref{fig:aligo_DRFPMI}.
This allows us to analyze the impact of the SR3 rear heater on a beam that somewhat approximates the arm cavity mode without involving any cavities in the interferometer. 
This allows us to operate without worrying about lock loss.
In practice, the dual recycled Fabry-Perot Michelson beam will differ in shape to the ``single bounce'' beam due to mode mismatches between the internal cavities within the interferometer, but the ``single bounce'' beam should be a good approximation when all the cavities inside the interferometer are well mode matched.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{drmi_full.pdf}
    \caption{Dual recycled Fabry-Perot Michelson (DRFPMI) layout of the aLIGO interferometer.}
    \label{fig:aligo_DRFPMI}
\end{figure}

The SR3 rear heater --- shown in \cref{fig:sr3_rear_heater} is a radiative heater mounted directly against the back surface of the SR3 mirror.
The heater has a max operating temperature of 360~K, or about 86$^\circ$C, which is modelled to increase the radius of curvature of the SR3 mirror by 32~mm via thermoelastic deformation~\cite{brooks_sr3_2015}.
The nominal radius of curvature of the SR3 is roughly 36~m and so the rear heater is equivalent to a 25~$\mu$D lens.
The thermal time constant of the heater is about 15 minutes, which is the time it takes for the difference between the current temperature and the target temperature to drop by $1/e$.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.4\textwidth]{sr3_rear_heater.jpg}
    \caption{A diagram of the SR3 rear heater. Taken from~\cite{brooks_sr3_2015}.}
    \label{fig:sr3_rear_heater}
\end{figure}

The rear heater temperature was steadily increased from 25 to 70$^\circ$C over the period of about 4 hours.
During that time the OMC cavity length was continuously scanned by driving the PZT with a sawtooth wave voltage signal.
The mode mismatch into the OMC was estimated by a peak detection algorithm running on the realtime data.
This peak detection algorithm picked out the \tem{00} and second order resonance peak height each OMC scan period by a combination of heuristics and user guidance.
The initial guess for the position of the \tem{00} and second order peak positions are specified by the user.
The heuristic was derived from the assumption that the PZT voltage of each OMC resonance peak changes very little from scan to scan, and so each scan period the new peaks were acquired using their previously recorded PZT voltage.
The raw data for the SR3 heater element temperature and the OMC mode mismatches as a function of time are shown in \cref{fig:SR3_heater_scan_GPS_time}.
\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{SR3_heater_scan_GPS_time.pdf}
    \caption{Plot of SR3 heater element temperature as a function of time (left). Plot of measured mode mismatch into the OMC as a function of time during that period (right).}
    \label{fig:SR3_heater_scan_GPS_time}
\end{figure}
The SR3 heater element temperature was set by hand by setting the target temperature and waiting for the rear heater equilibrate before increasing the target temperature again. The break in the data is in \cref{fig:SR3_heater_scan_GPS_time} corresponds to a lunch break when the experiment was not running.

After the experiment was finished the measured OMC mismatch was plotted against the SR3 heater element temperature, which is shown in \cref{fig:SR3_heater_scan_mismatch_vs_temperature}.
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.75\textwidth]{SR3_heater_scan_mismatch_vs_temperature.pdf}
    \caption{A plot of measured OMC mismatch as a function of SR3 heater element temperature.}
    \label{fig:SR3_heater_scan_mismatch_vs_temperature}
\end{figure}
The noise in the mode mismatch data was likely caused by intensity noise on the laser, which was not intensity stabilized throughout the experiment.
Intensity noise has an effect on mode mismatch measurements since the intensity readings for the \tem{00} and \tem{20} peaks happen at different times, typically seconds apart, which means that the noise is uncorrelated. 
This means that the impact of intensity noise on the measured mode mismatch is significant.

However, despite the noise, through the sheer number of data points a trend is visible in \cref{fig:SR3_heater_scan_mismatch_vs_temperature}.
The data shows a near linear decrease in mode mismatch from about 10.5\% at 25$^\circ$C to 9.75\% at 65$^\circ$C.
This decrease in OMC mode mismatch due to increase in SR3 curvature is small, but the fact that it is small over that range can be used to infer information about the beam parameter going in to the OMC.
A parameter map of how 10\% mode mismatched beam parameters at the SRM change as the SR3 radius of curvature is increased is shown in \cref{fig:SR3_heater_mismatch_contrours}.
\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{SR3_scan_mismatch_contours.pdf}
    \caption{A map in beam parameter space at the SRM close to mode matching to the OMC. Contours are shown for 0.1\%, 1\%, 5\%, 10\% mode mismatches. The region highlighted in pink corresponds to the area where the SR3 actuator has a small improvement on the mode matching of a 10\% mismatched beam, which is similar to what was seen in the measurement in \cref{fig:SR3_heater_scan_mismatch_vs_temperature}. This means the measured beam parameter is likely to be from that region.}
    \label{fig:SR3_heater_mismatch_contrours}
\end{figure}
This map was generated by taking a uniform sample of beam parameters that are 10\% mode mismatched to the OMC eigenmode.
What we would like to know is what these beam parameters would look like right before the SR3 so that we can model how they change when the SR3 curvature is increased.
For this we need to reverse propagate the beam at the OMC to the SR3 using inverse ABCD matrices.

After the mismatched OMC beam parameters are inverse propagated back to the SR3, the SR3 curvature is changed and the beam is forward propagated to the SRM, which is a common reference point for OMC mode matching used in commissioning.
This is repeated for a range of SR3 curvatures. 
The resulting beam parameters that have been transformed by different SR3 curvatures are shown as the points in \cref{fig:SR3_heater_mismatch_contrours}.The plot is a 2D map in beam parameter space where the x-axis is the beam's waist position and the y-axis is the beam's beam size.
These two degrees of freedom uniquely specify a beam parameter.

We can see that for certain initial beam parameters it is possible to see a relatively small decrease in mode mismatch as the SR3 rear heater temperature is increased, corresponding to a beam parameter that has a waist position at -3.2~m and a beam size at the SRM of about 1.55~mm.
The information about the beam parameter from scanning a mode matching actuator can be combined with other measurements, such as beam profiles to further constrain the space of possibility.

Using the model in \cref{fig:SR3_heater_mismatch_contrours} we conclude that in the best case the SR3 heater is able to fix mode mismatch of up to 5\%.
If the SR3 heater is operated at its middle point, which is about 55$^\circ$C then we expect it to fix mismatches of about 2\% on either side of the SR3 actuation axis.

To be able to effectively actuate on an area of beam parameters and not just one line a second mode matching actuator is required.
This actuator should ideally be orthogonal to the SR3 actuator, where orthogonal means that the axis on which it moves the beam parameters is 90 degrees to the SR3 actuation axis in \cref{fig:SR3_heater_mismatch_contrours}.
This can be achieved by having the actuator separated from the SR3 actuator by 90 degrees of Gouy phase, where the Gouy phase is defined as
\begin{align}
    \Psi = \arctan\left[{\frac{z}{z_R}}\right]
\end{align}
where $z$ and $z_R$ are the waist position and Rayleigh range of the beam at the actuator.
In \cref{tab:src_gouy_phase} we computed the accumulated Gouy phase in the signal recycling cavity with Finesse3 using design aLIGO parameters.
Unfortunately both SR2 and the ITMs are close in Gouy phase as SR3, making them a poor choice for a pair of actuators in the signal recycling cavity.
The remaining optic is the SRM, which is somewhat orthogonal to the SR3 by about 20 degrees of Gouy phase.
Unfortunately the SRM is used in both reflection and transmission.
This is a problem since any thermal actuation on the SRM will induce both a thermoelastic and thermorefractive deformation with the latter being undesirable in this case.
The thermorefractive actuation will then need to be compensated for in the OMC and squeezer paths to maintain good mode matching to both of these subsystems.
\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{SRC_accum_gouy.pdf}
    \caption{A plot of the beam size and accumulated Gouy phase in a round trip of the SRC through the ITMX using aLIGO design parameters.}
    \label{tab:src_gouy_phase}
\end{figure}

In summary, we have used the SR3 actuator combined with OMC cavity scans to constrain the single bounce beam parameter to lie on a small section of the mode mismatch contour in parameter space.
From actuation data we found that in a model of the design aLIGO the SR3 heater is able to correct mode mismatches of up to 5\%.
A second orthogonal actuator inside the signal recycling cavity is needed to be able to actuate on an area in beam parameter space.
We have investigated the suitability of each possible actuator and found that the SRM can provide the most orthogonal actuator to the existing SR3 rear heater.
We discussed the possibility of using the SRM with a thermal actuator and the mode mismatch that is introduced on transmission due to the thermorefractive effect, which affects the mode matching of the interferometer beam going into to the OMC and the squeezer beam entering the interferometer.

\section{Concluding remarks}

We have shown in \cref{sec:sqz_LHO_astigmatism} that using OMC cavity length scan data it is possible to precisely measure the transverse mode spacing (TMS) splitting due to OMC astigmatism by fitting two lorentzians to the second order transverse resonance of the OMC. 
Using this technique we have found that the LHO OMC is more astigmatic than the aLIGO design, and the LLO OMC is less astigmatic than the aLIGO design.

In \cref{sec:SR3_ring_heater_scan} we showed that information can be obtained about the beam parameter coming out of the SRC by scanning the SR3 rear heater temperature while measuring OMC mode mismatch from OMC scans. This mismatch data can be used to fit a beam parameter from an ABCD model of SR3 with different radii of curvature.
We investigated the suitability of a second actuator inside the signal recycling cavity to further improve mode matching in the signal recycling cavity.
