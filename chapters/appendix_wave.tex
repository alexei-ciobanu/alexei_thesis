\chapter{Wave Theory}

\section{The wave equation}
\label{sec:wave_equation}

To derive the wave equation we first start from Maxwell's equations
\begin{flalign}
    &\text{(Gauss's law)} & \nabla \cdot \bm{E} &= \frac{\rho}{\epsilon_0} && \label{eq:gauss_law}\\ 
    &\text{(No monopoles)} & \nabla \cdot \bm{B} &= 0 && \label{eq:no_monopoles}\\  
    &\text{(Faraday's law)} & \nabla \times \bm{E} &= -\frac{\partial \bm{B}}{\partial t} && \label{eq:faradays_law}\\
    &\text{(Ampere's law)} & \nabla \times \bm{B} &= \mu_0 \left(\bm{J} + \epsilon_0 \frac{\partial \bm{E}}{\partial t}\right)&& \label{eq:amperes_law}
\end{flalign}
Taking the curl of both sides of Faraday's law \cref{eq:faradays_law} gives us the following
\begin{align}
    \nabla \times \nabla \times \bm{E} &= -\nabla \times \frac{\partial \bm{B}}{\partial t}
\end{align}
We can move the curl on the right hand side (RHS) into the time derivative since spatial derivatives commute with time derivatives, this yields
\begin{align}
    \nabla \times \nabla \times \bm{E} &= -\frac{\partial \left( \nabla \times \bm{B} \right)}{\partial t}
\end{align}
We can substitute Ampere's law \cref{eq:amperes_law} into the RHS to yield
\begin{align}
    \nabla \times \nabla \times \bm{E} &= -\mu_0 \left(\frac{\partial \bm{J}}{\partial t} + \epsilon_0 \frac{\partial^2 \bm{E}}{\partial t^2}\right)
\end{align}
Here we assume that there is no current density, i.e. $\bm{J} = 0$. This simplifies us to
\begin{align}
    \nabla \times \nabla \times \bm{E} &= -\mu_0 \epsilon_0 \frac{\partial^2 \bm{E}}{\partial t^2}
\end{align}
here we define $\mu_0 \epsilon_0 = \frac{1}{c^2}$
\begin{align}
    \nabla \times \nabla \times \bm{E} &= -\frac{1}{c^2} \frac{\partial^2 \bm{E}}{\partial t^2}
\end{align}
Now for the left hand side (LHS) we can simplify using the \textit{curl of curl} identity
\begin{flalign}
    &\text{(curl of curl)} & \nabla \times \left(\nabla \times \bm{A}\right) &= \nabla \left(\nabla \cdot \bm{A}\right) - \nabla^2 \bm{A} &&
\end{flalign}
which gives us
\begin{align}
    \nabla \left(\nabla \cdot \bm{E}\right) - \nabla^2 \bm{E} &= -\frac{1}{c^2} \frac{\partial^2 \bm{E}}{\partial t^2}
\end{align}
here we can substitute Gauss's law \cref{eq:gauss_law} into the LHS, which gives us
\begin{align}
    \nabla \left(\frac{\rho}{\epsilon_0}\right) - \nabla^2 \bm{E} &= -\frac{1}{c^2} \frac{\partial^2 \bm{E}}{\partial t^2}
\end{align}
here we assume that the charge density $\rho$ is constant across space which means that its gradient is zero $\nabla \rho = 0$. This yields 
\begin{align}
    \nabla^2 \bm{E} &= \frac{1}{c^2} \frac{\partial^2 \bm{E}}{\partial t^2} \label{eq:E_vector_wave_equation}
\end{align}
We can perform the exact same procedure when taking the curl of Ampere's law \cref{eq:amperes_law} to obtain a wave equation for the magnetic field
\begin{align}
    \nabla^2 \bm{B} &= \frac{1}{c^2} \frac{\partial^2 \bm{B}}{\partial t^2}
\end{align}

\section{Helmholtz equation}
To derive the Helmholtz wave equation we take our previous vector wave equation \cref{eq:E_vector_wave_equation}
\begin{align}
    \nabla^2 \left[\bm{E}(\bm{x}, t)\right] &= \frac{1}{c^2} \frac{\partial^2}{\partial t^2} \left[\bm{E}(\bm{x}, t)\right]
\end{align}
where we explicitly denote the electric field $\bm{E}$ as a vector function of two inputs, a position vector $\bm{x}$, and a scalar time $t$, with the derivatives as operators acting on the function.
Next we assume that the electric field $\bm{E}$ is separable between space and time, that is the electric field takes the following form
\begin{align}
    \bm{E}(\bm{x}, t) = \bm{E}(\bm{x}) E(t)
\end{align}
where we assume that the time dependent part of the electric field is scalar.
The wave equation then becomes 
\begin{align}
    E(t) \nabla^2 \left[\bm{E}(\bm{x})\right] &= \bm{E}(\bm{x}) \frac{1}{c^2} \frac{\partial^2}{\partial t^2} \left[E(t)\right] \label{eq:wave_eq_separable}
\end{align}
For this equation to hold the spatial and temporal parts can be solved independently.
In separation of variables we can do a sneaky trick to make the equations simpler, which won't make much sense now without foresight.
We can multiply both left hand and right hand side of \cref{eq:wave_eq_separable} by an arbitrary constant.
We choose to multiply both sides by $-k^2$, which will cancel nicely later on.
The rescaled wave equation is now
\begin{align}
    -k^2 E(t) \nabla^2 \left[\bm{E}(\bm{x})\right] &= -k^2 \bm{E}(\bm{x}) \frac{1}{c^2} \frac{\partial^2}{\partial t^2} \left[E(t)\right] \label{eq:wave_eq_separable_scaled}
\end{align}
When we consider the spatial part of \cref{eq:wave_eq_separable_scaled} equation we keep only the right hand side $-k^2$ and when we consider the temporal part we only keep the left hand side $-k^2$.
It doesn't make much sense to do this now but it will pay off later. 
The spatial part is then the Helmholtz equation
\begin{align}
    \nabla^2 \left[\bm{E}(\bm{x})\right] &= -k^2 \bm{E}(\bm{x}) \label{eq:helmholtz_spatial}
\end{align}
and the temporal part is
\begin{align}
    -k^2 E(t) &= \frac{1}{c^2} \frac{\partial^2}{\partial t^2} \left[E(t)\right]
\end{align}
The temporal part is a well known second order ordinary differential equation (ODE), which has the following solution.
\begin{align}
    E(t) = a e^{\iu wt}
\end{align}
we identify that the ODE has an oscillatory solution with angular frequency $\omega = kc$, where $a$ is a constant complex scalar that comes from the initial condition of the ODE.

The Helmholtz equation is more complicated but has been solved with the general solution being given in terms of spherical harmonics~\cite{jackson_classical_1999}, for which the lowest order is a spherical wave.
In polar coordinates a spherical wave is given by
\begin{align}
    \bm{E}(\bm{r}) = \frac{\bm{r} - \bm{r}_0}{\left|\bm{r} - \bm{r}_0\right|^2}\, e^{-\iu k\left|\bm{r} - \bm{r}_0\right|} \label{eq:vector_spherical_wave}
\end{align}
where $\bm{r}_0$ is the point source origin of the spherical wave.

\section{Paraxial scalar wave equation}
The standard method of solving the scalar Helmholtz equation is to assume that the solution is of the following form
\begin{align}
    E(x,y,z) = A(x,y,z) e^{-ikz}
\end{align}
where we assume that the envelope function $A(x,y,z)$ is slowly varying with $z$.
For brevity we may not write the spatial coordinates to $A$ but will assume that the spatial dependence is still there.
Taking the Laplacian of this trial solution gives us
\begin{align}
    \nabla^2 E(x,y,z) &= \left(\partial_x^2 + \partial_y^2 + \partial_z^2 \right) A e^{-ikz}\\
    &= \left(\partial_x^2 + \partial_y^2\right) A e^{-ikz} +  \partial_z \left(\partial_z A e^{-ikz}\right)\\
    &= \left(\partial_x^2 + \partial_y^2\right) A e^{-ikz} + \partial_z \left(e^{-ikz}\left[ \partial_z A - ikA\right]\right)\\
    &= \left(\partial_x^2 + \partial_y^2\right) A e^{-ikz} + e^{-ikz}\left(\partial_z^2 A - 2ik \partial_z A - k^2A\right)
\end{align}
The slowly varying envelope approximation assures us that the $\partial_z^2 A$ is much much smaller than every other term in the equation so we can safely neglect it.
Substituting the reduced expression for the Laplacian into the Helmholtz equation we get
\begin{align}
    \nabla^2 E(x,y,z) &= -k^2 E(x,y,z)\\
   \left(\partial_x^2 + \partial_y^2\right) A e^{-ikz} + e^{-ikz}\left(- 2ik \partial_z A - k^2A\right) &= -k^2 A e^{-ikz}
\end{align}
Eliminating the common term $e^{-ikz}$ and rearranging we get
\begin{align}
    \left(\partial_x^2 + \partial_y^2\right) A - 2ik \partial_z A &= 0
    \label{eq:paraxial_wave_equation}
\end{align}
which is the paraxial wave equation for the envelope field $A(x,y,z)$.
The paraxial wave equation is solved by any Hermite-Gauss mode, with the simplest being the fundamental Gaussian beam.
We won't derive the functional form of the Gaussian beam here. 
Instead we choose the less conventional, but simpler complex source point method to do so, which we discuss later. 

\subsection{Fresnel diffraction integral}

The Green's function of the paraxial wave equation~(\ref{eq:paraxial_wave_equation}) is given by
\begin{align}
    G(x,y,z) = \frac{1}{4 \pi z} \exp\left[-i k \frac{\left(x^2 + y^2\right)}{2 z}\right]
    \label{eq:paraxial_greens_function}
\end{align}
which serves as the kernel of the Fresnel diffraction integral. \Cref{eq:paraxial_greens_function} can be verified by noting that it is a valid solution to \cref{eq:paraxial_wave_equation}.
The Fresnel diffraction integral is given by
\begin{align}
    E(x',y',z) = -2 i k e^{-ikz} \iint_{-\infty}^{\infty} G(x-x',y-y',z) E(x,y,0) dy dx
    \label{eq:fresnel_diffraction_integral}
\end{align}

\subsection{Exponential paraxial wave propagation operator}\label{sec:paraxial_matrix_exponential}

We can obtain an expression for wave propagation directly from the paraxial wave equation by noting the similarity to the Schrodinger equation.
Take the paraxial wave \cref{eq:paraxial_wave_equation} and relabel the envelope of the field $A$ to be $\psi$
\begin{align}
    \left(\partial_x^2 + \partial_y^2\right) \psi - 2ik \partial_z \psi &=0\\
    \left(\partial_x^2 + \partial_y^2\right) \psi &= 2ik \partial_z \psi
    \label{eq:pswe_to_schrodinger}
\end{align}
where the Schrodinger equation is given by
\begin{align}
    \hat{H}\psi = i \hbar \frac{d}{dt} \psi
    \label{eq:schrodinger equation}
\end{align}
Here we identify that \cref{eq:pswe_to_schrodinger} is just the Schrodinger equation in 2D for a free particle with $z$ acting as the time coordinate, $2k$ acting as $\hbar$, and the Hamiltonian being given by
\begin{align}
    \hat{H} = \partial_x^2 + \partial_y^2
\end{align}
The time evolution of the wavefunction $\psi$, now being the spatial evolution of the electric field is given by the exponential of the Hamiltonian.
\begin{align}
    \psi(z) = e^{-i \hat{H} z / 2k} \psi(0)
    \label{eq:paraxial_wave_propagator}
\end{align}
which is valid for any given envelope field $\psi$ since the choice of origin $z=0$ is arbitrary.

It is not completely obvious how \cref{eq:paraxial_wave_propagator} can be used in numerical models. 
The exponential of a derivative operator at first seems like an abuse of notation.
However taking the Taylor series of the exponential shows something interesting
\begin{align}
    e^X &= 1 + X + \frac{XX}{2!} + \frac{XXX}{3!} + \frac{XXXX}{4!} + \ldots\\
    &= \sum_{n=0}^\infty \frac{X^n}{n!}
    \label{eq:exponential_taylor_series}
\end{align}
The Taylor series of $e^X$ is perfectly computable even if $X$ is a matrix or an operator where an $n$'th power of $X$ is taken to mean $n$ applications of the operator $X$.
Taking $\hat{H}$ to be a second order finite difference operator \cref{eq:exponential_taylor_series} gives a way of computing the propagation by summing even order finite differences of some discrete sampled wave field $\psi$.

Alternatively we can take a Pade series of $e^X$ which converges faster than the Taylor series for any given order but requires us to compute the inverse of powers of $X$, this is difficult to implement for an arbitrary operator but straightforward if $X$ is an invertible matrix.
Fortunately the finite difference operator can be written in a matrix form.

However, both Taylor and Pade series pale in comparison in terms of computational speed to the next approach, which will ultimately lead us to the FFT model for propagation.
We begin by taking note of the following Fourier transform pair
\begin{align}
    \mathcal{F} [x f] = \iu \partial_x \mathcal{F}[f]\\
    \mathcal{F} [\partial_x f] = \iu x \mathcal{F}[f]
\end{align}
where $\mathcal{F}$ is a Fourier transform operator.
The second identity is particularly useful as it allows us to replace the derivative of a function with a coordinate multiplication and a pair of Fourier transforms in the following way
\begin{align}
    \partial_x f = \mathcal{F}^{-1}[ \iu x \mathcal{F}[f]]
\end{align}
This generalizes to higher order derivatives in a rather natural way
\begin{align}
    (\partial_x)^n f = \mathcal{F}^{-1}[ (\iu x)^n \mathcal{F}[f]]
\end{align}
an interesting aside is that the RHS of this equation is valid for noninteger $n$, which leads to a definition of fractional order derivatives.

Going back to our Hamiltonian, the following Fourier identity applies
\begin{align}
    \hat{H}  &= \partial_x^2 + \partial_y^2 \\
    &= \mathcal{F}^{-1} \left[ (i x)^2 + (i y)^2 \right] \mathcal{F}\\
    &=\mathcal{F}^{-1} \left[- (x^2 + y^2) \right] \mathcal{F}
\end{align}
and similarly for powers of $\hat{H}$
\begin{align}
    \hat{H}^n  &= (\partial_x^2 + \partial_y^2)^n \\
    &= \mathcal{F}^{-1} \left[ \left(- (x^2 + y^2) \right)^n \right] \mathcal{F}
\end{align}
we can then write the exponential of $\hat{H}$ as 
\begin{align}
    e^{\hat{H}} &= \sum_{n=0}^\infty \frac{\hat{H}^n}{n!}\\
    &= \sum_{n=0}^\infty \mathcal{F}^{-1} \left[ \frac{\left(- (x^2 + y^2) \right)^n}{n!}\right] \mathcal{F}\\
    &= \mathcal{F}^{-1} \left[\sum_{n=0}^\infty \frac{\left(- (x^2 + y^2) \right)^n}{n!}\right] \mathcal{F}\\
    &= \mathcal{F}^{-1} \left[e^{-(x^2 + y^2)}\right] \mathcal{F}
\end{align}
and so we can finally write the Fourier dual of the exponential propagator as
\begin{align}
    \psi(z) = \mathcal{F}^{-1} \left[e^{-\iu z (x^2 + y^2) /2k}\right] \mathcal{F}  \psi(0)
    \label{eq:paraxial_fourier_propagator}
\end{align}
This expression for paraxial wave propagation is highly efficient when the Fourier transforms are implemented with FFTs.
The exponential in the middle is just a regular complex exponential function and doesn't require any fancy tricks to evaluate.
Overall the majority of the computational burden is now placed onto the FFT which is orders of magnitude faster than summing a convergent infinite series of finite difference operators. 
It is for this reason that optical models that use \cref{eq:paraxial_fourier_propagator} are referred to as FFT models.

\section{Complex source point (CSP) method}

An alternative and perhaps more elegant derivation of the Gaussian beam comes from placing the point source of a spherical wave $\bm{r}_0$ a complex distance away, which creates an exact solution to the Helmholtz equation that is also beam-like, reducing to the standard Gaussian beam in the paraxial approximation.
This trick of moving the point source of a spherical wave into the complex domain was found independently in 1971 by Deschamps~\cite{deschamps_gaussian_1971} and Keller~\cite{keller_complex_1971}.

To show how the complex source point (CSP) method produces a Gaussian beam we begin with the scalar spherical wave
\begin{align}
    E(r) = \frac{1}{r} e^{-\iu kr}
    \label{eq:spherical_wave}
\end{align}
where 
\begin{align}
    r = \sqrt{(x-x_0)^2 + (y-y_0)^2 + (z-z_0)^2}
\end{align}
is the distance from the point source located at $r_0 = \{x_0, y_0, z_0\}$.
This can be verified by noting that \cref{eq:spherical_wave} solves \cref{eq:helmholtz_spatial} with the the 3D Laplacian $\nabla^2$ in spherical coordinates being
\begin{align}
    \nabla^2 = \frac{1}{r^2} \frac{\partial}{\partial r} \left(r^2 \frac{\partial}{\partial r} \right) + \frac{1}{r^2 \sin(\theta)} \frac{\partial}{\partial \theta} \left(\sin(\theta) \frac{\partial}{\partial \theta} \right) + \frac{1}{r^2 \sin(\theta)^2} \frac{\partial^2}{\partial \varphi^2}
\end{align}
where we can set $\dfrac{\partial E}{\partial \theta} = \dfrac{\partial E}{\partial \varphi} = 0$ since we want a uniform spherical wave.

Placing the point source at $r_0 = \{0,0,- \iu z_R\}$ we find the radial distance is
\begin{align}
    r = \sqrt{x^2 + y^2 + (z + \iu z_R)^2} \label{eq:nonparaxial_radius}
\end{align}
Here we make the paraxial approximation $x^2 + y^2 \ll |z + \iu z_R|^2$, the Taylor expansion of $r$ around $x=0$ and $y=0$ is then
\begin{align}
    r = (z + \iu z_R) + \frac{x^2 + y^2}{2(z + \iu z_R)} + \mathcal{O}(x^4) + \mathcal{O}(y^4) \label{eq:paraxial_radius}
\end{align}
substituting it back into the scalar spherical wave and neglecting higher order terms
\begin{align}
    E(x,y,z) = \frac{1}{z + \iu z_R} \exp\!\left[-\iu k\left((z+\iu z_R) +\frac{x^2 + y^2}{2(z+\iu z_R)}\right)\right]
\end{align}
where we drop the $\frac{x^2 + y^2}{2(z + \iu z_R)}$ term in $1/r$ since $x^2 + y^2 \ll |z + \iu z_R|^2$.
Rewriting in a more familiar form
\begin{align}
    E(x,y,z) = \frac{e^{-\iu k (z+\iu z_R)}}{z + \iu z_R} \exp\!\left[-\iu k \frac{x^2 + y^2}{2(z+\iu z_R)}\right] \label{eq:csp_paraxial_gaussian_beam}
\end{align}
which is identical to the paraxial \tem{00} beam, barring an overall normalization factor.
\Cref{eq:csp_paraxial_gaussian_beam} contains the plane wave phase term $e^{-\iu k z}$, the same transverse beam size and radius of curvature. 
The $1/(z + \iu z_R)$ term performs serves two purposes, the first is to keep the power of the beam constant as it expands, and the second is to compute the Gouy phase.
The overall constant term given by $e^{k z_R}$ can be discarded as it does not normalize the beam to unit power and is too numerically large to have a floating point representation, making it of limited use in computer calculations.
The correct normalization for \cref{eq:csp_paraxial_gaussian_beam} therefore has to be derived separately.

Higher order Gaussian modes can be derived by using the complex source point method on higher order spherical harmonic functions, which also solve the scalar Helmholtz equation and correspond to radiation emitted by a multipole source.
This was first demonstrated by Shin and Felsen in 1977~\cite{shin_gaussian_1977}.

Even though we invoked the paraxial approximation in \cref{eq:paraxial_radius} the complex source point method itself makes no paraxial approximation. 
Therefore the full nonparaxial complex source point radius in \cref{eq:nonparaxial_radius} can be used to obtain a nonparxial analogue of the Gaussian beam and its higher order modes that are an exact solution of the scalar Helmholtz equation.

The derivation we present here can also be performed in reverse; starting with the paraxial Gaussian beam higher order parxial corrections can be derived by using the methods described by Lax et al.~(1975)~\cite{lax_maxwell_1975}, or Agrawal et al.~(1979)~\cite{agrawal_gaussian_1979}.
Summing all orders of the paraxial corrections reproduces the complex source point spherical wave as was shown by Couture and Belanger in 1981~\cite{couture_gaussian_1981}.

The complex source point method also works for the vector Helmholtz equation, which can produce vector beam-like solutions that exactly satisfy Maxwell's equations in homogeneous free space.
In 1979 Cullen and Yu~\cite{cullen_complex_1979} published closed form expressions for the six vector components of the electric and magnetic field that satisfy Maxwell's equations based on the radiation emitted by crossed electric and magnetic dipole whose origin lies in the complex plane.
They found that this beam reduces to the standard Gaussian beam in the paraxial approximation but also had other desirable properties, such as a symmetric electric and magnetic field magnitude, which makes it suitable for calculations where vector polarization is needed.

The solution by Cullen and Yu had one major flaw in that it produced a ring of amplitude singularities around the focus of the beam where the complex radius in \cref{eq:nonparaxial_radius} goes to zero.
This singularity was addressed by Sheppard and Saghafi in 1998 for the scalar spherical wave by introducing a complex sink in addition to the complex source~\cite{sheppard_beam_1998}.
The next year in 1999 they extended their results to vector beams~\cite{sheppard_electromagnetic_1999} and multipole radiation~\cite{sheppard_transverse-electric_1999,sheppard_electric_1999}, completing their theory of complex source/sink pairs to generate closed form beam-like solutions to Maxwell's equations that are singularity-free and reduce to standard Gaussian beams in the paraxial limit.
In removing the singularity their solutions introduce a backwards travelling wave, which the authors acknowledge violates Sommerfeld's radiation condition~\cite{sheppard_transverse-electric_1999}. 
Though the authors claim that it does not mean that their solution is unphysical.

The complex source and sink pair solutions by Sheppard and Saghafi have seen subsequent use in modelling nonparaxial beams~\cite{sapozhnikov_exact_2012,mitri_quasi-gaussian_2013}, though some authors had an issue that their solution contains a backwards travelling wave, calling it non-causal~\cite{lekner_nonexistence_2018}. 

\section{Bayer-Helms scattering coefficients}
The Bayer-Helms solution~\cite{bayer-helms_coupling_1984} is given in closed-form as a complex exponential scaling factor multiplied by a rational function of the basis parameters.
\begingroup
\allowdisplaybreaks
\begin{align}
    k_{n_1n_2}[q_1, q_2, \delta, \gamma] &= C_1[n_1,n_2] \, C_2[n_1,n_2,q_1,q_2] \, C_3[q_1,q_2, \delta, \gamma]& \nonumber \\
    & \qquad \qquad \left(S_g[n_1,n_2,q_1,q_2, \delta, \gamma] - S_u[n_1,n_2,q_1,q_2, \delta, \gamma] \right)
\end{align}
where
\begin{align}
    C_1[n_1,n_2] &= (-1)^{n_2} \sqrt{n_1! \, n_2!}\\
    C_2[n_1,n_2,q_1,q_2] &= \left(1 + K_0\right)^{\left(\frac{n_1}{2} + \frac{1}{4}\right)} \left(1 + K^* \right)^{\left(\frac{-n_1 - n_2 - 1}{2}\right)}\\
    C_3[q_1, q_2, \delta, \gamma] &= \exp\left[\frac{-X \bar{X}}{2} - \frac{\iu \delta \, \im{q_2} \sin\left[\gamma\right]}{w_{0}\!^2}  \right]
\end{align}
where the terms are
\begin{align}
    w_0[q_2] &= \sqrt{\frac{\lambda \, \im{q_2}}{\pi} }\\
    K_0[q_1, q_2] &= \frac{\im{q_1} - \im{q_2}}{\im{q_2}}\\
    K_2[q_1, q_2] &= \frac{\re{q_1} - \re{q_2}}{\im{q_2}}\\
    K[q_1, q_2] &= \frac{K_0 + \iu K_2}{2}\\
    X[q_1, q_2, \delta, \gamma] &= \left(\frac{\delta}{w_0} - \left(\frac{\re{q_2}}{\im{q_2}} + \iu \left(1 + 2 K^*\right) \right)  \frac{\im{q_2}\, \sin[\gamma]}{w_0} \right) \left(\frac{1}{\sqrt{1 + K^*}}\right)\\
    \bar{X}[q_1, q_2, \delta, \gamma] &= \left(\frac{\delta}{w_0} - \left(\frac{\re{q_2}}{\im{q_2}} - \iu \right)  \frac{\im{q_2}\, \sin[\gamma]}{w_0} \right) \left(\frac{1}{\sqrt{1 + K^*}}\right)
\end{align}
The two sum terms $S_g$ and $S_u$ are
\begin{align}
    & \qquad S_g[n_1,n_2,q_1,q_2, \delta, \gamma] = \nonumber\\
    & \sum_{\mu_1 = 0}^{\lfloor n_1 / 2 \rfloor} \sum_{\mu_2 = 0}^{\lfloor n_2 / 2 \rfloor} \frac{(-1)^{\mu_1} \bar{X}^{\left(n_1 - 2 \mu_1\right)} X^{\left(n_2 - 2 \mu_2\right)}}{\left(n_1 - 2 \mu_1\right)! \left(n_2 - 2 \mu_2\right)!} \sum_{\sigma = 0}^{\min{\mu_1, \mu_2}} \frac{(-1)^\sigma \bar{F}^{\left(\mu_1 - \sigma\right)} F^{\left(\mu_2 - \sigma\right)}}{\left(2\sigma\right)! \left(\mu_1 - \sigma\right)! \left(\mu_2 - \sigma\right)!}\\
    \nonumber\\
    & \qquad S_u[n_1,n_2,q_1,q_2, \delta, \gamma] = \nonumber\\
    & \sum_{\mu_1 = 0}^{\lfloor \left(n_1-1\right) / 2 \rfloor} \sum_{\mu_2 = 0}^{\lfloor \left(n_2 - 1\right) / 2 \rfloor} \! \frac{(-1)^{\mu_1} \bar{X}^{\left(n_1 - 2 \mu_1 - 1\right)} X^{\left(n_2 - 2 \mu_2 - 1\right)}}{\left(n_1 - 2 \mu_1 - 1\right)! \left(n_2 - 2 \mu_2 - 1\right)!} \sum_{\sigma = 0}^{\min{\mu_1, \mu_2}} \! \frac{(-1)^\sigma \bar{F}^{\left(\mu_1 - \sigma\right)} F^{\left(\mu_2 - \sigma\right)}}{\left(2\sigma + 1\right)! \left(\mu_1 - \sigma\right)! \left(\mu_2 - \sigma\right)!}
\end{align}
where $\lfloor x \rfloor$ is the integer floor function, and $\min{x_1, x_2}$ is the minimum function, which returns $x_1$ if $x_1 < x_2$ and otherwise returns $x_2$. 
The new terms used in the sums are 
\begin{align}
    F[q_1, q_2] &= \frac{K^*}{2}\\
    \bar{F}[q_1, q_2] &= \frac{K}{2\left(1 + K_0\right)}
\end{align}
\endgroup
where $K^*$ is the complex conjugate of $K$.