\chapter{Linear Canonical Transform}

\section{Metaplectic sign ambiguity}\label{sec:metaplectic_sign}
%In this section we discuss an often overlooked fact of the use of the LCT in the optics community; that being that the LCT is a faithful representation of the metaplectic group and its implication on the accumulated Gouy phase.
%We conclude this section with a new formula for 1D accumulated Gouy phase, which can be used to compute the Gouy phase in simple astigmatic 2D optical systems.
The LCT has some implications when calculating the accumulated Gouy phase through an optical system---which is due to the LCT being a faithful representation of the metaplectic group. Here we find a new formula for the 1D accumulated Gouy phase, which can also be used to compute the accumulated Gouy phase in a simple astigmatic 2D optical system.

The ABCD matrices discussed here form a group known as the symplectic group $\text{Sp}_2(\mathbb{R})$~\cite{wolf_geometric_2004}, which are all $2\times2$ matrices with unit determinant.
There are 2 distinct LCTs for every ABCD matrix because the metaplectic group $\text{Mp}_2(\mathbb{R})$ is a double cover of the symplectic group $\text{Sp}_2(\mathbb{R})$.
The additional parameter that allows us to uniquely specify an LCT is called the metaplectic sign~\cite{wolf_geometric_2004}.

The metaplectic sign manifests in optics as a sign ambiguity in the Gouy phase accumulated by a beam propagating through an ABCD optical system.
This sign ambiguity disappears in 2D optical systems exhibiting cylindrical symmetry due to both vertical and horizontal having the same metaplectic sign, which end up cancelling yielding an overall expression for accumulated Gouy phase that has no sign ambiguity~\cite{erden_accumulated_1997,siegman_20_1986}.

The accumulated Gouy phase for a Gaussian beam with beam parameter $q$ through an ABCD system in two dimensions with cylindrical symmetry is
\begin{equation}
    \text{exp}(\iu \Psi) = \text{exp}(\iu 2 \psi) = \frac{A + B/q^*}{|A + B/q^*|}
    \label{eq:accum_gouy_2d}
\end{equation}
and in one dimension 
\begin{equation}
    \text{exp}(\iu \psi) = \pm \sqrt{\frac{A + B/q^*}{|A + B/q^*|}}
    \label{eq:accum_gouy_1d}
\end{equation}
where the $\pm$ sign, which we identify as the \textit{metaplectic sign} is a real sign ambiguity that is present in the 1D accumulated Gouy phase for an arbitrary ABCD matrix.

An algorithm to determine the metaplectic sign for LCTs to the best of our knowledge was first presented by Littlejohn (appendix A of~\cite{littlejohn_semiclassical_1986}, and \cite{littlejohn_new_1987}) and has been subsequently used by Lopez~\cite{lopez_pseudo-differential_2019}.
Their algorithm computes and tracks a winding number from the multiplication of ABCD matrices, which is then used to determine the metaplectic sign.
We propose a modification to this algorithm that bypasses the need to track the winding number and instead tracks the metaplectic sign directly.
We use this algorithm to define a group action, which is an extension of regular matrix multiplication between ABCD matrices paired with a metaplectic sign, which we call a \textit{metaplectic matrix}.

\subsection{Metaplectic Matrices}

A metaplectic matrix $\mathfrak{M} \in \text{Mp}_2(\mathbb{R})$ is a composite object that can be represented as a tuple/ordered set containing a symplectic matrix $\mathbf{M} \in \text{Sp}_2(\mathbb{R})$ and a binary sign $\sigma \in \{-1, 1\}$, which is the metaplectic sign.
\begin{equation}
    \mathfrak{M} = \{\mathbf{M}, \sigma\}.
\end{equation}
The multiplication law between two metaplectic matrices is then defined as
\begin{equation}
    \mathfrak{M}_3 = \mathfrak{M}_2 \mathfrak{M}_1 = \{\mathbf{M}_2 \mathbf{M}_1, \sigma_3\},
\end{equation}
where the new sign $\sigma_3$ is given by
\begin{equation}
    \sigma_3 = ( \sigma_2 \times \sigma_1 ) \times (-1)^{\rho(\mathbf{M}_2, \mathbf{M}_1)}
\end{equation}
where \hbox{$\rho(\mathbf{M}_2, \mathbf{M}_1) \in \{0, 1\}$} is given by
\begin{equation}
    \rho(\mathbf{M}_2, \mathbf{M}_1) = [\theta(\mathbf{M}_2) < 0] \oplus [\theta(\mathbf{M}_2 \mathbf{M}_1) < \theta(\mathbf{M}_1)] \label{eq:metaplectic_sign_flip}
\end{equation}
where $\oplus$ is a logical exclusive or (XOR), and $-\pi < \theta(\mathbf{M}) \leq \pi$ is a metaplectic phase associated with an ABCD matrix given by
\begin{equation}
    \theta(\mathbf{M}) = \text{arg}(A + \iu B).
\end{equation}
We adopt the convention of the boolean True and False being represented by 1 and 0 respectively.

\eqref{eq:metaplectic_sign_flip} was derived from logical expressions of the following kind: if the metaplectic phase is decreasing $[\theta(\mathbf{M}_2) < 0]$, and if the output metaplectic phase is less than the input metaplectic phase $[\theta(\mathbf{M}_2 \mathbf{M}_1) < \theta(\mathbf{M}_1)]$, then the metaplectic sign hasn't flipped $[\rho(\mathbf{M}_2, \mathbf{M}_1) = 0]$.
Enumerating all possible combinations of True and False for the conditions in \eqref{eq:metaplectic_sign_flip} reveals that the truth table for determining if a metaplectic sign flip has occurred is equivalent to an XOR of the two conditions.

The 1D Gouy phase of a Gaussian beam with beam parameter $q$ through a metaplectic ABCD matrix $\mathfrak{M} = (\mathbf{M}, \sigma)$ is then
\begin{equation}
    \text{exp}(\iu \psi) = \sigma \sqrt{\frac{A + B/q^*}{|A + B/q^*|}}.
    \label{eq:metaplectic_gouy_1d}
\end{equation}
With this we can define the Gouy phase of a Gaussian beam through a 2D optical system with simple astigmatism described with two metaplectic matrices $\mathfrak{M}_x$ and $\mathfrak{M}_y$ for the $x$ and $y$ axes respectively as 
\begin{equation}
    \text{exp}(\iu \Psi) = \sigma_x \sigma_y \exp \!\big[\iu (\psi_x + \psi_y )\big]
    \label{eq:metaplectic_gouy_2d}
\end{equation}
which reduces to \eqref{eq:accum_gouy_2d} to in the case of cylindrical symmetry, where $\mathfrak{M}_x = \mathfrak{M}_y$.

It should be noted that the expressions for Gouy phase in \eqref{eq:metaplectic_gouy_1d} and \eqref{eq:metaplectic_gouy_2d} are not analytic (i.e. they don't have Taylor series) due to the discrete nature of the metaplectic sign $\sigma \in \{-1, 1\}$.
We believe that it is likely impossible to derive an analytical expression for 1D Gouy phase that uses a single ABCD matrix to represent an arbitrary optical system.
A similar statement has been made by other authors for 2D Gouy phase in general astigmatic optical systems~\cite{habraken_geometric_2010}.

\section{Structured Matrices} \label{sec:structured_matrices}
Structured matrix is a generic term for any matrix that has a fast matrix-vector multiplication algorithm~\cite{pan_structured_2001}.
For example all sparse matrices are structured, and their fast matrix-vector multiplication is performed by ignoring the matrix zeroes in the calculation.
Other forms of structured matrices exist, each with their own specialized fast matrix-vector multiplication.
An example of a dense structured matrix is the DFT matrix, with the fast matrix-vector multiplication being the FFT algorithm.

For this paper the structured matrix in question is the $N^2 \times N^2$ separable 2D DLCT kernel $\dlct{\mathbf{M}}$, which we find can be written as a Kronecker product of two $N \times N$ 1D DLCT kernels $\dlct{\mathbf{M}_x}$ and $\dlct{\mathbf{M}_y}$ respectively.
\begin{align}
    \dlct{\mathbf{M}} = \dlct{\mathbf{M}_x} \otimes \dlct{\mathbf{M}_y}
\end{align}
The fast matrix-vector multiplication algorithm for a Kronecker product of two matrices is given by Roth's relation~\cite{roth_direct_1934, abadir_kronecker_2005}.
\begin{align}
    \left(\mathbf{A} \otimes \mathbf{B} \right) \text{vec}\left[\mathbf{X}\right] = \text{vec}\left[ \mathbf{B} \mathbf{X} \mathbf{A}^T \right]
\end{align}
where $\text{vec}\left[\mathbf{X}\right]$ is an operation that unravels any matrix $\mathbf{X}$ into a vector by stacking its columns underneath each other~\cite{abadir_kronecker_2005}.
This identity was used in discretizing the separable 2D LCT in \cref{eq:2d_separable_dlct}. The fast round trip procedure used in \cref{eq:structured_circ_operator} is
\begin{align}
    \mathbf{X}_\text{out} = \mathbf{R}_{1} \circ \left( \dlct{\mathbf{M}_d} \left( \mathbf{R}_{2} \circ \left(\dlct{\mathbf{M}_d} \mathbf{X}_\text{in} \left(\dlct{\mathbf{M}_d}\right)^T \right) \right) \left(\dlct{\mathbf{M}_d}\right)^T \right) \label{eq:fast_round_trip}
\end{align}
where the order of parentheses has to be respected since matrix and Hadamard multiplication are neither commutative or associative with each other.
\eqref{eq:fast_round_trip} can be thought of as the fast matrix-vector multiplication algorithm for a 2D cavity round trip with mirror maps and a separable free-space propagation kernel.

\section{Apertured LCT model}
Using the formalism described in \cref{chp:lct_paper} the circulating field $\bm{X}_1$ in the arm cavity is given by
\begin{align}
    \bm{X}_1 &= \bm{X}_5 + \iu t_1 \bm{A}_1 \circ \bm{X}_{inc}
\end{align}
where $\bm{X}_{inc}$ is the incident field and $\bm{X}_5$ is the round trip propagation in with the LCT and can be expanded to linear operations on the circulating field $\bm{X}_1$.
\begin{align}
    \bm{X}_2 &= \bm{D}_{y1} \bm{X}_1 \bm{D}_{x1}^T\\
    \bm{X}_3 &= \bm{A}_2 \circ \bm{R}_{c,2} \circ ( r_2 \bm{X}_2 )\\
    \bm{X}_4 &= \bm{D}_{y2} \bm{X}_3 \bm{D}_{x2}^T\\
    \bm{X}_5 &= \bm{A}_1 \circ \bm{R}_{c,1} \circ ( r_1 \bm{X}_4 )
\end{align}

\subsection{LCT model parameters}
% \begin{table}[!htb]
% \centering
\begin{longtable}{ |c|c|c| }
    \caption{Physical parameters describing the LIGO arm cavity.} \\
    \hline
    $r_1$ & ITM amplitude reflection & $\sqrt{1-0.014}$ \\
    $t_1$ & ITM amplitude transmission & $\sqrt{0.014}$ \\
    $r_2$ & ETM amplitude reflection & $\sqrt{1 - 5 \times 10^{-6}}$ \\
    $t_2$ & ETM amplitude transmission & $\sqrt{5 \times 10^{-6}}$\\
    $L_{arm}$ & arm length & 3994.5~m \\
    $A_{diam}$ & test mass diameter & 32.6~cm\\
    $R_{c,1}$ & ITM radius of curvature & 1934~m\\
    $R_{c,2}$ & ETM radius of curvature & 2245~m\\
    \hline
\end{longtable}
% \end{table}

% \begin{table}[!htb]
% \centering
\begin{longtable}{ |c|c|c| }
    \caption{LCT model parameters}
    \label{tab:arm_cavity_LCT_hyperparameters} \\
    \hline
    $P_{in}$ & incident power & 1~W\\
    $q_1$ & incident beam parameter & $-1834.2 + 427.8\iu $~m\\
    $w_1$ & beam size at ITM & 5.3~cm\\
    $w_2$ & beam size at ETM & 6.2~cm\\
    \verb|N| & number of points per grid axis & 801\\
    \verb|x1| & x-axis at ITM & $5 w_1 \times$ \verb|linspace(-1,1,N)| \\
    \verb|y1| & y-axis at ITM & $5 w_1 \times$ \verb|linspace(-1,1,N)| \\
    \verb|x2| & x-axis at ETM & $5 w_2 \times$ \verb|linspace(-1,1,N)| \\
    \verb|y2| & y-axis at ETM & $5 w_2 \times$ \verb|linspace(-1,1,N)| \\
    $\bm{X}_{in}$ & incident field & $\sqrt{P_{in}} \times $ \verb|U00(x1, y1,| $q_1$\verb|)|\\
    \verb|Md| & cavity length ABCD matrix & $\begin{bmatrix}
        1 & L_{arm} \\
        0 & 1 
    \end{bmatrix}$\\
    \hline
\end{longtable}
% \end{table}

% \begin{table}[!htb]
% \centering
\begin{longtable}{ |c|c|c| }
    \caption{Mirror maps and LCT operators.}\\
    \hline
    $\bm{D}_{x1}$ & x-axis LCT from ITM to ETM & \verb|DLCT(x1, x2, Md)|\\
    $\bm{D}_{y1}$ & y-axis LCT from ITM to ETM & \verb|DLCT(y1, y2, Md)|\\
    $\bm{D}_{x2}$ & x-axis LCT from ETM to ITM & \verb|DLCT(x2, x1, Md)|\\
    $\bm{D}_{y2}$ & y-axis LCT from ETM to ITM & \verb|DLCT(y2, y1, Md)|\\
    $\bm{R}_{c,1}$ & ITM curvature phase map & \verb|RoC_phase_map(x1, y1,| $R_{c,1}$\verb|)|\\
    $\bm{R}_{c,2}$ & ETM curvature phase map & \verb|RoC_phase_map(x1, y1,| $R_{c,2}$\verb|)|\\
    $\bm{A}_{1}$ & ITM aperture map & \verb|circ_ap(x1, y1,| $A_{diam}$\verb|)|\\
    $\bm{A}_{2}$ & ETM aperture map & \verb|circ_ap(x1, y1,| $A_{diam}$\verb|)|\\
    \hline
\end{longtable}
% \end{table}

\subsection{LCT model outputs}

Here we show visual representations of various operators used in the LCT model with the aim of making it easier to understand the inner workings of the model.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{arm_cav_ITM_map.pdf}
    \caption{ITM aperture and curvature phase map.}
    \label{fig:ITM_aperture_curvature_map}
\end{figure}

\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{arm_cav_ETM_map.pdf}
    \caption{ETM aperture and curvature phase map.}
    \label{fig:ETM_aperture_curvature_map}
\end{figure}

The mirror maps for the ITM and ETM shown in \cref{fig:ITM_aperture_curvature_map,fig:ETM_aperture_curvature_map} highlight an interesting application of the LCT --- that it can work with dynamic grid sizes. 
This can be seen by noting that the apparent size of the test mass aperture is slightly different between \cref{fig:ITM_aperture_curvature_map,fig:ETM_aperture_curvature_map}.
The aperture for the ITM appears occupies a larger area of the grid than the ETM aperture because the ITM grid uses a smaller step size.
The grid extent is chosen to be scaled to 10 times the beam size at that plane as shown in \cref{tab:arm_cavity_LCT_hyperparameters}, which naturally leads to a dynamic grid size.

The ring structure seen \cref{fig:ITM_aperture_curvature_map,fig:ETM_aperture_curvature_map} corresponds to a phase shift associated with the radius of curvature at the mirrors.
The ETM and ITM have a similar enough radius of curvature that there isn't an appreciable difference in the periodicity of the rings, despite the ITM having a smaller radius of curvature.
This method of implementing a mirror reflection operator can potentially lead to issues if the radius of curvature is small and the oscillation frequency of the rings exceeds the Nyquist frequency of the grid. 
This can often happen with fast telescopes, such as the one in the aLIGO power recycling cavity. 
In those cases the sampling frequency of the grid has to be carefully chosen with those telescopes in mind.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{arm_cav_LCT_Dy1.pdf}
    \caption{Space propagation LCT operator for the y-axis arm cavity length free space propagation.}
    \label{fig:arm_cavity_free_space_LCT}
\end{figure}

\Cref{fig:arm_cavity_free_space_LCT} is the LCT operator for the arm cavity length free space propagation.
The operator shown in \cref{fig:arm_cavity_free_space_LCT} is explicitly $\bm{D}_{y1}$, but since there is no difference between the x-axis and y-axis in this model it follows that there is no difference between the operators for the two axes, that is $\bm{D}_{y1} = \bm{D}_{x1}$.
The dynamic grid scaling mentioned earlier is also present here.
In fact the $\bm{D}_{y1}$ operator is the one that implements the grid scaling operation --- expanding the grid as the beam propagates from the ITM plane to the ETM plane.
The operator $\bm{D}_{y2}$ for propagating from the ETM back to the ITM has to perform the same free space propagation, but the inverse scaling operation --- shrinking the grid as the beam propagates from the ETM to the ITM.
In this model the inverse scaling operation is given by the transpose of the original scaling operator, and so the propagation operators between the ITM and ETM plane are related by a transpose $\bm{D}_{y2} = \bm{D}_{x2} = \bm{D}_{y1}^T = \bm{D}_{x1}^T$.
It follows that a free space propagation operator that doesn't scale the grid is transpose symmetric.