
\section{Motivation}
% \begin{itemize}
% 	\item Saw astigmatism on the squeezer beam
% 	\item Mode matching in LHO was strange due to astigmatism
% 	\item Noticed that LHO OMC was also astigmatic and could track the 
% 	astigmatism
% 	\item Used SR3 and SRM heaters to monitor how astigmatism was varying
% 	% \item Want to get LHO to 150 Mpc no matter what
% 	\item Only way from my end is to reduce squeezing losses
% 	\item How much can the SR3 and SRM control squeezing losses, what is the 
% 	lowest squeezing loss we can get to?
% 	\item Is there any other TCS or any other compensation we could do to 
% 	decrease squeezing loss
% \end{itemize}

To increase GW sensitivity of a detector it is important to first know what is wrong and what could be improved.
One could mull over the design of the detector and infer potential improvements that way.
However it is often easier to simply ask the people currently working on the detector on what the current issues are.
These kinds of discussions take place during the commissioning period.

The commissioning of a gravitational wave detector is a period where various subsystems of the detector are upgraded and tested in preparation of an observation run. 
During these times many sorts of problems appear, which must be diagnosed and rectified.

One of these subsystems is the thermal compensation system (TCS), whose primary function is to actuate on the core LIGO mirrors to alter their optical properties. 
In the past the TCS's role was to suppress the mechanical modes of mirrors that were excited by radiation pressure from the science beam, also known as parametric instabilities (PIs) [matt evans 2015].
The recent installation of acoustic mode dampers (AMD) [S. Gras 2015] promise to significantly suppress mechanical resonances, which frees the TCS for use in improving the mode matching of the interferometer.

My role during this commissioning period was to measure the mode matching state of the interferometer. 
As part of that goal I tested the range of the TCS actuators in their capacity to mode match the interferometer beam to the output mode cleaner. 
Additionally I was involved in the installation of a new TCS actuator: the SRM CO2 heater.

\section{Measuring mode mismatch from OMC scans}

%The mismatch of a pure Gaussian beam to a cavity can be inferred by performing a cavity scan on the beam, which
%decomposes the beam into it's Hermite-Gauss constituents in the cavity basis. This happens because different
%Hermite-Gauss modes accumulate different amounts of phase as they traverse the cavity and thus resonate at
%different frequencies.

The mismatch of a pure Gaussian beam incident on a cavity can be inferred by microscopically changing the 
length of the cavity and measuring the power transmitted from the cavity. As the cavity changes length different 
higher order modes of the incident beam become resonant. These modes are expressed in the basis of the cavity's
eigenmode.

A beam can decomposed into a sum of Hermite-Gauss modes of any basis. Different Hermite-Gauss modes acquire 
different amounts of phase as they traverse the cavity and thus resonate at different frequencies. 

The mode mismatch can be inferred by comparing the relative strengths of the resonances between Hermite-Gauss 
modes. The fraction of total power scattered into higher order modes in basis $q_2$ for a pure Gaussian beam in 
basis $q_1$ without misalignments is 
\begin{equation}
\frac{|q_2 - q_1|^2}{|q_2 - q_1^*|^2}
\end{equation}
This can alternatively be expressed in terms of overlap integrals between the two bases as 
\begin{equation}
\frac{|k_{0002}(q_1,q_2)|^2 + |k_{0020}(q_1,q_2)|^2}{|k_{0000}(q_1,q_2)|^2}
\end{equation}
which is the ratio of the height of the second order resonance to the zeroth order resonance as measured in 
an OMC scan.

Thus the mode matching state of the interferometer can be inferred from the transmission of the OMC as the 
microscopic length of the OMC is varied.

\section{Resonance splitting due to OMC astigmatism}\label{sec:omc_astigmatism_intro}

The design specification of the OMC is a bowtie cavity, which is a weakly astigmatic ring cavity.
A few key parameters of the LHO OMC cavity are listed in table~\ref{tab:LHO_OMC_parameters}.
There are a number of ways to quantify the astigmatism. 
The one that makes the most sense for this discussion is the difference between the mode separation frequency between the x and y axis relative to the FWHM of the resonance peaks.
\begin{table}[ht]
	\centering
	\begin{tabular}{|c|c|c|c|c|}
		\hline
		$\delta f_x$ & $\delta f_y$ & FWHM & FSR & finesse\\
		\hline
		5.813e+07 Hz & 5.798e+07 Hz & 6.44e+05 Hz & 2.65e+08 Hz & 411\\
		\hline
	\end{tabular}
	\caption{OMC parameters}
	\label{tab:LHO_OMC_parameters}
\end{table}
\begin{align}
\frac{\delta f_x - \delta f_y}{\Gamma} = 0.24
\end{align}
where $\Gamma$ is the full-width-half-maximum (FWHM) of the resonance peak.
This quantity can be used to determine how easy it is to resolve adjacent HOM resonances e.g. if the TEM$_{01}$ peak can be resolved from the TEM$_{10}$ peak.
The second order modes are separated from the TEM$_{00}$ by twice the mode separation frequency. 
Hence the resolvability of the TEM$_{02}$ and TEM$_{20}$ modes in the OMC scan is twice what is for the first order modes.

One can make an argument that the sum of two lorentzians with identical FWHMs can be resolved if there is a visible valley between the two peaks. 
This can be stated as the point where first derivative is zero and the second derivative is positive, with the stationary point being where both first and second derivatives are zero. 
The point where the sum of two lorentzians become resolvable is 
\begin{equation}
\frac{\delta f_x - \delta f_y}{\Gamma} = \frac{1}{\sqrt{3}} \approx 0.577
\end{equation}
The derivation of this condition will be included in the appendix.
One should note this is not a hard limit and more of a hueristic guide.
In practice one will be trying to separate the peaks buried in noise.
There the resolvability will be given by a probability.

Applying the resolvability criterion to the second order modes of the LHO OMC we find
\begin{equation}
\frac{2(\delta f_x - \delta f_y)}{\Gamma} = 0.48
\end{equation}
which is less than the heuristic for resolving two lorentzians. 
So the TEM$_{02}$ and TEM$_{20}$ peaks are difficult to resolve in a cavity scan.

\subsection{Underestimation of mismatch due to OMC astigmatism}
Since the TEM$_{02}$ and TEM$_{20}$ peaks are not easily resolvable, it makes sense to treat their sum as one peak and compute mismatch from the value of that peak. 
This approach will always tend to underestimate the actual mismatch the cavity sees from a Gaussian beam. 
The amount that the mismatch will be underestimated can be estimated from theory. 
One just needs the ratio between the heights of sum of two lorentzians that are on top of each other and the sum of two that are separated by some small distance (ie resolvability is less than $\frac{1}{\sqrt{3}}$).

The derivation of this will be included in the appendix, but the amount the second order peak height and hence the mismatch will be underestimated by is
\begin{equation}
\left(\frac{2(\delta f_x - \delta f_y)}{\Gamma}\right)^2 + 1 \approx (0.48)^2 + 1 = 1.23
\end{equation}
So if the TEM$_{02}$ and TEM$_{20}$ peaks are going to be treated as one in an LHO OMC scan then a better estimate of the true mode mismatch can be obtained by scaling the measured mode mismatch by 1.23.

\section{Astigmatic mismatch}

In addition to the intrinsic astigmatism of the OMC there is also the case of astigmatism in the beam incident on the OMC. 
This raises the question of what does it mean for an astigmatic beam to be mismatched to an astigmatic cavity. 
Recalling that for an non-astigmatic gaussian beam mismatched to a non-astigmatic cavity without misalignments, the fraction of power scattered into higher order modes can be measured by comparing the relative transmitted powers of the cavity locked on the TEM$_{0}$ resonance and the TEM$_{2}$ resonance.

A common way to extend the mismatch measurement to take astigmatism into account is to compare the transmitted powers of the TEM$_{00}$ mode and the sum of TEM$_{20}$ and TEM$_{02}$ modes. 
This may be correct for relatively small astigmatisms (and we will later derive this to be the case), but for large astigmatisms this clearly breaks down.
Consider the case where the waists of an input beam differed for the x and y axis. 
For this example the x axis is perfectly matched (100\% overlap) and the y axis is almost perfectly mismatched (1\% overlap). 
So pretty much all of the beam power is scattered into even y axis higher order modes and none into the x axis modes. 
But the sum of the TEM$_{20}$ and TEM$_{02}$ modes relative to the TEM$_{00}$ mode will always be close to $\frac{1}{2}$ because the TEM$_{20}$ mode doesn't exist and the TEM$_{02}$ mode can't be bigger than a half of the TEM$_{00}$ mode.

To have an accurate extension of mismatch to cover astigmatism it is useful to start at the overlap integrals. 
As before we define the transverse amplitude of a TEM$_{00}$ mode as $U_{00}$.
\begin{equation}
U_{00}(x,y,q_{1s},q_{1t}) = u_0(x,q_{1s})u_0(y,q_{1t}) \label{eq:separable_tem00}
\end{equation}
where $x$ and $y$ define the transverse coordinates and $q_{1s}$ and $q_{1t}$ define the astigmatic basis. 
Here we have expressed $U_{00}(x,y)$ in terms of a product of amplitudes separated in x~and~y. 
The mode mismatch $\mathcal{M}$ of this beam to an astigmatic basis defined by $q_{2s}$~and~$q_{2t}$ is
\begin{align}
\mathcal{M} = 1 - \frac{|\iint_{-\infty}^{\infty} U_{00}(x,y,q_{1s},q_{1t})U_{00}(x,y,q_{2s},q_{2t})^* \dif y \dif x |^2}{\iint_{-\infty}^{\infty} |U_{00}(x,y,q_{1s},q_{1t})|^2 \dif y \dif x}
\end{align}
which is the fraction of power of the input beam that is lost to higher order modes when projecting to another basis, provided that all of the input beam power was in the TEM$_{00}$ mode and no misalignments. 
$\iint_{-\infty}^{\infty} |U_{00}(x,y,q_{1s},q_{1t})|^2 \dif y \dif x$ 
is just unity if we take $U_{00}$ to be normalized. 
\begin{align}
\mathcal{M} = 1 - \left|\iint_{-\infty}^{\infty} U_{00}(x,y,q_{1s},q_{1t})U_{00}(x,y,q_{2s},q_{2t})^* \dif y \dif x \right|^2
\end{align}
From here we can separate the double integral into a product of integrals in $x$ and $y$ 
using equation \ref{eq:separable_tem00}.
\begin{align}
\mathcal{M} &= 1 - \left|\iint_{-\infty}^{\infty} u_0(x,q_{1s})u_0(y,q_{1t})u_0(x,q_{2s})^*u_0(y,q_{2t})^* \dif y \dif x \right|^2\\
&=  1 - \left|\int_{-\infty}^{\infty} u_0(x,q_{1s})u_0(x,q_{2s})^*\dif x \right|^2 \left|\int_{-\infty}^{\infty}  u_0(y,q_{1t}) u_0(y,q_{2t})^* \dif y\right|^2 
\end{align}
These integrals are more commonly expressed as a doubly indexed complex coefficient $k_{nm}$, where $n$ and $m$ are the transverse mode indices in the integral.
\begin{align}
\mathcal{M} &=  1 - |k_{00}(q_{1s},q_{2s})|^2 |k_{00}(q_{1t},q_{2t})|^2
\end{align}
Here we take note that 
\begin{align}
\left(\int_{-\infty}^{\infty} u_0(x,q_{1})u_0(x,q_{2})^*\dif x \right)^2 =\iint_{-\infty}^{\infty} U_{00}(x,y,q_{1})U_{00}(x,y,q_{2})^* \dif y \dif x
\end{align}
hence
\begin{align}
k_{00}(q_1,q_2) = \sqrt{k_{0000}(q_1,q_2)}
\end{align}
Additionally we also note that the overlap $\mathcal{O}$ between two $q$ parameters is given as
\begin{align}
\mathcal{O}(q_1,q_2) = |k_{0000}(q_1,q_2)|^2
\end{align}
hence we can rewrite the astigmatic mismatch in terms of overlap of purely spherical $q$ parameters
\begin{align}
\mathcal{M}\left(q_{1s},q_{1t},q_{2s},q_{2t}\right) = 1 - \sqrt{\mathcal{O}(q_{1s},q_{2s})\mathcal{O}(q_{1t},q_{2t})}
\end{align}
which is one minus the geometric mean of the overlap between the x axis and the y axis of the input beam and the cavity.
This can be rewritten in terms of only spherical mismatches by noting that $\mathcal{M}(q_1,q_2) = 1 - \mathcal{O}(q_1,q_2)$.
\begin{align}
\mathcal{M}\left(q_{1s},q_{1t},q_{2s},q_{2t}\right) = 1 - \sqrt{\left(1 - \mathcal{M}(q_{1s},q_{2s})\right) \left(1 - \mathcal{M}(q_{1t},q_{2t}) \right)} \label{eq:astigmatic_mismatch}
\end{align}

\subsection{Measuring astigmatic mismatch}

The measurement of astigmatic mismatch is only possible with an astigmatic cavity where the degeneracy between the resonance frequencies between the x and the y transverse modes is broken. 
Astigmatic cavities can be created for example by either using astigmatic optics or by forming a ring cavity with spherical optics, where the non-normal angle of incidence provides the astigmatism.

The degeneracy is broken by having different amounts of round trip gouy phase accumulated between the horizontal and vertical transverse axes, which results in the horizontal and vertical transverse modes to resonate at different frequencies, allowing them to be discriminated in a cavity scan.

Once again spherical mismatch is measured by comparing the power transmitted $A_2$ through a cavity when it's locked onto it's TEM$_{02}$ mode (which is degenerate with the TEM$_{20}$ mode) relative to the power transmitted $A_{00}$ when the cavity is locked onto it's TEM$_{00}$ mode
\begin{align}
\frac{A_{2}}{A_{00}} &= \frac{|k_{0002}(q_1,q_2)|^2 + |k_{0020}(q_1,q_2)|^2}{|k_{0000}(q_1,q_2)|^2}\\ 
&= \mathcal{M}(q_1,q_2)
\end{align}
Since we are only considering the case where a spherical gaussian beam is mismatched to a spherical cavity, the overlap integral $k_{0002}$ is equal to $k_{0020}$.
\begin{align}
\mathcal{M}(q_1,q_2) &= \frac{2|k_{0002}(q_1,q_2)|^2}{|k_{0000}(q_1,q_2)|^2}
\end{align}
We can split the overlap integral across x and y as before
\begin{align}
\mathcal{M}(q_1,q_2) &= \frac{2|k_{00}(q_1,q_2)k_{02}(q_1,q_2)|^2 }{|k_{00}(q_1,q_2)k_{00}(q_1,q_2)|^2}\\
&= \frac{2|k_{02}(q_1,q_2)|^2 }{|k_{00}(q_1,q_2)|^2}
\end{align}

For the case where the cavity is astigmatic and the degeneracy between TEM$_{02}$ and the TEM$_{20}$ mode is broken.
The power transmitted $A_{02}$ through the cavity when it's locked on the TEM$_{02}$ mode relative to $A_{00}$ is 
\begin{align}
\frac{A_{02}}{A_{00}} &= \frac{|k_{0002}(q_{1s},q_{1t},q_{2s},q_{2t})|^2}{|k_{0000}(q_{1s},q_{1t},q_{2s},q_{2t})|^2}\\ 
&= \frac{|k_{00}(q_{1s},q_{2s})k_{02}(q_{1t},q_{2t})|^2}{|k_{00}(q_{1s},q_{2s})k_{00}(q_{1t},q_{2t})|^2}\\ 
&= \frac{|k_{02}(q_{1t},q_{2t})|^2}{|k_{00}(q_{1t},q_{2t})|^2}\\
&= \frac{1}{2} \mathcal{M}(q_{1t},q_{2t})
\end{align}
which shows that the TEM$_{02}$ power relative to TEM$_{00}$ power is half the spherical mismatch between the tangential (vertical) $q$ parameters of the beam and the cavity.

Repeating for $A_{20}$
\begin{align}
\frac{A_{20}}{A_{00}} &= \frac{|k_{0020}(q_{1s},q_{1t},q_{2s},q_{2t})|^2}{|k_{0000}(q_{1s},q_{1t},q_{2s},q_{2t})|^2}\\ 
&= \frac{|k_{02}(q_{1s},q_{2s})k_{00}(q_{1t},q_{2t})|^2}{|k_{00}(q_{1s},q_{2s})k_{00}(q_{1t},q_{2t})|^2}\\ 
&= \frac{|k_{02}(q_{1s},q_{2s})|^2}{|k_{00}(q_{1s},q_{2s})|^2}\\
&= \frac{1}{2} \mathcal{M}(q_{1s},q_{2s})
\end{align}
perhaps unsurprisingly the TEM$_{20}$ power relative to TEM$_{00}$ gives half the spherical mismatch between the sagittal (horizontal) $q$ parameters of the beam and the cavity.

We can now express astigmatic mismatch in terms of purely measurable quantities. By plugging the mode ratios into equation \ref{eq:astigmatic_mismatch} we get
\begin{align}
\mathcal{M}\left(q_{1s},q_{1t},q_{2s},q_{2t}\right) = 1 - \sqrt{\left(1 - 2\frac{A_{20}}{A_{00}}\right) \left(1 - 2\frac{A_{02}}{A_{00}} \right)} \label{eq:measured_astigmatic_mismatch}
\end{align}

\subsection{Special case: weak astigmatism}

\section{Measuring astigmatic mismatch in the OMC}

It was claimed that the astigmatism in the OMC was too weak to be observed by gradient tests in section \ref{sec:omc_astigmatism_intro}. 
Without a strong astigmatism conventional peak finding algorithms (like the ones found in MATLAB and python) will not distinguish between the TEM$_{20}$ and TEM$_{02}$ resonances 
and instead just find the peak in their sum. 
However it is known that the resonances in the cavity take the shape of lorentzians [need source]. 
The sum of two separated lorentzians is not another lorentzian. 
This fact can be used to extract out the individual resonances from a sum by employing non-linear curve fitting algorithms to fit a sum of lorentzians to a measured cavity scan.

As an example take the following chunk of an OMC scan. 
This particular piece came from an OMC scan with a highly astigmatic input beam, such that the asymmetry in the resonance can be seen by eye. 
Perhaps unsurprisingly fitting a single lorentzian to the data results in a relatively poor fit.

We define a lorentzian as the following
\begin{align}
L(x,x_0,\Gamma) = \frac{\left( \frac{\Gamma}{2} \right)^2}{\left(x-x_0\right)^2 + \left( \frac{\Gamma}{2} \right)^2 }
\end{align}
where it is defined such that $L(0,0,\Gamma) = 1$. 
The model used to fit this particular set was the following
\begin{align}
f(y_0,A_2,t_0,\Gamma) = y_0 + A_2 \cdot L(t,t_0,\Gamma)
\end{align}
where $t$ is vector of GPS times that the peak was measured at. 
The model is fitted by minimizing the sum of squares of the residual between the model and the data by a non-linear optimization algorithm such as Nelder-Mead.
A single lorentzian proves to be an inaccurate model as can be seen from figure \ref{fig:single_lotrentz_fit} since it is unable to represent the lopsided nature of the peak. 

The model can be substantially improved by introducing two more free parameters for a second lorentzian, namely the height of the second peak and the separation from the first. 
The model is now
\begin{align}
f(y_0,A_{20},A_{02},t_0,\delta_t,\Gamma) = y_0 + A_{20} \cdot L(t,t_0-\delta_t,\Gamma) + A_{02} \cdot L(t,t_0+\delta_t,\Gamma)
\end{align}

Running this through the optimizer results in figure \ref{fig:double_lotrentz_fit}.

The fit parameters for the fit were

\aac{add script for auto putting in numbers (maybe)}
\begin{figure}[H]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|}
		\hline
		$y_0$&$A_{20}$&$A_{02}$&$t_0$&$\delta_t$&$\Gamma$\\
		\hline
	\end{tabular} 
\end{figure}

The fit residual (fitted model subtracted from the data) and the peak decomposition are shown below in figure \ref{fig:double_lotrentz_fit}

\section{MCMC}

The next logical step to improve on the nonlinear fitting of models is to sample the parameter space of the model around the fit to find the probability distribution (i.e. the uncertainty) of the fit parameters. 
Markov chain monte carlo algorithms exist for such a purpose. 
The one used here is called emcee; a python implementation of an MCMC algorithm. 
The usage is straightforward requiring a likelihood function, priors, the number of walkers, and an initialisation of the walkers.

\subsection{Likelihood function}
In this application the likelihood function serves the same purpose as the objective function in classical machine learning
 
