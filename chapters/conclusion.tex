\chapter{Conclusion}
\label{chp:conclusion}

Improvements to the sensitivity of the Advanced LIGO detectors will require more impact from the injected squeezing. This imposes tighter constraints on the allowed mode mismatches inside the interferometer.
% Further improvements to the sensitivity of the Advanced LIGO detectors in the upcoming aLIGO+ upgrade will introduce frequency dependent squeezing, which imposes tighter constraints on the allowed mode mismatches inside the interferometer.
In this thesis we have explored and demonstrated new modelling and mode matching sensing schemes that can help aLIGO+ reach design sensitivity.
This thesis can be summarised as follows:

\Cref{chp:introduction} presented an introduction to the field of ground-based gravitational wave interferometry.

In \cref{chp:optical_modelling} we reviewed the application of Hermite-Gauss (HG) modes to paraxial optics and how they form the basis of a geometrically stable cavity with infinite mirrors.
This naturally led to a definition of a HG basis, which we used extensively to analyze our mode matching signal presented in \cref{chp:mode_matching_error_signal}.
We additionally reviewed the ABCD matrix formalism for modelling beam propagation in paraxial optical systems, which is necessary for understanding the new optical modelling methods presented in \cref{chp:lct_paper,chp:advanced_lct}.

% \section{Linear Canonical Transform}
\Cref{chp:lct_paper} demonstrated the use of the Linear Canonical Transform (LCT) to study the circulating field in the aLIGO arm cavity with high spatial resolution in the presence of point absorbers and a finite aperture.
The model is similar but distinct to existing Fast Fourier Transform (FFT) based optical models.
The advantage of the LCT is that a sequence of paraxial optical elements can be efficiently compressed into a single linear operator, which improves computational performance and reduces spatial aliasing from multiple FFT operations.
We demonstrated a further improvement to model runtime by using Krylov subspace iterative solvers in our LCT model.
The Krylov solvers are model agnostic and can in principle be used to speed up existing FFT based and possibly HG based models.

In \cref{chp:advanced_lct} we presented a framework for extending LCT models to coupled cavities by treating an optical model as a directed network graph.
By applying a graph reduction procedure a generalization of the round trip operator is produced that describes how the circulating fields in each cavity interact with each other.
The circulating fields in the coupled cavity can then be solved using the same methods as a single cavity by replacing the round trip operator with the generalized round trip operator.
As a sanity check we demonstrated that the graph based solver produces numerically identical results to a standard sparse matrix solver in an HG based model like Finesse.
Future work will demonstrate a graph based coupled cavity solver using LCTs with the aim to create a high spatial resolution model of the circulating field in the dual recycled Fabry-Perot Michelson interferometer used in LIGO with finite apertures and realistic mirror surfaces.

% \subsection{Potential directions}
% The LCT can be used to model the eigenmodes of any stable or unstable resonator by using the round trip operator in the iterative Arnoldi method.
% Preliminary work shows that this is a viable approach, however the eigenmodes of unstable resonators are difficult to identify when they are not closely approximated by HG modes.

% It is possible to extend the LCT to model misaligned paraxial optical systems that can be described by ABCDEF matrices.
% That family of transforms is sometimes referred to as the Special Affine Fourier Transform (SAFT), of which the LCT is a subset of.

In \cref{chp:commissioning} we presented the commissioning tasks I undertook at the LIGO Hanford observatory.
I measured the transverse mode spacing (TMS) in the Hanford (LHO) and Livingston (LLO) output mode cleaners (OMCs) from existing OMC cavity scans and found that the LHO OMC had a TMS that was more than double of the LLO OMC.
The LHO OMC astigmatism was found to cause the mode mismatch measured from resonant peak heights to be underestimated by a factor of up to 1.6.
I measured the ability of the SR3 rear heater to correct for mode mismatches at LHO and found a small improvement, which could be explained by the beam being in a Gouy phase that was orthogonal to the SR3 heater, where a second actuator on the SRM was needed to correct for it.

% \section{Mode matching error signal}
\Cref{chp:mode_matching_error_signal} demonstrated a new type of mode matching error signal based on inducing radio-frequency beam shape modulation via the addition of a second order HG mode sideband.
This method only uses single element photodetectors and no Gouy phase telescopes to sense the two orthogonal mode matching degrees of freedom.
In our tabletop experiment this mode matching error signal was used to increase the mode matching to a cavity to 99.9\%, which is well above the aLIGO+ target of 98\%.
A potential approach for implementing it in aLIGO+ was presented, serving as the error signal for the planned aLIGO+ mode matching actuators to match the filter cavity, output mode cleaner, and interferometer beam to the same reference.

\section{Outlook}
Personally, I am excited about the future direction of gravitational wave detection. 
As our understanding of these complex optical systems increases we are able to extract more and more sensitivity from these detectors, going beyond what I had initially believed would be possible.
With the improved squeezing in aLIGO+ it seems all but inevitable to me that there will need to be a closed loop mode matching sensing and actuation system.
The sensing problem is challenging to solve completely for a coupled cavity interferometer, but this thesis has made progress towards that goal.
The remaining challenges on that front I believe are as much conceptual as they are practical.
This was one of the main motivating factors for me to pursue alternative theories of optical modelling, which culminated in the LCT research presented in this thesis. 
% The LCT modelling framework for gravitational wave detectors that I worked on developing during my PhD has the potential to be disruptive to the field of optical modelling.
The LCT modelling framework elegantly ties the theory of paraxial diffraction with quantum mechanics and signal processing in a way that is both satisfying and insightful.
After publishing my LCT paper a number of people have expressed interest in using those techniques in their own work.
% I am not the first person to use the LCT in such a way, but unfortunately many past attempts ended up as historical footnotes.
Beyond this PhD I aim to integrate the LCT methods with a major open source modelling tool such as \textsc{Finesse3}, hopefully making it more useful to a wider group of people.
