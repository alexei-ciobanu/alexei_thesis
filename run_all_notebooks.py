import sys
import os
import pathlib
from pathlib import Path
import subprocess

notebook_path = Path('./notebooks')
if not notebook_path.exists():
    raise Exception(f'notebook path "{notebook_path.absolute()}" does not exist')

walk_generator = os.walk(notebook_path)
notebook_py_scripts = []
for i in walk_generator:
    cwd, dirs, files = i
    pcwd = Path(cwd).absolute()
    for file in files:
        file_path = pcwd / file
        if file_path.suffix == '.py':
            notebook_py_scripts.append(file_path)

print(f'found {len(notebook_py_scripts)} notebook scripts at "{notebook_path.absolute()}"')

for script_path in notebook_py_scripts:
    print(f"running {script_path.absolute()}")
    res1 = subprocess.run(["python", script_path], shell=True, capture_output=True)
    try:
        res1.check_returncode()
    except Exception as e:
        print(res1.stderr.decode()) # print the traceback of the script
        sys.exit() # don't bother raising the error here
    print(res1.stdout.decode())