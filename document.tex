\documentclass[a4paper,12pt]{book}

% global packages
\usepackage{ parskip }
\usepackage{ setspace }

%% math packages
\usepackage{ amssymb }
\usepackage{ mathtools } % superseeds amsmath
\usepackage{ braket }
\usepackage{ bm }
\usepackage{ commath } % provides \dif
\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}
% \allowdisplaybreaks

%% figure packages
\usepackage{ float }
\usepackage{ xcolor } % superceeds color
\usepackage{ caption }
\usepackage{ subcaption } % provides subfigure
\captionsetup{font={stretch=1.2}}
\usepackage{ tabularx }
\usepackage{ booktabs }
\usepackage{ longtable }

% https://tex.stackexchange.com/a/157400/252691
\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}

% Define typographic struts, as suggested by Claudio Beccari
%   in an article in TeX and TUG News, Vol. 2, 1993.
\newcommand\Tstrut{\rule{0pt}{5.6ex}}         % = `top' strut
\newcommand\Bstrut{\rule[-3.9ex]{0pt}{0pt}}   % = `bottom' strut


%% reference packages
\usepackage{ url }
\usepackage{ hyperref }
\usepackage[noabbrev]{ cleveref }
\hypersetup{
	colorlinks,
	linkcolor={red!50!black},
	citecolor={blue!50!black},
	urlcolor={blue!80!black}
}

\usepackage[
style=numeric,
backend=biber,
sorting=ynt,
url=false
]{biblatex}
\addbibresource{mylibrary.bib}
% make \fullcite print all authors
\preto\fullcite{\AtNextCite{\defcounter{maxnames}{99}}}

\usepackage{ graphicx } % superceeds graphics
\graphicspath{{papers/mode_matching_error_signal/}{papers/linear_canonical_transform/}{notebooks/}{notebooks/beam_profiling/}{notebooks/commissioning/}{images/}}

\usepackage{geometry}
\geometry{
    a4paper,
    left=30mm,
    right=25mm,
    top=25mm,
    bottom=25mm
}

%\renewcommand{\baselinestretch}{1.5}
\setstretch{1.5} % provided by setspace

\usepackage{titlesec}
\titlespacing{\chapter}{0pt}{0pt}{20pt}
\titlespacing{\section}{0pt}{10pt}{10pt}
\titlespacing{\subsection}{0pt}{10pt}{5pt}


\newcommand{\aac}[1]{\textcolor{red}{[AAC] #1}}

\newcommand{\tem}[1]{HG$_{#1}$}
\newcommand{\bigO}[1]{\mathcal{O}\!\left(#1\right)}

\newcommand{\operation}[2]{\text{#1}\left[#2\right]}
\newcommand{\mean}[1]{\operation{mean}{#1}}
\newcommand{\std}[1]{\operation{std}{#1}}
\newcommand{\var}[1]{\operation{var}{#1}}
\renewcommand{\min}[1]{\operation{min}{#1}}
\renewcommand{\max}[1]{\operation{max}{#1}}
\newcommand{\erf}[1]{\operation{Erf}{#1}}
\newcommand{\erfinv}[1]{\operation{ErfInv}{#1}}

\newcommand{\p}{\mkern1mu}
\newcommand{\iu}{\mathrm{i}\mkern1mu}
\newcommand{\clf}{400~MHz }
\newcommand{\clfd}{$\delta f_2$ }
\newcommand{\mmp}{\text{M} }
\newcommand{\mma}{\mathcal{M} }

\newcommand{\lct}[1]{\mathcal{L}_{#1}}
\newcommand{\dlct}[1]{\mathbf{L}_{#1}}

\newcommand{\re}[1]{\text{Re}\!\left[#1\right]}
\newcommand{\im}[1]{\text{Im}\!\left[#1\right]}

\author{Alexei Ciobanu}
\title{Wavefront modelling and sensing for advanced gravitational wave detectors}
\date{June 2021}

\makeatletter
\newcommand\thefontsize[1]{{#1 The current font size is: \f@size pt\par}}
\makeatother

% Force floats to stay in the section/subsection they were defined in
% https://tex.stackexchange.com/a/235312/252691
\usepackage{placeins}
\let\Oldsection\section
\renewcommand{\section}{\FloatBarrier\Oldsection}
\let\Oldsubsection\subsection
\renewcommand{\subsection}{\FloatBarrier\Oldsubsection}

\usepackage{lineno}
% compatibility patch between lineno and \align environments
% https://tex.stackexchange.com/a/55297/252691
\newcommand*\patchAmsMathEnvironmentForLineno[1]{%
  \expandafter\let\csname old#1\expandafter\endcsname\csname #1\endcsname
  \expandafter\let\csname oldend#1\expandafter\endcsname\csname end#1\endcsname
  \renewenvironment{#1}%
     {\linenomath\csname old#1\endcsname}%
     {\csname oldend#1\endcsname\endlinenomath}}% 
\newcommand*\patchBothAmsMathEnvironmentsForLineno[1]{%
  \patchAmsMathEnvironmentForLineno{#1}%
  \patchAmsMathEnvironmentForLineno{#1*}}%
\AtBeginDocument{%
\patchBothAmsMathEnvironmentsForLineno{equation}%
\patchBothAmsMathEnvironmentsForLineno{align}%
\patchBothAmsMathEnvironmentsForLineno{flalign}%
\patchBothAmsMathEnvironmentsForLineno{alignat}%
\patchBothAmsMathEnvironmentsForLineno{gather}%
\patchBothAmsMathEnvironmentsForLineno{multline}%
}
% number all lines
% \linenumbers

\begin{document}
\frontmatter

\include{chapters/titlepage}

\include{chapters/abstract}

\include{chapters/declaration}

\include{chapters/acknowledgements}

% textwidth = \the\textwidth\\
% columnwidth = \the\columnwidth\\

% 1. \thefontsize\tiny
% 2. \thefontsize\scriptsize
% 3. \thefontsize\footnotesize
% 4. \thefontsize\small
% 5. \thefontsize\normalsize
% 6. \thefontsize\large
% 7. \thefontsize\Large
% 8. \thefontsize\LARGE
% 9. \thefontsize\huge
% 10. \thefontsize\Huge

\tableofcontents

\mainmatter

\include{chapters/intro_chapter}

\include{chapters/modelling_chapter}

\include{chapters/modelling_circulating_fields_with_LCT}

\include{chapters/advanced_LCT}

\include{chapters/comissioning_chapter}

\include{chapters/mode_matching_error_signal}

\include{chapters/conclusion}

\appendix

\include{chapters/appendix_wave}

\include{chapters/appendix_lct}

\include{chapters/appendix_commssioning}

% missing some key results
% \include{chapters/experimental_chapter}

\include{chapters/appendix_errsig}

\include{chapters/list_of_publications}

\printbibliography
    
\end{document}