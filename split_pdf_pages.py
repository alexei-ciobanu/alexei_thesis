import fitz # provided by pymupdf
import argparse
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument("document_path", help="path to pdf file (include the extension)")
parser.add_argument("-o", metavar='out_dir', default='.', help="output directory")

args = parser.parse_args()

document_path = Path(args.document_path)
document_name = document_path.stem
output_directory = Path(args.o)

def main():
    doc = fitz.open(args.document_path)
    first_page = 1
    last_page = doc.page_count

    pages = range(first_page, last_page+2)
    page_ranges = zip(pages, pages[1:])

    output_directory.mkdir(parents=True, exist_ok=True)

    for i,page_range in enumerate(page_ranges):
        p1,p2 = page_range
        doc = fitz.open(document_path) # reopen document (surprisingly fast)
        # could alternatively use copy/deepcopy (might be slower)
        doc.select(range(p1-1, p2-1))
        # not sure why select() uses 0-based indexing for page numbers when pdf page 
        # numbers start with 1

        # need to call garbage collect when saving otherwise the deleted pages 
        # will also be saved to file
        filename = Path(f'{i:02d}_{document_name}.pdf')
        filepath = output_directory / filename
        doc.save(filepath, garbage=4, deflate=True)

if __name__ == '__main__':
    main()