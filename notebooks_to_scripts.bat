@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

REM loop over all .ipynb files in the root \notebooks subdirectories
REM convert the notebooks to python scripts
REM strip those python scripts of ipython magics using regex

for /R .\notebooks\ %%f in (*.ipynb) do (
	set str1=%%f
	if "x!str1:\.ipynb_checkpoints\=!"=="x!str1!" (
	jupyter nbconvert --to script !str1!
	python strip_ipy_magics.py %%~dpnf.py
	) else (
	echo skipping ipynb checkpoint
	)
)